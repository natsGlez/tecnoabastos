@extends('layout.mainlayout')

@section( 'content')
<div class="mt-4 staggered-animation-wrap">
    <!-- START SECTION BREADCRUMB -->
    <div class="breadcrumb_section bg_gray page-title-mini">
        <div class="container"><!-- STRART CONTAINER -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="page-title">
                        <h1>TÉRMINOS Y CONDICIONES</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Términos y Condiciones</li>
                    </ol>
                </div>
            </div>
        </div><!-- END CONTAINER-->
    </div>
    <!-- END SECTION BREADCRUMB -->

    <!-- START MAIN CONTENT -->
    <div class="main_content">

    <!-- STAT SECTION FAQ -->
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-12" style="padding-left: 90px; padding-right: 90px">
                    <div class="term_conditions">
                        <p>En TecnoAbastos nos importa tu completa satisfacción, por lo que nuestra política de garantías y devoluciones es muy sencilla.</p>
                        <h6>Sobre las Devoluciones</h6>
                        <ol>
                            <li>Solo se aceptará devoluciones de mercancía de línea de nuestro stock.</li>
                            <li>Solo se podrán realizar devoluciones de producto no mayor a 5 días hábiles posteriores a la fecha de facturación.</li>
                            <li>El producto debe estar completo en perfectas condiciones y dentro de su empaque original.</li>
                            <li>Toda Devolución causará una penalización del 25% del valor de la mercancía y el FLETE debe ser cubierto por cuenta y gasto del cliente.</li>
                            <li>Los cargos de flete no son reembolsables ni considerados como parte de la devolución.</li>
                            <li>En caso de proceder la devolución el rembolso no será en efectivo ni pagadero inmediatamente, sino que será rembolsado mediante nota de crédito, misma que será aplicada a facturas pendientes de pago actuales o futuras. Bajo ningún caso se reembolsara el efectivo.</li>
                            <li>Para solicitar una Devolución deberá hacerlo a través de su Ejecutivo Comercial.</li>
                            <li>No se aceptarán devoluciones cuando se incumplan cualquier de las siguientes condiciones: </li>
                        {{-- </ol> --}}
                        {{-- <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        <hr>
                        <h6>Why do we use it?</h6>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words</p>
                        <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p> --}}
                        <ul>
                            <li>El empaque ha sido abierto, maltratado, golpeado, rayado o modificado a las condiciones originales.</li>
                            <li>El producto fue usado o alterado total o parcialmente. </li>
                            <li>Cuando la mercancía tenga más de 5 días de haberse facturado.</li>
                        </ul>
                        <h6>Sobre las Garantías</h6>
                        {{-- <ol> --}}
                            <li>Cualquier reclamo de garantía debe ser hecha exclusivamente en nuestra empresa contactando a su Ejecutivo Comercial.</li>
                            <li>Cualquier reclamo por daño o faltante debe ser reportado a su ejecutivo dentro de las primeras 24hrs posteriores a haber recibido la mercancía.</li>
                            <li>En caso de que la mercancía sea entregada por paquetería y el paquete se encuentre abierto, golpeado o dañado, te sugerimos no recibir el producto y reportarlo inmediatamente a tu Ejecutivo Comercial.</li>
                            <li>En caso de que aplique la garantía, los gastos de flete serán cubiertos por TecnoAbastos; en caso de que se procese una garantía y el material se encuentre en buen funcionamiento el cliente deberá pagar $300.00 pesos por concepto de fletes y revisión..</li>
                            <li>TecnoAbastos ofrece garantía y responsabilidad ÚNICAMENTE sobre los productos que comercializamos y nunca ni bajo ninguna circunstancia en/o sobre los productos donde estos son utilizados.</li>
                            <li>Entre las causas de invalidez de garantía se encuentran:</li>
                                <ul>
                                    <li>Cuando el reclamo de Garantía se haga fuera del periodo indicado como vigencia de garantía.</li>
                                    <li>Cuando el producto se encuentre golpeado, rayado, quemado, alterado, intervenido o dañado por factores externos al mismo. </li>
                                    <li>Dañados por otros equipos no compatibles técnicamente.</li>
                                    <li>Cuando el producto no se haya colocado o usado correctamente.</li>
                                    <li>Cuando el producto NO tenga nuestra ETIQUETA INSIGNIA DE GARANTÍA o esta se encuentre tachada, rayada, alterada o que muestre cualquier signo de intento o modificación.</li>
                                </ul>
                            <li>El periodo de garantía inicia a partir de la fecha de facturación y se distribuyen por tipo de producto de la siguiente manera:</li>
                                <ul>
                                    <li>Un año para Baterías y Adaptadores de corrientes para Laptops.</li>
                                    <li>Tres meses para Displays de Laptops.</li>
                                    <li>Tres meses para Teclados de Laptops.</li>
                                    <li>Un año Fusores y Kits de Mantenimiento.</li>
                                    <li>En otros productos no mencionados anteriormente, el Ejecutivo Comercial indicará por escrito el periodo de garantía en la cotización y/o factura.</li>
                                </ul>
                            <li>Nuestras políticas de Garantías y Devoluciones podrá cambiar total o parcialmente en cualquier momento.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION FAQ -->
</div>
@endsection
