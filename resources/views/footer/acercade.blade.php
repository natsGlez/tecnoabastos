@extends('layout.mainlayout')

@section('content')
<div class="mt-4 staggered-animation-wrap">
    <!-- START SECTION BREADCRUMB -->
    <div class="breadcrumb_section bg_gray page-title-mini">
        <div class="container"><!-- STRART CONTAINER -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="page-title">
                        <h1>Acerca de Nosotros</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">About</li>
                    </ol>
                </div>
            </div>
        </div><!-- END CONTAINER-->
    </div>
    <!-- END SECTION BREADCRUMB -->


    <!-- STAT SECTION ABOUT -->
    <div class="section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="about_img scene mb-4 mb-lg-0">
                        <img style="padding: 50px" src="assets/images/nosotros.jpg" alt="nosotros"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="heading_s1">
                        <h2>¿Quiénes Somos?</h2>
                    </div>
                    <p>TecnoAbastos es el proveedor en refaccionamiento, accesorios y tecnología más grande de México, y estamos ubicados en Guadalajara, Jalisco.

                    <p>Iniciamos operaciones en el año 2017 ofreciendo soluciones en tecnología para el mercado mexicano.
                        Contamos con partes, refacciones, accesorios y soluciones para los modelos y equipos más vendidos
                        en el país, de las marcas mas reconocidas nacional e internacionalmente.</p>

                    <p>En TecnoAbastos encontrarás desde filminas para equipos de impresión, hasta accesorios, baterías,
                        cargadores, teclados y displays para equipos de cómputo.</p>
                    <p>Haz tu compra hoy y comprueba nuestra calidad y servicio.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION ABOUT -->

    <!-- START SECTION WHY CHOOSE -->
    <div class="section bg_light_blue2 pb_70">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <div class="heading text-center">
                        <h2>¿Por qué comprar con nosotros?</h2>
                    </div>
                    {{-- <p class="text-center leads">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p> --}}
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-sm-6" style="padding: 20px">
                    <div class="icon_box icon_box_style4 box_shadow1">
                        <div class="icon">
                            <img src="assets/images/envio.png" width="17%"/>
                        </div>
                        <div class="icon_box_content">
                            <h5>Envíos el mismo día</h5>
                            <p>Compra hoy y recibe tu producto en un tiempo estimado entre uno y tres
                                días hábiles.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6" style="padding: 20px">
                    <div class="icon_box icon_box_style4 box_shadow1">
                        <div class="icon">
                            <img src="assets/images/disponible.png" width="17%"/>
                        </div>
                        <div class="icon_box_content">
                            <h5>Disponibilidad</h5>
                            <p>Contamos con stock en la mayoría de nuestros productos y un catálogo de más
                                de 5,000 artículos.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6" style="padding: 20px">
                    <div class="icon_box icon_box_style4 box_shadow1">
                        <div class="icon">
                            <img src="assets/images/atencion.png" width="17%"/>
                        </div>
                        <div class="icon_box_content">
                            <h5>Soporte y Atención</h5>
                            <p>Contacta a nuestro Ejecutivos vía Whatsapp para que te asesoren con la
                                compra de tus productos.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION WHY CHOOSE -->
</div>
@endsection
