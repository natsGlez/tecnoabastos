@extends('layout.mainlayout2')

@section('content')
<div class="mt-4 staggered-animation-wrap">
<!-- START SECTION BREADCRUMB -->
<div class="breadcrumb_section bg_gray page-title-mini">
    <div class="container"><!-- STRART CONTAINER -->
        <div class="row align-items-center">
        	<div class="col-md-6">
                <div class="page-title">
            		<h1>Aviso de Privacidad</h1>
                </div>
            </div>
            <div class="col-md-6">
                <ol class="breadcrumb justify-content-md-end">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Pages</a></li>
                    <li class="breadcrumb-item active">Aviso de Privacidad</li>
                </ol>
            </div>
        </div>
    </div><!-- END CONTAINER-->
</div>
<!-- END SECTION BREADCRUMB -->

<!-- START MAIN CONTENT -->
<div class="main_content">

<!-- STAT SECTION FAQ -->
<div class="section">
	<div class="container">
    	<div class="row">
        	<div class="col-12" style="padding-left: 90px; padding-right: 90px">
            	<div class="term_conditions" style="align: justify">
                    <p>TecnoAbastos, con domicilio en Calle Domingo Sarmiento Núm. 2822 Col.
                        Prados Providencia,   C.P. 44690 en Guadalajara, Jalisco, es poseedora y
                        operadora del sitio web con dominio: www.tecnoabastos.com</p>
                    <p>TecnoAbastos es y se hace responsable del manejo de datos personales, por lo cual
                        reconoce y privilegia la privacidad y confidencialidad de cualquier información
                        que tenga o llegue a tener en su poder, ya sea de manera temporal o definitiva,
                        incluyendo datos o información personal. TecnoAbastos reconoce que toda la
                        información personal que se encuentra en nuestro poder ha sido proporcionada de
                        manera libre y voluntaria por sus titulares y/o ha sido adquirida de forma
                        legítima a través de medios legales idóneos bajo el principio de privacidad y
                        confidencialidad de la información, con el objeto de garantizar la seguridad y
                         protección de la misma y de sus titulares.</p>
                    <p>Por lo anterior, hemos desarrollado un Aviso de Privacidad que deseamos le ayude
                        a comprender la forma en la que recabamos, utilizamos, transferimos,
                        almacenamos y protegemos su información.</p>
                    <p>La información personal, incluso la patrimonial o sensible, está constituida por
                         datos que pueden ser utilizados exclusivamente para identificar o contactar a
                         una sola persona. Se puede solicitar información personal en cualquier momento
                          cuando te contactes con TecnoAbastos o una compañía afiliada. TecnoAbastos y
                          sus afiliadas pueden compartir información personal entre ellas y utilizarla
                          en virtud del presente Aviso de Privacidad. Asimismo, podrán combinarla con
                          otra información para proporcionar y mejorar productos, servicios, contenidos
                          y publicidad.</p>
                    <p>De conformidad con la Ley de Protección de Datos Personales en Posesión de los
                        Particulares (la “Ley”), TecnoAbastos al procesar la información del cliente
                         está obligado a observar los principios de licitud, consentimiento,
                         información, calidad, finalidad, lealtad, proporcionalidad y responsabilidad
                         previstos en la Ley.</p>
                    <p>Mantendremos políticas, medidas de seguridad física, electrónica y de
                        procedimientos para proteger la confidencialidad de la información que
                        obtenemos sobre nuestros clientes y aquellos que utilizan y visitan nuestro
                        sitio Web.</p>
                    <p>TecnoAbastos hace de su conocimiento que sus datos personales recabados, que se
                        recaben o sean generados con motivo de la relación jurídica que tengamos
                        celebrada, o que en su caso, se celebre; los datos que recabamos son: nombre
                        completo, correo electrónico, domicilio, teléfono, RFC, teléfono móvil, fecha
                        de nacimiento y edad, se tratarán para todos los fines vinculados con dicha
                        relación, los cuales son:</p>
                    <h6>Finalidades primarias</h6>
                    <ol>
                    	<li>La comercialización de accesorios, componentes, partes y refacciones para equipos de cómputo e impresión. </li>
                        <li>La prestación de cualquier servicio solicitado por usted.</li>
                        <li>La facturación de los servicios prestados o productos adquiridos.</li>
                    </ol>
                    <h6>Finalidades secundarias:</h6>
                    <ol>
                    	<li>Informar y promocionar nuestros servicios, productos o eventos, incluyendo envío
                             de publicidad y boletines informativos de TecnoAbastos  o cualquiera de las
                             empresas pertenecientes a su mismo grupo corporativo y de promotores o
                             anunciantes relacionados con éstos;</li>
                        <li>Evaluaciones de calidad y servicio, auditorías, análisis e investigación
                            para el desarrollo y mejoramiento de nuestros servicios, productos,
                            contenidos, comunicaciones y publicidad.</li>
                    </ol>
                    <p>En caso de que no desee que sus datos personales sean tratados para alguna o
                        todas las finalidades secundarias, desde este momento usted nos puede comunicar
                        lo anterior al correo contacto@tecnoabastos.com</p>
                    <p>Al proporcionar los datos personales de terceros usted reconoce tener el
                        consentimiento de éstos para que nosotros tratemos sus datos para contactarles.</p>
                    <p>Queda convenido que usted acepta la transferencia que pudiera realizarse con
                        estos fines, en su caso a las entidades que formen parte de nosotros directa o
                        indirectamente, subsidiarias y afiliadas, así como a terceros, nacionales o
                        extranjeros, los cuales son: a) instituciones bancarias y demás proveedores
                        relacionados en virtud del procesamiento de pagos, b) personas relacionadas con
                        nosotros para fines relacionados con el servicio, comprobación, revisión y
                        certificación en materia fiscal y administrativa, c) proveedores que nos
                        asisten para el efectivo cumplimiento de los servicios, d) Empresas que tengan
                        celebrados convenios de colaboración con nosotros para llevar a cabo la promoción
                        y ofrecimiento de sus productos y servicios, (se requiere de su consentimiento), e)
                        terceros para dar cumplimiento a la legislación aplicable y/o resoluciones judiciales
                        o administrativas, y, f) la autoridad competente cuando lo requiera.</p>
                    <p>Si usted no desea que transfiramos sus datos personales para aquellas
                        transferencias para las cuales es necesario su consentimiento, le pedimos que
                        envíe un correo electrónico a contacto@tecnoabastos.com en donde se le atenderá en tiempo y forma.</p>
                    <p>Para limitar el uso y divulgación de sus datos, mantendremos medidas de seguridad
                        física, administrativa y técnica, políticas y procedimientos para proteger la
                        seguridad y confidencialidad de la información obtenida. Cualquier modificación a
                        este aviso de privacidad podrá consultarse en nuestra página www.tecnoabastos.com</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION FAQ -->
</div>
@endsection
