@extends('layout.mainlayout')

@section('content')
<div class="mt-4 staggered-animation-wrap">
    <!-- START SECTION BREADCRUMB -->
    <div class="breadcrumb_section bg_gray page-title-mini">
        <div class="container"><!-- STRART CONTAINER -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="page-title">
                        <h1>Preguntas Frecuentes</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Faq</li>
                    </ol>
                </div>
            </div>
        </div><!-- END CONTAINER-->
    </div>
    <!-- END SECTION BREADCRUMB -->

    <!-- START MAIN CONTENT -->
    <div class="main_content">

    <!-- STAT SECTION FAQ -->
    <div class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    {{-- <div class="heading_s1 mb-3 mb-md-5">
                        <h3>Preguntas Generales</h3>
                    </div> --}}
                    <div id="accordion" class="accordion accordion_style1">
                        <div class="card">
                            <div class="card-header" id="headingUno">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseUno" aria-expanded="false" aria-controls="collapseUno">¿Quiénes somos?</a> </h6>
                            </div>
                            <div id="collapseUno" class="collapse" aria-labelledby="headingUno" data-parent="#accordion">
                                <div class="card-body">
                                    <p>TecnoAbastos es el proveedor de partes, refacciones y accesorios para equipos de cómputo e impresión más grande de México, y estamos ubicados en Guadalajara, Jalisco.

                                        Iniciamos operaciones en el año 2017 ofreciendo soluciones en tecnología para el mercado mexicano. Contamos con partes, refacciones, accesorios y soluciones para los modelos y equipos más vendidos en el país, de las marcas mas reconocidas nacional e internacionalmente.

                                        En TecnoAbastos encontrarás desde filminas para equipos de impresión, hasta accesorios, baterías, cargadores, teclados y displays para equipos de cómputo.

                                        Haz tu compra hoy y comprueba todos nuestros beneficios.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingDos">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseDos" aria-expanded="false" aria-controls="collapseDos">¿Cuál es nuestro domicilio?</a> </h6>
                            </div>
                            <div id="collapseDos" class="collapse" aria-labelledby="headingDos" data-parent="#accordion">
                                <div class="card-body">
                                    <p> Nuestras oficinas corporativas se encuentra en Calle Domingo Sarmiento Núm. 2822 Interior 5, Colonia Prados Providencia, C.P. 44690, en Guadalajara, Jalisco, México.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTres">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseTres" aria-expanded="false" aria-controls="collapseTres">¿Por qué medios puedo ponerme en contacto con personal de TecnoAbastos?</a> </h6>
                            </div>
                            <div id="collapseTres" class="collapse" aria-labelledby="headingTres" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Puedes ponerte en contacto con nosotros a través de los siguientes medios:</p>
                                    <ul class="list_style_3">
                                        <li>Teléfono: 33 1575 9152</li>
                                        <li>WhatsApp: 33 1575 9152</li>
                                        <li>Correo electrónico: ventas@tecnoabastos.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingCuatro">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseCuatro" aria-expanded="false" aria-controls="collapseCuatro">¿Dónde puedo buscar un producto?</a> </h6>
                            </div>
                            <div id="collapseCuatro" class="collapse" aria-labelledby="headingCuatro" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Puedes utilizar el buscador inteligente e ingresar el modelo del equipo, el número de parte, el número de reemplazo o marca de tu producto en la barra de búsqueda. También puedes usar los filtros por categoría y buscar tu producto en la categoría de accesorios, desktops, laptops o impresión y su respectiva subcategoría (adaptadores, baterías, displays, teclados, etc.) según corresponda.</p>

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingCinco">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseCinco" aria-expanded="false" aria-controls="collapseCinco">¿Cómo me registro?</a> </h6>
                            </div>
                            <div id="collapseCinco" class="collapse" aria-labelledby="headingCinco" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Registrarte es muy sencillo, solo tienes que dar clic en el botón de Registrarse que se encuentra en la parte superior de nuestra página web, llenar el formulario para crear tu cuenta y ¡listo!
                                        También puedes crear tu cuenta con tu usuario de Facebook o con tu correo electrónico de Gmail.
                                        Después de registrarte recibirás un email de confirmación al correo proporcionado. Verifica que tus datos sean correctos y da clic en el link que te enviamos para confirmar tu registro.
                                        Te invitamos a conocer nuestro Aviso de Privacidad aquí.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingSeis">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseSeis" aria-expanded="false" aria-controls="collapseSeis">¿Dónde puedo rastrear el envío de mi pedido? </a> </h6>
                            </div>
                            <div id="collapseSeis" class="collapse" aria-labelledby="headingSeis" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Para rastrear tu pedido es necesario tener a la mano el número de guía, el cual podrás visualizar en en la sección de Detalle del Pedido una vez tu producto haya sido embarcado. Una vez que lo puedas visualizar, deberás ingresar a la página web de la paquetería asignada para realizar el rastreo.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingSiete">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseSiete" aria-expanded="false" aria-controls="collapseSiete">¿Cómo hago la devolución o cambio de un producto? </a> </h6>
                            </div>
                            <div id="collapseSiete" class="collapse" aria-labelledby="headingSiete" data-parent="#accordion">
                                <div class="card-body">
                                    <p>La solicitud de devolución deberá realizarse a través de tu Ejecutivo Comercial y dentro de los primeros 5 días hábiles posteriores a la fecha de facturación; no hay excepciones.</p>
                                    <p>El producto deberá estar completo y en perfectas condiciones dentro de su empaque original; toda devolución causará una penalización del 25% del valor de la mercancía y el flete deberá ser cubierto por cuenta y gasto del cliente. Para conocer más de nuestras políticas consulta la sección de Política de Garantías y Devoluciones.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingNueve">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseNueve" aria-expanded="false" aria-controls="collapseFive">¿Los productos cuenta con garantía?</a> </h6>
                            </div>
                            <div id="collapseNueve" class="collapse" aria-labelledby="headingNueve" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Todos nuestros productos cuentan garantía, la cual tiene una vigencia de acuerdo a la marca y categoría del producto:</p>
                                    <ul>
                                        <li>Un año para Baterías y Adaptadores de corrientes para Laptops.</li>
                                        <li>Tres meses para Displays de Laptops.</li>
                                        <li>Tres meses para Teclados de Laptops.</li>
                                        <li>Un año Fusores y Kits de Mantenimiento.</li>
                                    </ul>
                                    <p>En otros productos no mencionados, el Ejecutivo Comercial indicará por escrito el periodo de garantía en la cotización y/o factura. </p>
                                    <p>Para conocer más de nuestras políticas consulta la sección de Política de Garantías y Devoluciones.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingOcho">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseOcho" aria-expanded="false" aria-controls="collapseOcho">¿Qué métodos de pago se aceptan? </a> </h6>
                            </div>
                            <div id="collapseOcho" class="collapse" aria-labelledby="headingOcho" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Aceptamos pagos con tarjetas de crédito y débito de todas las instituciones bancarias del país; también pagos a través de PayPal con cualquier tarjeta de crédito o débito y pagos en efectivo a través de MercadoPago en OXXO, Banamex, Santander y BBVA. También se pueden realizar transferencias electrónicas a nuestra cuenta bancaria, la cual estará visible al momento de finalizar la compra y elegir como método de pago Transferencia Bancaria directa.  </p>
                                    <p>Para acreditar los pagos realizados en efectivo o por SPEI, y agilizar el envío de tu mercancía, deberás adjuntar tu comprobante de pago al pedido ingresando a tu cuenta en la sección de Pedidos.
                                        Tienes un plazo máximo de 2 días hábiles después de haber levantado el pedido para realizar tu pago; posterior a este lapso de tiempo la mercancía estará nuevamente disponible para venta.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    {{-- <div class="heading_s1 mb-3 mb-md-5">
                        <h3>Other questions</h3>
                    </div> --}}
                    <div id="accordion2" class="accordion accordion_style1">
                        <div class="card">
                            <div class="card-header" id="headingDiez">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseDiez" aria-expanded="false" aria-controls="collapseDiez">¿Cómo puedo recuperar mi contraseña si la perdí o la olvidé?</a> </h6>
                            </div>
                            <div id="collapseDiez" class="collapse" aria-labelledby="headingDiez" data-parent="#accordion2">
                                <div class="card-body">
                                    <p>Solo tienes que dar clic en el botón de Iniciar Sesión que se encuentra en la parte superior de nuestra página web y posteriormente dar clic en ¿Olvidaste tu contraseña?
                                        Una vez dentro de la sección deberás escribir el correo electrónico con el que te registraste y al cual enviaremos un email con la nueva contraseña para poder acceder a tu cuenta.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingOnce">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseOnce" aria-expanded="false" aria-controls="collapseOnce">¿Dónde puedo modificar mis datos?</a> </h6>
                            </div>
                            <div id="collapseOnce" class="collapse" aria-labelledby="headingOnce" data-parent="#accordion2">
                                <div class="card-body">
                                    <p>En todo momento tendrás acceso a modificar los datos de tu cuenta como el email, dirección de entrega, teléfono de contacto, etc. Para modificar esta información solo tienes que Iniciar Sesión y posteriormente dar clic en Direcciones o Detalles de Cuenta según la información que desees modificar.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingDoce">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseDoce" aria-expanded="false" aria-controls="collapseDoce">¿Cómo puedo realizar una compra en la tienda online de TecnoAbastos?</a> </h6>
                            </div>
                            <div id="collapseDoce" class="collapse" aria-labelledby="headingDoce" data-parent="#accordion2">
                                <div class="card-body">
                                    <p>Las compras realizadas a través de nuestra tienda online son fáciles y seguras, para realizar tu compra de forma exitosa te recomendamos seguir los siguientes pasos:</p>
                                    <ul class="list_style_3">
                                        <li>Antes de iniciar el proceso de compra deberás realizar tu registro en nuestra tienda online.</li>
                                        <li>Si deseas ver todos los productos que tenemos disponibles selecciona la categoría del producto desde nuestro menú.</li>
                                        <li>Si buscas algún producto en específico puedes utilizar el buscador inteligente y realizar tu búsqueda por modelo del equipo, número de parte, número de reemplazo o marca del producto.</li>
                                        <li>Una vez encontrado tu producto podrás ver las especificaciones técnicas, características, precio y disponibilidad del mismo.</li>
                                        <li>Para iniciar el proceso de compra da clic en el botón Añadir a Carrito y el producto se agregará automáticamente a tu carrito de compras.</li>
                                        <li>Una vez que estés seguro(a) de tener todos los artículos que te interesan en el carrito de compras es momento de finalizar la compra. Para finalizarla hay que dar clic en el icono del carrito de compras que encuentra en la parte superior derecha de la página.</li>
                                        <li>Aparecerá un resumen de tu compra indicando cantidad, modelo, descripción y precio. Si todos los datos son correctos solo debes dar clic en Finalizar Compra.</li>
                                        <li>Si ya estas registrado solo necesitas iniciar sesión con tu email y contraseña. En caso de no estar registrado del lado derecho te daremos la opción para el registro.</li>
                                        <li>Es momento de confirmar que todos tus datos sean correctos; si los datos son correctos solo tienes que dar clic en Finalizar Compra.</li>
                                        <li>Llegamos al resumen de la compra donde podrás ver a detalle tu pedido; en caso de tener un cupón de descuento no olvides utilizarlo. Da clic en Finalizar Compra.</li>
                                        <li>Elige el método de pago que utilizarás para realizar tu compra; puedes pagar mediante transferencia bancaria, con tarjeta de débito o crédito, con tu cuenta de PayPal o en efectivo en tiendas Oxxo, Banamex, Santander y BBVA.</li>
                                        <li>Una vez que finalices el proceso, aparecerá en pantalla un mensaje de que la compra fue realiza con éxito y también recibirás un email de confirmación.</li>
                                    </ul>
                                    <p>Si tienes alguna duda adicional, nos puedes contactar al 33 1575 9152 o envíanos un email a contacto@tecnoabastos.com y nos pondremos en contacto a la brevedad.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTrece">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseTrece" aria-expanded="false" aria-controls="collapseTrece">¿Dónde puedo hacer el seguimiento de mi pedido?</a> </h6>
                            </div>
                            <div id="collapseTrece" class="collapse" aria-labelledby="headingTrece" data-parent="#accordion2">
                                <div class="card-body">
                                    <p>Accede a tu cuenta con tu usuario y contraseña; una vez que estés en tu cuenta da clic en la sección Pedidos donde podrás ver el estado, fecha y método de pago de tu pedido, así como adjuntar el comprobante de tu pago. Para cualquier duda adicional también puedes contactarnos vía telefónica o por email.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingCatorce">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseCatorce" aria-expanded="false" aria-controls="collapseCatorce">¿Cuál es el costo de envío?</a> </h6>
                            </div>
                            <div id="collapseCatorce" class="collapse" aria-labelledby="headingCatorce" data-parent="#accordion2">
                                <div class="card-body">
                                    <p>En compras superiores a $499 pesos el envío es gratis a cualquier parte de la República Mexicana. En compras menores a $499 pesos el envío tendrá un costo de $150 pesos.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingQuince">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseQuince" aria-expanded="false" aria-controls="collapseQuince">¿A través de que mensajería se envía mi pedido y cuánto tiempo tarda en llegar?</a> </h6>
                            </div>
                            <div id="collapseQuince" class="collapse" aria-labelledby="headingQuince" data-parent="#accordion2">
                                <div class="card-body">
                                    <p>Nuestros envíos se realizan a través de diversas paqueterías de acuerdo al destino, manejando un tiempo estimado de entrega de entre uno y tres días hábiles; el tiempo de entrega podrá variar en zonas remotas.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingDieciseis">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseDieciseis" aria-expanded="false" aria-controls="collapseDieciseis">¿Por qué se rechazó mi pago con tarjeta de crédito o débito?</a> </h6>
                            </div>
                            <div id="collapseDieciseis" class="collapse" aria-labelledby="headingDieciseis" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Existen varios motivos por los cuales tu pago puede ser rechazado, te recomendamos ponerte en contacto con tu banco para saber si tu cuenta tiene algún tipo de bloqueo para compras en línea, así como para la aclaración de cualquier duda.</p>
                                    <p>Es importante asegurarte de que tus datos estén capturados correctamente antes de finalizar la compra.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingDiecisiete">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseDiecisiete" aria-expanded="false" aria-controls="collapseDiecisiete">En caso de contar con un cupón de promoción, ¿cómo lo puedo aplicar a mi compra?</a> </h6>
                            </div>
                            <div id="collapseDiecisiete" class="collapse" aria-labelledby="headingDiecisiete" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Deberás aplicar el cupón de promoción o cualquier otro cupón antes de finalizar tu compra, ya que una vez finalizada es imposible aplicarlo.</p>
                                    <p>Cualquier cupón debe ser aplicado dentro del proceso de compra cuando te encuentres en el resumen del Carrito de Compras. Una vez que estés en esa sección encontrarás un recuadro que dice Código de Cupón, donde deberás introducir el código del mismo y dar clic en el botón Aplicar Cupón. Una vez aplicado,  se verá reflejado el descuento del cupón en el resumen del carrito de compra del lado derecho.</p>
                                    <p>Los cupones pueden contar con las siguientes restricciones:
                                    • Solo aplica en productos seleccionados
                                    • Monto mínimo de compra
                                    • Sujeto a disponibilidad
                                    • Vigencia</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingDieciocho">
                                <h6 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseDieciocho" aria-expanded="false" aria-controls="collapseDieciocho">¿Cuáles son los beneficios de suscribirme al Boletín de Noticias?</a> </h6>
                            </div>
                            <div id="collapseDieciocho" class="collapse" aria-labelledby="headingDieciocho" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Si te suscribes a nuestro Boletín de Noticias podrás aprovechar la disponibilidad de los nuevos modelos antes que nadie; también te mantendremos al tanto de nuestras ofertas y promociones especiales.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION FAQ -->
</div>
@endsection
