@extends('layout.mainlayout')

@section('content')
    <!-- START 404 SECTION -->
<div class="section">
	<div class="error_wrap">
    	<div class="container">
            <br><br><br><br><br><br><br><br><br>
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-6 col-md-10 order-lg-first">
                	<div class="text-center">
                        <div class="error_txt">404</div>
                        <h5 class="mb-2 mb-sm-3">Oops! La página que buscas no se encuentra!</h5>
                        <p>La página que buscas fue removida, cambió de nombre o nunca existió.</p>
                        <br><br>
                        <a href="{{ url('/') }}" class="btn btn-fill-out">Regresar al Inicio</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END 404 SECTION -->

@endsection
