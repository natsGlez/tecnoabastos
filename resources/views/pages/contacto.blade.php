@extends('layout.mainlayout')

@section('content')
<div class="mt-4 staggered-animation-wrap">
    <!-- START SECTION BREADCRUMB -->
    <div class="breadcrumb_section bg_gray page-title-mini">
        <div class="container"><!-- STRART CONTAINER -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="page-title">
                        <h1>Contacto</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Contact</li>
                    </ol>
                </div>
            </div>
        </div><!-- END CONTAINER-->
    </div>
    <!-- END SECTION BREADCRUMB -->
    <br><br><br>
        @if(session('status'))
            <div class="alert alert-success" style="height: 50px; width: 300px; margin-left: 500px" id="alert">
                {{ session('status') }}
            </div>
        @endif
    <!-- START MAIN CONTENT -->
    <div class="main_content">

    <!-- START SECTION CONTACT -->
    <div class="section pb_70">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6">
                    <div class="contact_wrap contact_style3">
                        <div class="contact_icon">
                            <i class="linearicons-map2"></i>
                        </div>
                        <div class="contact_text" >
                            <span>Domicilio</span>
                            <p style="padding-bottom: 20px">Domingo Sarmiento No. 2822
                            Col. Prados Providencia
                            C.P. 44690 Guadalajara,Jalisco, México.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6" >
                    <div class="contact_wrap contact_style3">
                        <div class="contact_icon">
                            <i class="linearicons-envelope-open"></i>
                        </div>
                        <div class="contact_text">
                            <span>Correo Electrónico</span>
                            <a href="mailto:info@sitename.com">ventas@tecnoabastos.com</a><br><br><br><br>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6">
                    <div class="contact_wrap contact_style3">
                        <div class="contact_icon">
                            <i class="linearicons-tablet2"></i>
                        </div>
                        <div class="contact_text">
                            <span>Teléfono</span>
                            <p>(33) 1575 9152</p><br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION CONTACT -->

    <!-- START SECTION CONTACT -->
    <div class="section pt-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="heading_s1">
                        <h2>Escríbenos</h2>
                    </div>
                        {!! Form::open(['url'=>'enviarMensajeContacto']) !!}
                            <div class="row">
                                <div class="form-group col-md-6" style="padding-right: 5px">
                                    {!! Form::text('Name', '', ['class'=>'form-control', 'required', 'placeholder'=>'Nombre y Apellido*', 'style'=>'margin-bottom: 15px']) !!}
                                    {!! Form::text('Email', '',  ['class'=>'form-control', 'required', 'placeholder'=>'Email*']) !!}
                                </div>
                                <div class="form-group col-md-6" style="padding-left: 5px">
                                    {!! Form::text('Tel', '', ['class'=>'form-control','required','placeholder'=>'Teléfono*', 'style'=>'margin-bottom: 15px']) !!}
                                    {!! Form::text('Ciudad', '', ['class'=>'form-control','required','placeholder'=>'Ciudad y Estado*']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    {!! Form::textarea('Mensaje', '', ['class'=>'form-control','required','placeholder'=>'Mensaje*', 'style'=>'resize: none']) !!}
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-fill-out" >Enviar Mensaje</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                </div>
                <div style="padding-left: 50px">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3732.6040813302275!2d-103.38867198497991!3d20.685680486185216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8428ae6a0be4bfc3%3A0x3783321777ff4a48!2sDomingo%20Sarmiento%202822%2C%20Prados%20Providencia%2C%2044670%20Guadalajara%2C%20Jal.!5e0!3m2!1ses-419!2smx!4v1593021525059!5m2!1ses-419!2smx" width="450" height="400" frameborder="0" style="margin-top: 57px" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION CONTACT -->
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            setTimeout(function() { $() }, 1000);
        })
    </script>
@endsection
