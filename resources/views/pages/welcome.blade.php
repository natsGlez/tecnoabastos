
@extends('layout.mainlayout')

@section('content')
    <!-- START SECTION BANNER -->
    <div class="mt-4 staggered-animation-wrap" >
        <div class="custom-container">
            <br><br><br><br><br><br><br><br><br><br><br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner_section shop_el_slider" style="padding-bottom: 20px; padding-left: 30px;">
                        <div id="carouselExampleControls" class="carousel slide carousel-fade light_arrow" data-ride="carousel">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <a href="{{ url('buscarFamilia/Desktop') }}"><img class="d-block w-100" src="{{ asset('assets/images/bannershome-03.png') }}" alt="First slide"></a>
                                    </div>
                                    <div class="carousel-item">
                                        <a href="{{ url('buscarFamilia/Laptops') }}"><img class="d-block w-100" src="{{ asset('assets/images/bannershome-02.png') }}" alt="Second slide"></a>
                                    </div>
                                    <div class="carousel-item">
                                        <a href="{{ url('buscarFamilia/Printers') }}"><img class="d-block w-100" src="{{ asset('assets/images/bannershome-04.png') }}" alt="Third slide"></a>
                                    </div>
                                    <div class="carousel-item ">
                                        <a href="{{ url('buscarFamilia/Server') }}"><img class="d-block w-100" src="{{ asset('assets/images/bannershome-01.png') }}" alt="Forth slide"></a>
                                    </div>
                                    <div class="carousel-item">
                                        <a href="{{ url('buscarFamilia/Projectors') }}"><img class="d-block w-100" src="{{ asset('assets/images/bannershome-05.png') }}" alt="Five slide"></a>
                                    </div>
                                    <div class="carousel-item">
                                        <a href="{{ url('buscarFamilia/Plotter') }}"><img class="d-block w-100" src="{{ asset('assets/images/bannershome-01.png') }}" alt="Six slide"></a>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- END SECTION BANNER -->

        <!-- START SECTION CLIENT LOGO -->
        @if(session('status'))
            <div class="alert alert-success" id="msgAlert" style="width: 350px; margin-left: 550px" id="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="section pt-0 small_pb">
            <div class="custom-container" style="padding-right: 70px">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading_tab_header"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="client_logo carousel_slider owl-carousel owl-theme nav_style3" data-dots="false" data-nav="true" data-margin="30" data-loop="true" data-autoplay="true" data-responsive='{"0":{"items": "2"}, "480":{"items": "3"}, "767":{"items": "4"}, "991":{"items": "5"}, "1199":{"items": "6"}}'>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/HP.png" alt="cl_logo"/>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/LENOVO.png" alt="cl_logo"/>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/DELL.png" alt="cl_logo"/>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/APPLE.png" alt="cl_logo"/>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/ASUS.png" alt="cl_logo"/>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/ACER.png" alt="cl_logo"/>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/SAMSUNG.png" alt="cl_logo"/>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/SONY.png" alt="cl_logo"/>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/GATEWAY.png" alt="cl_logo"/>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/ECOTEC.png" alt="cl_logo"/>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cl_logo">
                                    <img src="assets/images/NOVOLAP.png" alt="cl_logo"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SECTION CLIENT LOGO -->

        <!-- START SECTION SHOP -->
        <div class="section pt-0 pb_20" style="padding-right: 60px; padding-left: 30px">
            <div class="custom-container">
                <div class="row">
                    {{-- HP--}}
                    <div class="col-lg-4" style="padding-top: 30px">
                        <div class="row" style="padding-left: 49px" >
                            <div class="col-12">
                                <div class="heading_tab_header">
                                    <div class="heading_s2">
                                        <h4>Productos Destacados</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 40px">
                            <div class="col-12">
                                <div class="product_slider carousel_slider product_list owl-carousel owl-theme nav_style5" data-nav="true" data-dots="false" data-loop="true" data-margin="20" data-responsive='{"0":{"items": "1"}, "380":{"items": "1"}, "640":{"items": "2"}, "991":{"items": "1"}}'>
                                    <div class="item">
                                        @foreach($hp_chunk[0] as $resp)
                                            @php
                                                $precioEntreMargen = number_format($resp->P1/.65,2);
                                                $precio = ($precioEntreMargen*$tc)*1.16;

                                                if($resp->Condicion == 1){
                                                    $condicion = 'Nuevo';
                                                }elseif($resp->Condicion == 2){
                                                    $condicion = 'Remanufacturado';
                                                }elseif($resp->Condicion == 3){
                                                    $condicion = 'Usado';
                                                }

                                                if($resp->Disponible > 0){
                                                    $existencia = 'Stock';
                                                }elseif($resp->Disponible == 0){
                                                    $existencia = 'Agotado';
                                                }
                                                // echo $precio;
                                            @endphp


                                            <div class="product_wrap">
                                                <div class="product_img">
                                                    <a href="{{ url('verDetalle/'.$resp->id) }}">
                                                        <img src="{{ $resp->ImageURL }}" alt="el_img5">
                                                        <img class="product_hover_img" src="{{ $resp->ImageURL }}" alt="el_hover_img5">
                                                    </a>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="{{ url('verDetalle/'.$resp->id) }}" id="aNombre">{{ $resp->Grupo }}-{{ $resp->Marcas }}</a></h6>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">{{ $resp->PartNo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500">{{ $existencia }}</span></a>
                                                        </div>
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><p>{{ $resp->Descripcion }}.</p></a>
                                                    </div>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color: red">{{ $resp->Tipo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:green">{{ $condicion }}</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-left: 5px">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">$ {{ number_format($precio,2) }} MXN</span></a>
                                                        @foreach($array as $value)
                                                            @if($value['product_id'] == $resp->id)
                                                                <div class="rating_wrap" style="margin-left: 90px">
                                                                    <div class="rating">
                                                                        @php
                                                                            $division = $value['Suma'] / $value['Promedio'];
                                                                            $redondear = round($division);
                                                                        @endphp
                                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div></a>
                                                                    </div>
                                                                    <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="rating_num">{{ $value['Promedio']}}</span></a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                        <div class="row" >
                                                            <ul class="list_none pr_action_btn" style="padding-left: 200px; display: flex;" >
                                                                <li style="padding: 10px"><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                                <li class="add-to-cart" style="padding: 10px" ><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            </ul>
                                                        </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="item">
                                        @foreach($hp_chunk[1] as $resp)
                                            @php
                                                $precioEntreMargen = number_format($resp->P1/.65,2);
                                                $precio = ($precioEntreMargen*$tc)*1.16;

                                                if($resp->Condicion == 1){
                                                    $condicion = 'Nuevo';
                                                }elseif($resp->Condicion == 2){
                                                    $condicion = 'Remanufacturado';
                                                }elseif($resp->Condicion == 3){
                                                    $condicion = 'Usado';
                                                }

                                                if($resp->Disponible > 0){
                                                    $existencia = 'Stock';
                                                }elseif($resp->Disponible == 0){
                                                    $existencia = 'Agotado';
                                                }
                                                // echo $precio;
                                            @endphp
                                            <div class="product_wrap">
                                                <div class="product_img">
                                                    <a href="{{ url('verDetalle/'.$resp->id) }}">
                                                        <img src="{{ $resp->ImageURL }}" alt="el_img5">
                                                        <img class="product_hover_img" src="{{ $resp->ImageURL }}" alt="el_hover_img5">
                                                    </a>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="{{ url('verDetalle/'.$resp->id) }}" id="aNombre">{{ $resp->Grupo }}-{{ $resp->Marcas }}</a></h6>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">{{ $resp->PartNo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500">{{ $existencia }}</span></a>
                                                        </div>
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><p>{{ $resp->Descripcion }}.</p></a>
                                                    </div>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color: red">{{ $resp->Tipo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:green">{{ $condicion }}</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-left: 5px">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">$ {{ number_format($precio,2) }} MXN</span></a>
                                                        @foreach($array as $value)
                                                            @if($value['product_id'] == $resp->id)
                                                                <div class="rating_wrap" style="margin-left: 90px">
                                                                    <div class="rating">
                                                                        @php
                                                                            $division = $value['Suma'] / $value['Promedio'];
                                                                            $redondear = round($division);
                                                                        @endphp
                                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div></a>
                                                                    </div>
                                                                    <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="rating_num">{{ $value['Promedio']}}</span></a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                        <div class="row" >
                                                            <ul class="list_none pr_action_btn" style="padding-left: 200px; display: flex;" >
                                                                <li style="padding: 10px"><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                                <li class="add-to-cart" style="padding: 10px" ><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            </ul>
                                                        </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="item">
                                        @foreach($hp_chunk[2] as $resp)
                                            @php
                                                $precioEntreMargen = number_format($resp->P1/.65,2);
                                                $precio = ($precioEntreMargen*$tc)*1.16;

                                                if($resp->Condicion == 1){
                                                    $condicion = 'Nuevo';
                                                }elseif($resp->Condicion == 2){
                                                    $condicion = 'Remanufacturado';
                                                }elseif($resp->Condicion == 3){
                                                    $condicion = 'Usado';
                                                }

                                                if($resp->Disponible > 0){
                                                    $existencia = 'Stock';
                                                }elseif($resp->Disponible == 0){
                                                    $existencia = 'Agotado';
                                                }
                                            @endphp
                                            <div class="product_wrap">
                                                <div class="product_img">
                                                    <a href="{{ url('verDetalle/'.$resp->id) }}">
                                                        <img src="{{ $resp->ImageURL }}" alt="el_img5">
                                                        <img class="product_hover_img" src="{{ $resp->ImageURL }}" alt="el_hover_img5">
                                                    </a>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="{{ url('verDetalle/'.$resp->id) }}" id="aNombre">{{ $resp->Grupo }}-{{ $resp->Marcas }}</a></h6>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">{{ $resp->PartNo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500">{{ $existencia }}</span></a>
                                                        </div>
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><p>{{ $resp->Descripcion }}.</p></a>
                                                    </div>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color: red">{{ $resp->Tipo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:green">{{ $condicion }}</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-left: 5px">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">$ {{ number_format($precio,2) }} MXN</span></a>
                                                        @foreach($array as $value)
                                                            @if($value['product_id'] == $resp->id)
                                                                <div class="rating_wrap" style="margin-left: 90px">
                                                                    <div class="rating">
                                                                        @php
                                                                            $division = $value['Suma'] / $value['Promedio'];
                                                                            $redondear = round($division);
                                                                        @endphp
                                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div></a>
                                                                    </div>
                                                                    <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="rating_num">{{ $value['Promedio']}}</span></a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                        <div class="row" >
                                                            <ul class="list_none pr_action_btn" style="padding-left: 200px; display: flex;" >
                                                                <li style="padding: 10px"><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                                <li class="add-to-cart" style="padding: 10px" ><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            </ul>
                                                        </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- LENOVO--}}
                    <div class="col-lg-4" style="padding-top: 58px">
                        <div class="row" >
                            <div class="col-12" >
                                <div class="heading_tab_header">
                                    <div class="heading_s2">
                                    </div>
                                    <div class="view_all">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 40px">
                            <div class="col-12">
                                <div class="product_slider carousel_slider product_list owl-carousel owl-theme nav_style5" data-nav="true" data-dots="false" data-loop="true" data-margin="20" data-responsive='{"0":{"items": "1"}, "380":{"items": "1"}, "640":{"items": "2"}, "991":{"items": "1"}}'>
                                    <div class="item">
                                        @foreach($lenovo_chunk[0] as $resp)
                                            @php
                                                $precioEntreMargen = number_format($resp->P1/.65,2);
                                                $precio = ($precioEntreMargen*$tc)*1.16;

                                                if($resp->Condicion == 1){
                                                    $condicion = 'Nuevo';
                                                }elseif($resp->Condicion == 2){
                                                    $condicion = 'Remanufacturado';
                                                }elseif($resp->Condicion == 3){
                                                    $condicion = 'Usado';
                                                }

                                                if($resp->Disponible > 0){
                                                    $existencia = 'Stock';
                                                }elseif($resp->Disponible == 0){
                                                    $existencia = 'Agotado';
                                                }
                                            @endphp
                                            <div class="product_wrap">
                                                <div class="product_img">
                                                    <a href="{{ url('verDetalle/'.$resp->id) }}">
                                                        <img src="{{ $resp->ImageURL }}" alt="el_img5">
                                                        <img class="product_hover_img" src="{{ $resp->ImageURL }}" alt="el_hover_img5">
                                                    </a>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="{{ url('verDetalle/'.$resp->id) }}" id="aNombre">{{ $resp->Grupo }}-{{ $resp->Marcas }}</a></h6>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">{{ $resp->PartNo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500">{{ $existencia }}</span></a>
                                                        </div>
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><p>{{ $resp->Descripcion }}.</p></a>
                                                    </div>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color: red">{{ $resp->Tipo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:green">{{ $condicion }}</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-left: 5px">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">$ {{ number_format($precio,2) }} MXN</span></a>
                                                        @foreach($array as $value)
                                                            @if($value['product_id'] == $resp->id)
                                                                <div class="rating_wrap" style="margin-left: 90px">
                                                                    <div class="rating">
                                                                        @php
                                                                            $division = $value['Suma'] / $value['Promedio'];
                                                                            $redondear = round($division);
                                                                        @endphp
                                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div></a>
                                                                    </div>
                                                                    <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="rating_num">{{ $value['Promedio']}}</span></a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                        <div class="row" >
                                                            <ul class="list_none pr_action_btn" style="padding-left: 200px; display: flex;" >
                                                                <li style="padding: 10px"><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                                <li class="add-to-cart" style="padding: 10px" ><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            </ul>
                                                        </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="item">
                                        @foreach($lenovo_chunk[1] as $resp)
                                            @php
                                                $precioEntreMargen = number_format($resp->P1/.65,2);
                                                $precio = ($precioEntreMargen*$tc)*1.16;

                                                if($resp->Condicion == 1){
                                                    $condicion = 'Nuevo';
                                                }elseif($resp->Condicion == 2){
                                                    $condicion = 'Remanufacturado';
                                                }elseif($resp->Condicion == 3){
                                                    $condicion = 'Usado';
                                                }

                                                if($resp->Disponible > 0){
                                                    $existencia = 'Stock';
                                                }elseif($resp->Disponible == 0){
                                                    $existencia = 'Agotado';
                                                }
                                            @endphp
                                            <div class="product_wrap">
                                                <div class="product_img">
                                                    <a href="{{ url('verDetalle/'.$resp->id) }}">
                                                        <img src="{{ $resp->ImageURL }}" alt="el_img5">
                                                        <img class="product_hover_img" src="{{ $resp->ImageURL }}" alt="el_hover_img5">
                                                    </a>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="{{ url('verDetalle/'.$resp->id) }}" id="aNombre">{{ $resp->Grupo }}-{{ $resp->Marcas }}</a></h6>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">{{ $resp->PartNo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500">{{ $existencia }}</span></a>
                                                        </div>
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><p>{{ $resp->Descripcion }}.</p></a>
                                                    </div>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color: red">{{ $resp->Tipo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:green">{{ $condicion }}</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-left: 5px">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">$ {{ number_format($precio,2) }} MXN</span></a>
                                                        @foreach($array as $value)
                                                            @if($value['product_id'] == $resp->id)
                                                                <div class="rating_wrap" style="margin-left: 90px">
                                                                    <div class="rating">
                                                                        @php
                                                                            $division = $value['Suma'] / $value['Promedio'];
                                                                            $redondear = round($division);
                                                                        @endphp
                                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div></a>
                                                                    </div>
                                                                    <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="rating_num">{{ $value['Promedio']}}</span></a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                        <div class="row" >
                                                            <ul class="list_none pr_action_btn" style="padding-left: 200px; display: flex;" >
                                                                <li style="padding: 10px"><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                                <li class="add-to-cart" style="padding: 10px" ><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            </ul>
                                                        </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="item">
                                        @foreach($lenovo_chunk[2] as $resp)
                                            @php
                                                $precioEntreMargen = number_format($resp->P1/.65,2);
                                                $precio = ($precioEntreMargen*$tc)*1.16;

                                                if($resp->Condicion == 1){
                                                    $condicion = 'Nuevo';
                                                }elseif($resp->Condicion == 2){
                                                    $condicion = 'Remanufacturado';
                                                }elseif($resp->Condicion == 3){
                                                    $condicion = 'Usado';
                                                }

                                                if($resp->Disponible > 0){
                                                    $existencia = 'Stock';
                                                }elseif($resp->Disponible == 0){
                                                    $existencia = 'Agotado';
                                                }
                                            @endphp
                                            <div class="product_wrap">
                                                <div class="product_img">
                                                    <a href="{{ url('verDetalle/'.$resp->id) }}">
                                                        <img src="{{ $resp->ImageURL }}" alt="el_img5">
                                                        <img class="product_hover_img" src="{{ $resp->ImageURL }}" alt="el_hover_img5">
                                                    </a>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="{{ url('verDetalle/'.$resp->id) }}" id="aNombre">{{ $resp->Grupo }}-{{ $resp->Marcas }}</a></h6>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">{{ $resp->PartNo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500">{{ $existencia }}</span></a>
                                                        </div>
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><p>{{ $resp->Descripcion }}.</p></a>
                                                    </div>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color: red">{{ $resp->Tipo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:green">{{ $condicion }}</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-left: 5px">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">$ {{ number_format($precio,2) }} MXN</span></a>
                                                        @foreach($array as $value)
                                                            @if($value['product_id'] == $resp->id)
                                                                <div class="rating_wrap" style="margin-left: 90px">
                                                                    <div class="rating">
                                                                        @php
                                                                            $division = $value['Suma'] / $value['Promedio'];
                                                                            $redondear = round($division);
                                                                        @endphp
                                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div></a>
                                                                    </div>
                                                                    <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="rating_num">{{ $value['Promedio']}}</span></a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                        <div class="row" >
                                                            <ul class="list_none pr_action_btn" style="padding-left: 200px; display: flex;" >
                                                                <li style="padding: 10px"><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                                <li class="add-to-cart" style="padding: 10px" ><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            </ul>
                                                        </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- DELL--}}
                    <div class="col-lg-4" style="padding-top: 58px">
                        <div class="row" >
                            <div class="col-12">
                                <div class="heading_tab_header">
                                    <div class="heading_s2">
                                    </div>
                                    <div class="view_all">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 40px">
                            <div class="col-12">
                                <div class="product_slider carousel_slider product_list owl-carousel owl-theme nav_style5" data-nav="true" data-dots="false" data-loop="true" data-margin="20" data-responsive='{"0":{"items": "1"}, "380":{"items": "1"}, "640":{"items": "2"}, "991":{"items": "1"}}'>
                                    <div class="item">
                                        @foreach($dell_chunk[0] as $resp)
                                           @php
                                                $precioEntreMargen = number_format($resp->P1/.65,2);
                                                $precio = ($precioEntreMargen*$tc)*1.16;

                                                if($resp->Condicion == 1){
                                                    $condicion = 'Nuevo';
                                                }elseif($resp->Condicion == 2){
                                                    $condicion = 'Remanufacturado';
                                                }elseif($resp->Condicion == 3){
                                                    $condicion = 'Usado';
                                                }

                                                if($resp->Disponible > 0){
                                                    $existencia = 'Stock';
                                                }elseif($resp->Disponible == 0){
                                                    $existencia = 'Agotado';
                                                }
                                            @endphp
                                            <div class="product_wrap">
                                                <div class="product_img">
                                                    <a href="{{ url('verDetalle/'.$resp->id) }}">
                                                        <img src="{{ $resp->ImageURL }}" alt="el_img5">
                                                        <img class="product_hover_img" src="{{ $resp->ImageURL }}" alt="el_hover_img5">
                                                    </a>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="{{ url('verDetalle/'.$resp->id) }}" id="aNombre">{{ $resp->Grupo }}-{{ $resp->Marcas }}</a></h6>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">{{ $resp->PartNo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500">{{ $existencia }}</span></a>
                                                        </div>
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><p>{{ $resp->Descripcion }}.</p></a>
                                                    </div>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color: red">{{ $resp->Tipo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:green">{{ $condicion }}</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-left: 5px">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">$ {{ number_format($precio,2) }} MXN</span></a>
                                                        @foreach($array as $value)
                                                            @if($value['product_id'] == $resp->id)
                                                                <div class="rating_wrap" style="margin-left: 90px">
                                                                    <div class="rating">
                                                                        @php
                                                                            $division = $value['Suma'] / $value['Promedio'];
                                                                            $redondear = round($division);
                                                                        @endphp
                                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div></a>
                                                                    </div>
                                                                    <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="rating_num">{{ $value['Promedio']}}</span></a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                        <div class="row" >
                                                            <ul class="list_none pr_action_btn" style="padding-left: 200px; display: flex;" >
                                                                <li style="padding: 10px"><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                                <li class="add-to-cart" style="padding: 10px" ><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            </ul>
                                                        </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="item">
                                        @foreach($dell_chunk[1] as $resp)
                                           @php
                                                $precioEntreMargen = number_format($resp->P1/.65,2);
                                                $precio = ($precioEntreMargen*$tc)*1.16;

                                                if($resp->Condicion == 1){
                                                    $condicion = 'Nuevo';
                                                }elseif($resp->Condicion == 2){
                                                    $condicion = 'Remanufacturado';
                                                }elseif($resp->Condicion == 3){
                                                    $condicion = 'Usado';
                                                }

                                                if($resp->Disponible > 0){
                                                    $existencia = 'Stock';
                                                }elseif($resp->Disponible == 0){
                                                    $existencia = 'Agotado';
                                                }
                                            @endphp
                                            <div class="product_wrap">
                                                <div class="product_img">
                                                    <a href="{{ url('verDetalle/'.$resp->id) }}">
                                                        <img src="{{ $resp->ImageURL }}" alt="el_img5">
                                                        <img class="product_hover_img" src="{{ $resp->ImageURL }}" alt="el_hover_img5">
                                                    </a>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="{{ url('verDetalle/'.$resp->id) }}" id="aNombre">{{ $resp->Grupo }}-{{ $resp->Marcas }}</a></h6>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">{{ $resp->PartNo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500">{{ $existencia }}</span></a>
                                                        </div>
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><p>{{ $resp->Descripcion }}.</p></a>
                                                    </div>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color: red">{{ $resp->Tipo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:green">{{ $condicion }}</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-left: 5px">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">$ {{ number_format($precio,2) }} MXN</span></a>
                                                        @foreach($array as $value)
                                                            @if($value['product_id'] == $resp->id)
                                                                <div class="rating_wrap" style="margin-left: 90px">
                                                                    <div class="rating">
                                                                        @php
                                                                            $division = $value['Suma'] / $value['Promedio'];
                                                                            $redondear = round($division);
                                                                        @endphp
                                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div></a>
                                                                    </div>
                                                                    <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="rating_num">{{ $value['Promedio']}}</span></a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                        <div class="row" >
                                                            <ul class="list_none pr_action_btn" style="padding-left: 200px; display: flex;" >
                                                                <li style="padding: 10px"><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                                <li class="add-to-cart" style="padding: 10px" ><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            </ul>
                                                        </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="item">
                                        @foreach($dell_chunk[2] as $resp)
                                            @php
                                                $precioEntreMargen = number_format($resp->P1/.65,2);
                                                $precio = ($precioEntreMargen*$tc)*1.16;

                                                if($resp->Condicion == 1){
                                                    $condicion = 'Nuevo';
                                                }elseif($resp->Condicion == 2){
                                                    $condicion = 'Remanufacturado';
                                                }elseif($resp->Condicion == 3){
                                                    $condicion = 'Usado';
                                                }

                                                if($resp->Disponible > 0){
                                                    $existencia = 'Stock';
                                                }elseif($resp->Disponible == 0){
                                                    $existencia = 'Agotado';
                                                }
                                            @endphp
                                            <div class="product_wrap">
                                                <div class="product_img">
                                                    <a href="{{ url('verDetalle/'.$resp->id) }}">
                                                        <img src="{{ $resp->ImageURL }}" alt="el_img5">
                                                        <img class="product_hover_img" src="{{ $resp->ImageURL }}" alt="el_hover_img5">
                                                    </a>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="{{ url('verDetalle/'.$resp->id) }}" id="aNombre">{{ $resp->Grupo }}-{{ $resp->Marcas }}</a></h6>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">{{ $resp->PartNo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500">{{ $existencia }}</span></a>
                                                        </div>
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><p>{{ $resp->Descripcion }}.</p></a>
                                                    </div>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color: red">{{ $resp->Tipo }}</span></a>
                                                        <div class="on_sale">
                                                            <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:green">{{ $condicion }}</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-left: 5px">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">$ {{ number_format($precio,2) }} MXN</span></a>
                                                        @foreach($array as $value)
                                                            @if($value['product_id'] == $resp->id)
                                                                <div class="rating_wrap" style="margin-left: 90px">
                                                                    <div class="rating">
                                                                        @php
                                                                            $division = $value['Suma'] / $value['Promedio'];
                                                                            $redondear = round($division);
                                                                        @endphp
                                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div></a>
                                                                    </div>
                                                                    <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="rating_num">{{ $value['Promedio']}}</span></a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                        <div class="row" >
                                                            <ul class="list_none pr_action_btn" style="padding-left: 200px; display: flex;" >
                                                                <li style="padding: 10px"><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                                <li class="add-to-cart" style="padding: 10px" ><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            </ul>
                                                        </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SECTION SHOP -->
    </div>
@endsection


@section('script')
    <script>
        $('#myCarousel').on('slide.bs.carousel', function () {
        // do something…
        })
         $(document).ready(function() {

              $('#msgAlert').fadeIn();
                setTimeout(function() {
                    $("#msgAlert").fadeOut();
                },2000);
         })
    </script>
@endsection
