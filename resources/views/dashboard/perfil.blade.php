@extends('layout.mainlayout')

@section('content')
@include('partials.menudashboard')
{{-- <br><br><br><br><br><b><br><br><br><br><br><br><br> --}}
<div class="mt-4 staggered-animation-wrap">
    <div class="container" style="margin-top: -80px" >
        {!! Form::open(['url'=>'guardarPerfil']) !!}
            <div class="row" style="margin-left: 900px">
                <button class="btn btn-fill-out" type="submit">Guardar</button>
            </div>
            <div class="col-lg-8 col-lg-offset-8">
                <div class="row" style="margin-left: 350px">
                    <div class="col-md-8" >
                        {!! Form::open(['url'=>'guardarDomicilio']) !!}
                        <div class="form-group">
                            {!! Form::label('Nombre', 'Nombre Completo') !!}
                            {!! Form::text('Nombre',"$user->name",['class'=>'form-control', 'required', 'placeholder'=>'Ingresa tu nombre'] ) !!}
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left: 350px">
                    <div class="col-md-8" >
                        <div class="form-group">
                            {!! Form::label('Email', 'Correo Electrónico') !!}
                            {!! Form::text('Email',"$user->email",['class'=>'form-control', 'required', 'placeholder'=>'Ingresa tu correo electrónico'] ) !!}
                        </div>
                    </div>
                </div>
                @if($user->RedesSociales == 0)
                    <div class="row" style="margin-left: 350px">
                        <div class="col-md-8">
                            <div class="form-group">
                                {!! Form::label('PasswordN', 'Password Nueva') !!}
                                {!! Form::password('PasswordN',['class'=>'form-control', 'id'=>'passNew', 'placeholder'=>'Ingresa tu Contraseña nueva si deseas cambiar la actual'] ) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 350px">
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="alert alert-success" id="alert" role="alert">
                                    Las contraseñas no coinciden
                                </div>
                                {!! Form::label('PasswordN2', 'Confirmar Password Nueva') !!}
                                {!! Form::password('PasswordN2',['class'=>'form-control', 'id'=>'passConfirm', 'placeholder'=>'Confirma tu nueva contraseña '] ) !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        {!! Form::close() !!}
    </div>
    <br><br>
</div>
@endsection

@section('script')
    <script>
         $(document).ready(function(){
            $('#alert').hide();
        })
        $('#passConfirm').on('change',function(){
            var pass2 = $('#passConfirm').val();
            var pass = $('#passNew').val();

            if(pass != pass2){
                $('#alert').show();
            }else{
                $('#alert').hide();
            }
        })
    </script>
@endsection
