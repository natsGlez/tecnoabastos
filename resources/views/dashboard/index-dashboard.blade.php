@extends('layout.mainlayout')

@section('content')
@include('partials.menudashboard')
<div class="mt-4 staggered-animation-wrap">
    <div class="container" >
        {{-- <br><br><br><br><br><br><br><br><br><br>--}}
        <div class="row" style="margin-left: 400px">

            <p>
                Hola {{ Auth::user()->name }} (¿No eres {{ Auth::user()->name }} ?
                    <a  href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        {{ __('Cerrar Sesión') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                )
            </p>
            <p>
                Desde el panel de control de tu cuenta puedes ver tus pedidos recientes, gestionar tus direcciones de envío,
                descargar tus facturas y editar tu contraseña y los detalles de tu cuenta.
            </p>
        </div>
        <br><br> <br><br> 
    </div>
</div>
@endsection
