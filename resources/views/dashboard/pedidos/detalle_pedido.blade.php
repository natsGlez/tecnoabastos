@extends('layout.mainlayout')

@section('content')
@include('partials.menudashboard')
{{-- <br><br><br><br><br><br><br><br><br><br> --}}
<div class="mt-4 staggered-animation-wrap">
    <div class="container">
        <div class="row " style="margin-left: 350px; margin-top: -100px">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Número de Pedido</th>
                            <th>Fecha</th>
                            <th>Email</th>
                            <th>Total</th>
                            <th>Método de Pago</th>
                            <th>Ficha de Pago</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $respuesta->id }}</td>
                            <td>{{ $respuesta->Fecha }}</td>
                            <td>{{ $pedido->user->email }}</td>
                            <td>${{ number_format($respuesta->Total,2) }}</td>
                            <td>{{ $respuesta->TipoDePago }}</td>
                            @if($respuesta->Extra == null)
                                <td>
                                    <button data-toggle="modal" style="margin-left: auto; float: left" data-target="#pago" data-whatever="{{ $respuesta->id }}" class="btn btn-secondary">Adjuntar Pago</button>
                                </td>
                            @elseif($respuesta->Extra != null)
                                <td>
                                    <img width="100" src="data: {!! $respuesta->Extra->FichaDePago->mime !!};base64, {!! $respuesta->Extra->FichaDePago->ImagenB64!!} "  alt="">
                                </td>
                            @endif
                        </tr>
                    </tbody>
                </table>
            </div>
            <p style="margin-top: 15px">Sube tu ficha de pago. Tienes un plazo máximo de dos días hábiles después de haber
                    levantado el pedido para realizar el pago.</p>
            <div class="col-md-8">
                <br><br>
                <div class="order_review">
                    <div class="heading_s1">
                        <h4>Detalle del Pedido</h4>
                    </div>
                    <div class="table-responsive order_table">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Núm. Parte/Cantidad</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($respuesta->Partidas as $producto)
                                    <tr>
                                        <td>
                                            {{-- <div class="comment_img"> --}}
                                                <img src="{{ asset($producto->ImagenUrl) }}" />
                                            {{-- </div> --}}
                                        </td>
                                        <td>{{ $producto->idProducto }} <span class="product-qty">x {{ $producto->Cantidad }}</span></td>
                                        <td>${{ $producto->PrecioUnitario }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                {{-- <tr>
                                    <th>SubTotal</th>
                                    <td class="product-subtotal">${{ $arrayTotal }}</td>
                                </tr> --}}
                                {{-- <tr>
                                    <th>Gastos de Envío</th>
                                    <td>${{ $envio }}</td>
                                </tr> --}}
                                    {{-- @php
                                    $total = $arrayTotal + $envio;
                                @endphp --}}
                                <tr>
                                    <th> </th>
                                    <th align="right">Total</th>
                                    <td class="product-subtotal">${{ number_format($respuesta->Total,2) }}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        {{-- <div class="row">
            <div class="col-md-6">
                <h5 style=" margin-left: 360px">Datos de Envío</h5>
                <table class="table thead table-bordered thead td" style="width: 50%; margin-top: 10px; margin-left: 360px">
                    <thead class="table-secondary">
                        <tr>
                            <th>Producto</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody class="table-bordered td" >
                        @php
                            $total = 0;
                        @endphp
                        @foreach($arrayCart as $id => $cart)
                            <tr>
                                <td># {{ $cart->PartNo }}</td>
                                <td>$ {{ $cart->Precio }}</td>
                            </tr>
                            @php
                                $total = $cart->Precio + $total;
                            @endphp
                        @endforeach
                        <tr>
                            <td>TOTAL</td>
                            <td>$ {{ $total }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <table class="table thead table-bordered thead td" style="width: 50%; margin-top: 40px; margin-left: 250px">
                    <h5>Detalles del pedido</h5>
                    <thead class="table-secondary">
                        <tr>
                            <th>Producto</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody class="table-bordered td">
                        @php
                            $total = 0;
                        @endphp
                        @foreach($arrayCart as $id => $cart)
                            <tr>
                                <td># {{ $cart->PartNo }}</td>
                                <td>$ {{ $cart->Precio }}</td>
                            </tr>
                            @php
                                $total = $cart->Precio + $total;
                            @endphp
                        @endforeach
                        <tr>
                            <td>TOTAL</td>
                            <td>$ {{ $total }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div> --}}
        <div class="modal fade" id="pago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar Dirección</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="margin: 50px">
                        {!! Form::open(['url'=>'subirFicha','files'=>true]) !!}
                            {!! Form::file('fichaPago', ['class'=>'form-control']) !!}
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="idPedido" name="idPedido">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-fill-out">Guardar Cambios</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
         $('#pago').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('whatever');
            $('#idPedido').val(id);
            // alert(id)
        })
    </script>
@endsection
