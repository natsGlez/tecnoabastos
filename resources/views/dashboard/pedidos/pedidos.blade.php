@extends('layout.mainlayout')

@section('content')
@include('partials.menudashboard')
{{-- <br><br><br><br><br><br><br><br><br><br><br> --}}
<div class="mt-4 staggered-animation-wrap">


        <div class="container" style="margin-bottom: 200px">
            @if(session('status'))
                <div class="alert alert-success" id="msgAlert" style="width: 800px; margin-left: 400px; margin-top: -100px; margin-bottom: 100px" id="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row " style="margin-left: 350px">
                <table class="table thead table-bordered thead td "  style="margin-top: -90px">
                    <thead class="table-secondary">
                        <tr>
                            <th>Pedido</th>
                            <th>Fecha</th>
                            <th>Estatus</th>
                            <th>Total</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-bordered td">
                        @foreach($pedidos as $id => $pedido)
                            <tr>
                                <td>#{{ $pedido->id_pedidoBonance }}</td>
                                <td>{{ $pedido->created_at }}</td>
                                <td>{{ $pedido->estatus->Nombre }}</td>
                                <td>$ {{ number_format($pedido->Total, 2) }}</td>
                                <td>
                                    <a class="btn btn-secondary" href="{{ url('detallePedido/'.$pedido->id_pedidoBonance) }}">Ver</a>
                                    {{-- <a type="button" data-toggle="modal" data-target="#pago" data-whatever="{{ $pedido->id_pedidoBonance }}">Editar</a> --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

</div>
@endsection
@section('script')
    <script>
         $('#pago').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('whatever');
            $('#idPedido').val(id);
            // alert(id)
        })

        $(document).ready(function() {
              $('#msgAlert').fadeIn();
                setTimeout(function() {
                    $("#msgAlert").fadeOut();
                },2000);
        })
    </script>
@endsection
