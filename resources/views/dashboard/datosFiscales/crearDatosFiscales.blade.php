@extends('layout.mainlayout')

@section('content')
@include('partials.menudashboard')
    <div class="mt-4 staggered-animation-wrap">
        <div class="container">
            {!! Form::open(['url'=>'guardarDatosFiscales']) !!}
                @if(session('status'))
                    <div class="alert alert-success" id="msgAlert" style="width: 500px; margin-left: 350px" id="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="form-group col-md-5" style="margin-left: 350px" >
                    {!! Form::label('Titulo', 'Título') !!}
                    {!! Form::text('Titulo', '', ['class'=>'form-control']) !!}
                </div>
                <div class="row">
                    <div class="col-md-2" style="margin-left: 360px">
                        {!! Form::open(['url'=>'guardarDomicilio']) !!}
                        <div class="form-group">
                            {!! Form::label('RFC', 'RFC') !!}
                            {!! Form::text('RFC','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa tu RFC']) !!}
                        </div>
                    </div>
                    <div class="col-md-3" style="margin-left: 10px">
                        <div class="form-group">
                            {!! Form::label('RazonSocial', 'Razón Social ') !!}
                            {!! Form::text('RazonSocial','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa tu Razón Social '] ) !!}
                        </div>
                    </div>
                    <div class="col-md-3" style="margin-left: 10px">
                        <div class="form-group">
                            {!! Form::label('UsoCFDI', 'CFDI') !!}
                            {!! Form::select('USOCFDI',$cfdi,'',['class'=>'form-control',  'placeholder'=>'Ingresa el uso del CFDI '] ) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3" style="margin-left: 360px">
                        <div class="form-group">
                            {!! Form::label('MetodoPago', 'Método de Pago') !!}
                            {!! Form::select('MetodoPago',$metodo,'',['class'=>'form-control', 'placeholder'=>'Ingresa tu Método de Pago '] ) !!}
                        </div>
                    </div>
                    <div class="col-md-3" style="margin-left: 10px">
                        <div class="form-group">
                            {!! Form::label('FormaPago', 'Forma de Pago') !!}
                            {!! Form::select('FormaPago',$forma,'',['class'=>'form-control',  'placeholder'=>'Ingresa tu Forma de Pago '] ) !!}
                        </div>
                    </div>
                </div>
                <button class="btn btn-fill-out" style="margin-left: 1000px" type="submit">Guardar</button>
            {!! Form::close() !!}
        </div>
        <br><br><br>
    </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        setTimeout(function() {
            $("#msgAlert").fadeOut();
        },3500);
    })
</script>
@endsection
