@extends('layout.mainlayout')

@section('content')
@include('partials.menudashboard')
<br><br>
<div class="mt-4 staggered-animation-wrap" >
    <div class="container" style="margin-bottom: 200px">
        <a href="{{ url('crearDatosFiscales') }}" style="margin-left: 900px; margin-top: -200px" class="btn btn-fill-out">Crear Datos de Facturación</a>
        <br>
        <div class="row" style="padding-left: 330px">
            @if($datosFiscales == null)
            <br><br>
                <a href="{{ url('crearDatosFiscales') }}" style="margin-left: 900px; margin-top: -200px" class="btn btn-fill-out">Crear Datos de Facturación</a>
            <br><br><br><br>
            @else
                @foreach($datosFiscales as $id => $datoFactura)
                    <div class="col-md-4" style="margin-right: 100px; margin-top: -50px">
                        <div class="card" style="width: 23rem; margin-bottom: 30px;" >
                            <div class="card-header">
                                Dirección Fiscal {{ $id }} &nbsp &nbsp &nbsp &nbsp<a type="button" data-toggle="modal" data-target="#exampleModal" data-whatever="{{ $datoFactura->id }}">Editar</a><a type="button" href="{{ url('eliminarDatosFiscales/'.$datoFactura->id) }}">&nbsp &nbsp &nbsp<i class="icon-close"></i></a>
                            </div>

                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Razón Social: {{ $datoFactura->RazonSocial }}</li>
                                <li class="list-group-item">RFC: {{ $datoFactura->RFC }}</li>
                                <li class="list-group-item">CFDI: {{ $datoFactura->CFDI }}</li>
                                <li class="list-group-item">Método de Pago: {{ $datoFactura->MetodoPago }}</li>
                                <li class="list-group-item">Forma de Pago: {{ $datoFactura->FormaPago }}</li>
                            </ul>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar Dirección</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="margin: 50px">
                        {!! Form::open(['url'=>'actualizarDatosFiscales']) !!}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('RazonSocial', 'Razón Social ') !!}
                                        {!! Form::text('RazonSocial','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa tu Razón Social '] ) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"  style="margin-right: 15px">
                                        {!! Form::label('RFC', 'RFC') !!}
                                        {!! Form::text('RFC','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa tu RFC'] ) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"  style="margin-left: 15px">
                                        {!! Form::label('UsoCFDI', 'CFDI') !!}
                                        {!! Form::select('USOCFDI',$cfdi,'',['class'=>'form-control','id'=>'CFDI' ,'placeholder'=>'Ingresa el uso del CFDI '] ) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" >
                                        {!! Form::label('MetodoPago', 'Método de Pago') !!}
                                        {!! Form::select('MetodoPago',$metodo,'',['class'=>'form-control', 'id'=>'MetodoPago','placeholder'=>'Ingresa tu Método de Pago '] ) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" >
                                        {!! Form::label('FormaPago', 'Forma de Pago') !!}
                                        {!! Form::select('FormaPago',$forma,'',['class'=>'form-control','id'=>'FormaPago',  'placeholder'=>'Ingresa tu Forma de Pago '] ) !!}
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="id" name="id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button class="btn btn-fill-out" type="submit">Guardar</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('whatever');

            $.get('{{ url("getData") }}/'+id,function(data){
                $('#RFC').val(data.RFC);
                $('#RazonSocial').val(data.RazonSocial);
                $('#CFDI').val(data.CFDI);
                $('#MetodoPago').val(data.MetodoPago);
                $('#FormaPago').val(data.FormaPago);
                $('#id').val(id);
            })
        })
   </script>
@endsection
