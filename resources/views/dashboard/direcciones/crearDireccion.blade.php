@extends('layout.mainlayout')

@section('content')
@include('partials.menudashboard')
<br><br>
<div class="mt-4 staggered-animation-wrap">
    <div class="container">
        {!! Form::open(['url'=>'guardarDomicilio']) !!}
            <div class="row" style="margin-top: -100px">
                <div class="col-md-3" style="margin-left: 360px">
                    <div class="form-group" >
                        {!! Form::label('Titulo', 'Titulo para identificar el domicilio') !!}
                        {!! Form::text('Titulo', '', ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-3" style="margin-left: 10px">
                    <div class="form-group" >
                        {!! Form::label('Contacto', 'Quien recibe') !!}
                        {!! Form::text('Contacto', '', ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row"  >
                <div class="col-md-4" style="margin-left: 360px">
                    <div class="form-group">
                        {!! Form::label('Calle', 'Domicilio') !!}
                        {!! Form::text('Calle','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa la calle'] ) !!}
                    </div>
                </div>
                <div class="col-md-2" style="margin-left: 10px">
                    <div class="form-group">
                        {!! Form::label('NumEL', 'Número Exterior') !!}
                        {!! Form::text('NumE','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa el número exterior '] ) !!}
                    </div>
                </div>
                <div class="col-md-2" style="margin-left: 10px">
                    <div class="form-group">
                        {!! Form::label('NumIL', 'Número Interior') !!}
                        {!! Form::text('NumI','',['class'=>'form-control',  'placeholder'=>'Ingresa el número interior '] ) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2" style="margin-left: 360px">
                    <div class="form-group">
                        {!! Form::label('EstadoL', 'Estado') !!}
                        <select class="form-control" name="Estado" id="Est">
                            @foreach($code as $id => $cd)
                                <option value="{{ $cd->Estado  }}" >{{ $cd->Estado  }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3" style="margin-left: 10px">
                    <div class="form-group">
                        {!! Form::label('Municipio', 'Localidad/Municipio') !!}
                        {!! Form::select('Municipio',[],'',['class'=>'form-control', 'id'=>'Municipio'] ) !!}
                    </div>
                </div>
                <div class="col-md-3" style="margin-left: 10px">
                    <div class="form-group">
                        {!! Form::label('Ciudad', 'Ciudad') !!}
                        {!! Form::text('Ciudad','',['class'=>'form-control', 'id'=>'Ciudad'] ) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2" style="margin-left: 360px">
                    <div >
                        {!! Form::label('CP', 'Código Postal') !!}
                        {!! Form::select('CP',[],'', ['class'=>'form-control','id'=>'CP']) !!}
                    </div>
                </div>
                <div class="col-md-3" style="margin-left: 10px">
                    <div class="form-group">
                        {!! Form::label('ColoniaL', 'Colonia') !!}
                        {!! Form::select('Colonia',[],'',['class'=>'form-control', 'id'=>'Col'] ) !!}
                    </div>
                </div>
                <div class="col-md-3" style="margin-left: 10px">
                    <div >
                        {!! Form::label('Telefono', 'Teléfono') !!}
                        {!! Form::text('Telefono','', ['class'=>'form-control','id'=>'CP']) !!}
                    </div>
                </div>
                <div class="col-md-4" style="margin-left: 360px">
                    <div class="form-group" >
                        {!! Form::label('Referencias', 'Referencias') !!}
                        {!! Form::text('Referencias', '', ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
        <button class="btn btn-fill-out" style="margin-left: 1000px" type="submit">Guardar</button>
        {!! Form::close() !!}
        <br><br>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('document').ready(function(){
            $("#Est").prepend("<option value='' selected='selected'>Selecciona</option>");
        })
        $('#Est').on('change',function(){
            var cp= $('#Est').val();
            // cp = parseInt(cp)+1;
            // alert(cp)
            $.get('{{ url("getMunicipio") }}/'+cp,function(val){
                $('#Municipio').empty();
                $("#Municipio").prepend("<option value='' selected='selected'>Selecciona </option>");
                for(var i=0; i<val.length; i++){
                    $("#Municipio").append('<option value="'+val[i].Municipio+'">'+val[i].Municipio+'</option>');
                }
            })
        })
        $('#Municipio').on('change',function(){
            var cp= $('#Est').val();
            // cp = parseInt(cp)+1;
            // alert(cp)
             $.get('{{ url("getCodigoPostal") }}/'+cp,function(val){
                $('#CP').empty();
                $("#CP").prepend("<option value='' selected='selected'>Selecciona</option>");
                for(var i=0; i<val.length; i++){
                    $("#CP").append('<option value="'+val[i].CodigoPostal+'">'+val[i].CodigoPostal+'</option>');
                }
            })
        })
        $('#CP').on('change',function(){
            var cp= $('#CP').val();
            // alert(cp)
             $.get('{{ url("getColonia") }}/'+cp,function(val){
                $('#Col').empty();
                 $("#Col").prepend("<option value='' selected='selected'>Selecciona</option>");
                for(var i=0; i<val.length; i++){
                    $("#Col").append('<option value="'+val[i].Asentamiento+'">'+val[i].Asentamiento+'</option>');
                }
            })
        })
    </script>
@endsection
