@extends('layout.mainlayout')

@section('content')
@include('partials.menudashboard')
{{-- <br><br><br><br><br><br><br><br><br><br><br> --}}
<div class="mt-4 staggered-animation-wrap">
    <div class="container" style="margin-bottom: 100px; margin-top: -100px">
        <br>
        <div class="row" style="padding-left: 330px">
            @if(count($domicilios) == 0)
                <a href="{{ url('crearDomicilio') }}" style="margin-left: 350px; margin-bottom: 200px"class="btn btn-fill-out">Nuevo Domicilio</a>
            @else
                    <a href="{{ url('crearDomicilio') }}" style="margin-left: 700px; margin-bottom: 30px"class="btn btn-fill-out">Nuevo Domicilio</a>
                @foreach($domicilios as $id => $domicilio)
                    {{-- @if(($loop->iteration % 2) == 1) --}}
                        <div class="col-md-4">
                    {{-- @endif --}}
                            <div class="card" style="width: 18rem;margin-bottom: 30px" >
                                <div class="card-header">
                                    {{ $domicilio->Nombre }} &nbsp &nbsp &nbsp &nbsp<a type="button" data-toggle="modal" data-target="#exampleModal" data-whatever="{{ $domicilio->id }}">Editar</a><a type="button" href="{{ url('eliminarDomicilio/'.$domicilio->id) }}">&nbsp &nbsp &nbsp<i class="icon-close"></i></a>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Calle: {{ $domicilio->Calle }} {{ $domicilio->NumeroExterior }} {{ $domicilio->NumeroInterior }}</li>
                                    <li class="list-group-item">Colonia: {{ $domicilio->Colonia }}</li>
                                    <li class="list-group-item">Estado: {{ $domicilio->Estado }}</li>
                                    <li class="list-group-item">Municipio: {{ $domicilio->Municipio }}</li>
                                    <li class="list-group-item">CP: {{ $domicilio->CP }}</li>
                                    <li class="list-group-item">Ciudad: {{ $domicilio->Ciudad }}</li>
                                    <li class="list-group-item">Contacto: {{ $domicilio->Contacto }}</li>
                                    <li class="list-group-item">Referencias: {{ $domicilio->Referencia }}</li>
                                    <li class="list-group-item">Teléfono: {{ $domicilio->Telefono }}</li>
                                </ul>
                            </div>
                    {{-- @if(($loop->iteration % 2) == 0) --}}
                            {{-- <br> --}}
                        </div>
                    {{-- @endif --}}
                @endforeach
            @endif
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar Dirección</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="margin: 50px">
                        {!! Form::open(['url'=>'actualizarDomicilio']) !!}
                             <div class="row" style="margin-top: -30px">
                                <div class="col-md-12">
                                    <div class="control-form">
                                        {!! Form::label('Titulo', 'Titulo') !!}
                                        {!! Form::text('Titulo', '', ['class'=>'form-control','id'=>'Titulo']) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="control-form">
                                        {!! Form::label('Contacto', 'Contacto') !!}
                                        {!! Form::text('Contacto', '', ['class'=>'form-control','id'=>'Contacto']) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="control-form">
                                        {!! Form::label('Referencias', 'Referencias') !!}
                                        {!! Form::text('Referencias', '', ['class'=>'form-control','id'=>'Referencia']) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="control-form">
                                        {!! Form::label('Telefono', 'Teléfono') !!}
                                        {!! Form::text('Telefono', '', ['class'=>'form-control','id'=>'Telefono']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('Calle', 'Domicilio') !!}
                                        {!! Form::text('Calle','',['class'=>'form-control', 'id'=>'Calle','required', 'placeholder'=>'Ingresa la calle'] ) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"  style="margin-right: 15px">
                                        {!! Form::label('NumEL', 'Número Exterior') !!}
                                        {!! Form::text('NumE','',['class'=>'form-control', 'required', 'id'=>'NumE','placeholder'=>'Ingresa el número exterior '] ) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"  style="margin-left: 15px">
                                        {!! Form::label('NumIL', 'Número Interior') !!}
                                        {!! Form::text('NumI','',['class'=>'form-control', 'id'=>'NumI', 'placeholder'=>'Ingresa el número interior '] ) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" >
                                    <div class="form-group">
                                        {!! Form::label('EstadoL', 'Estado') !!}
                                        <select class="form-control" name="Estado" id="Est">
                                            @foreach($code as $id => $cd)
                                                    <option value="{{ $cd->Estado  }}" >{{ $cd->Estado  }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Municipio', 'Localidad/Municipio') !!}
                                        {!! Form::select('Municipio',[],'',['class'=>'form-control', 'id'=>'Municipio'] ) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Ciudad', 'Ciudad') !!}
                                        {!! Form::text('Ciudad','',['class'=>'form-control', 'id'=>'Ciudad'] ) !!}
                                    </div>
                                    <div >
                                        {!! Form::label('CP', 'Código Postal') !!}
                                        {!! Form::select('CP',[],'', ['class'=>'form-control','id'=>'CP']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('ColoniaL', 'Colonia') !!}
                                        {!! Form::select('Colonia',[],'',['class'=>'form-control', 'id'=>'Col'] ) !!}
                                    </div>
                                        <input type="hidden" name="id" id="id">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-fill-out">Guardar Cambios</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('whatever');
            // alert(id)
            $.get('{{ url("getDataDomicilio") }}/'+id,function(data){
            // alert(data.Calle)
                $('#Calle').val(data.Calle);
                $('#NumE').val(data.NumeroExterior);
                $('#NumI').val(data.NumeroInterior);
                $('#Col').val(data.Colonia);
                $('#Est').val(data.Estado);
                $('#Municipio').val(data.Municipio);
                $('#Contacto').val(data.Contacto);
                $('#Ciudad').val(data.Ciudad);
                $('#Referencia').val(data.Referencia);
                $('#Telefono').val(data.Telefono);
                $('#CP').val(data.CP);
                $('#id').val(id);
                $('#Titulo').val(data.Nombre);
            })
        })
        $('#Est').on('change',function(){
            var cp= $('#Est').val();
            // cp = parseInt(cp)+1;
            // alert(cp)
            $.get('{{ url("getMunicipio") }}/'+cp,function(val){
                $('#Municipio').empty();
                for(var i=0; i<val.length; i++){
                    $("#Municipio").append('<option value="'+val[i].Municipio+'">'+val[i].Municipio+'</option>');
                }
            })
        })
        $('#Municipio').on('change',function(){
            var cp= $('#Est').val();
            // cp = parseInt(cp)+1;
            // alert(cp)
             $.get('{{ url("getCodigoPostal") }}/'+cp,function(val){
                $('#CP').empty();
                for(var i=0; i<val.length; i++){
                    $("#CP").append('<option value="'+val[i].CodigoPostal+'">'+val[i].CodigoPostal+'</option>');
                }
            })
        })
        $('#CP').on('change',function(){
            var cp= $('#CP').val();
            // alert(cp)
             $.get('{{ url("getColonia") }}/'+cp,function(val){
                // $('#CP').empty();
                for(var i=0; i<val.length; i++){
                    $("#Col").append('<option value="'+val[i].Asentamiento+'">'+val[i].Asentamiento+'</option>');
                }
            })
        })
    </script>
@endsection
