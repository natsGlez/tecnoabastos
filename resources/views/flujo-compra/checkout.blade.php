@extends('layout.mainlayout')

@section('content')
<div class="mt-4 staggered-animation-wrap">
    <!-- START SECTION BREADCRUMB -->
    <div class="breadcrumb_section bg_gray page-title-mini">
        <div class="container"><!-- STRART CONTAINER -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="page-title">
                        <h1>Checkout</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Checkout</li>
                    </ol>
                </div>
            </div>
        </div><!-- END CONTAINER-->
    </div>
    <!-- END SECTION BREADCRUMB -->

    <!-- START MAIN CONTENT -->
    <div class="main_content">

        <!-- START SECTION SHOP -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="medium_divider"></div>
                        <div class="divider center_icon"><i class="linearicons-credit-card"></i></div>
                        <div class="medium_divider"></div>
                    </div>
                </div>
                    @if(session('status'))
                        <div class="alert alert-success" id="msgAlert" style="width: 1000px;" id="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="row">
                            <div class="heading_s1">
                                <h4>Datos de Envío y Facturación</h4>
                            </div>
                            @if(count($direcciones) > 0)
                                <div class="form-group col-md-8">
                                    {!! Form::label('Direccion', 'Dirección ') !!}
                                    <select class="form-control" name="Direccion" id="Dire">
                                        @foreach($direcciones as $id => $dire)
                                                <option value="{{ $dire->id  }}" >{{ $dire->Nombre  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div style="padding: 10px">
                                    {!! Form::label('NuevaDireccion','¿Deseas agregar otra dirección?') !!}
                                    {!! Form::checkbox('NuevaDireccion','true') !!}
                                </div>
                                <div style="padding: 10px">
                                    {!! Form::label('Factura','¿Deseas facturar? ') !!}
                                    {!! Form::checkbox('Factura','true','',['id'=>'Factura']) !!}
                                    {{-- {!! Form::checkbox($name, $value, $checked, [$options]) !!} --}}
                                </div>
                            @elseif(count($direcciones) == 0 )
                                {!! Form::open(['url'=>'guardarDomicilio']) !!}
                                    <div class="row">
                                    <div class="col-md-4" style="margin-left: 25px">
                                            <div class="form-group">
                                                {!! Form::label('Titulo', 'Titulo para identificar el domicilio') !!}
                                                {!! Form::text('Titulo','',['class'=>'form-control', 'required', 'placeholder'=>'Nombre','style'=>'width: 270px'] ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-5" style="margin-left: 55px;  margin-top: 22px">
                                            <div class="form-group">
                                                {!! Form::label('Contacto', 'Nombre de quien recibe') !!}
                                                {!! Form::text('Contacto','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa el Contacto'] ) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10" style="margin-left: 25px">
                                            <div class="form-group">
                                                {!! Form::label('Calle', 'Domicilio') !!}
                                                {!! Form::text('Calle','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa la calle'] ) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2" style="margin-left: 25px">
                                            <div class="form-group" style=" width: 150px">
                                                {!! Form::label('NumEL', 'Número Exterior') !!}
                                                {!! Form::text('NumE','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa el número exterior '] ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="margin-left: 53px; ">
                                            <div class="form-group" style=" width: 160px">
                                                {!! Form::label('NumIL', 'Número Interior') !!}
                                                {!! Form::text('NumI','',['class'=>'form-control',  'placeholder'=>'Ingresa el número interior '] ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="margin-left: 62px">
                                            <div class="form-group">
                                                {!! Form::label('EstadoL', 'Estado') !!}
                                                <select class="form-control" name="Estado" id="Est">
                                                    @foreach($code as $id => $cd)
                                                            <option value="{{ $cd->Estado  }}" >{{ $cd->Estado  }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3" style="margin-left: 25px">
                                            <div class="form-group"  style=" width: 205px">
                                                {!! Form::label('Municipio', 'Localidad/Municipio') !!}
                                                {!! Form::select('Municipio',[],'',['class'=>'form-control', 'id'=>'Municipio'] ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="margin-left: 55px">
                                            <div class="form-group" >
                                                {!! Form::label('CP', 'Código Postal') !!}
                                                {!! Form::select('CP',[],'', ['class'=>'form-control','id'=>'CP']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 20px">
                                            <div class="form-group"  style=" width: 210px">
                                                {!! Form::label('ColoniaL', 'Colonia') !!}
                                                {!! Form::select('Colonia',[],'',['class'=>'form-control', 'id'=>'Col'] ) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5" style="margin-left: 25px">
                                            <div class="form-group">
                                                {!! Form::label('Ciudad', 'Ciudad') !!}
                                                {!! Form::text('Ciudad','',['class'=>'form-control'] ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="margin-left: 50px">
                                            <div class="form-group"  >
                                                {!! Form::label('Telefono', 'Teléfono') !!}
                                                {!! Form::text('Telefono','',['class'=>'form-control'] ) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10" style="margin-left: 25px">
                                            <div class="form-group" >
                                                {!! Form::label('Referencias', 'Referencias') !!}
                                                {!! Form::text('Referencias','', ['class'=>'form-control','id'=>'CP']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="checkout" id="1" value="1">
                                    <button class="btn btn-fill-out" style="margin-left: 200px" type="submit">Guardar</button>
                                {!! Form::close() !!}
                            @endif
                        </div>
                        {!! Form::open(['url'=>'guardarDomicilio']) !!}
                            <div id="formDireccion">
                                <div class="row">
                                    <div class="col-md-4" style="margin-left: 25px">
                                        <div class="form-group">
                                            {!! Form::label('Titulo', 'Titulo para identificar el domicilio') !!}
                                            {!! Form::text('Titulo','',['class'=>'form-control', 'required', 'placeholder'=>'Nombre','style'=>'width: 270px'] ) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-5" style="margin-left: 55px; margin-top: 22px">
                                        <div class="form-group">
                                            {!! Form::label('Contacto', 'Nombre de quien recibe') !!}
                                            {!! Form::text('Contacto','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa el Contacto'] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10" style="margin-left: 25px">
                                        <div class="form-group">
                                            {!! Form::label('Calle', 'Domicilio') !!}
                                            {!! Form::text('Calle','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa la calle'] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2" style="margin-left: 25px">
                                        <div class="form-group" style=" width: 150px">
                                            {!! Form::label('NumEL', 'Número Exterior') !!}
                                            {!! Form::text('NumE','',['class'=>'form-control', 'required', 'placeholder'=>'Ingresa el número exterior '] ) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="margin-left: 53px; ">
                                        <div class="form-group" style=" width: 160px">
                                            {!! Form::label('NumIL', 'Número Interior') !!}
                                            {!! Form::text('NumI','',['class'=>'form-control',  'placeholder'=>'Ingresa el número interior '] ) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="margin-left: 62px">
                                        <div class="form-group">
                                            {!! Form::label('EstadoL', 'Estado') !!}
                                            <select class="form-control" name="Estado" id="Est">
                                                @foreach($code as $id => $cd)
                                                        <option value="{{ $cd->Estado  }}" >{{ $cd->Estado  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3" style="margin-left: 25px">
                                        <div class="form-group"  style=" width: 205px">
                                            {!! Form::label('Municipio', 'Localidad/Municipio') !!}
                                            {!! Form::select('Municipio',[],'',['class'=>'form-control', 'id'=>'Municipio'] ) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="margin-left: 55px">
                                        <div class="form-group" >
                                            {!! Form::label('CP', 'Código Postal') !!}
                                            {!! Form::select('CP',[],'', ['class'=>'form-control','id'=>'CP']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="margin-left: 20px">
                                        <div class="form-group"  style=" width: 210px">
                                            {!! Form::label('ColoniaL', 'Colonia') !!}
                                            {!! Form::select('Colonia',[],'',['class'=>'form-control', 'id'=>'Col'] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5" style="margin-left: 25px">
                                        <div class="form-group">
                                            {!! Form::label('Ciudad', 'Ciudad') !!}
                                            {!! Form::text('Ciudad','',['class'=>'form-control'] ) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="margin-left: 50px">
                                        <div class="form-group"  >
                                            {!! Form::label('Telefono', 'Teléfono') !!}
                                            {!! Form::text('Telefono','',['class'=>'form-control'] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10" style="margin-left: 25px">
                                        <div class="form-group" >
                                            {!! Form::label('Referencias', 'Referencias') !!}
                                            {!! Form::text('Referencias','', ['class'=>'form-control','id'=>'CP']) !!}
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="checkout" id="1" value="1">
                                <button class="btn btn-fill-out" style="margin-left: 200px" type="submit">Guardar</button>
                            </div>
                        {!! Form::close() !!}
                        <div id="form-Facturas">
                            @if(count($datosFiscales ) > 0)
                                <div class="form-group col-md-8">
                                    {!! Form::label('Facturacion', 'Datos Fiscales ') !!}
                                    <select class="form-control" name="datosFactura" id="datosFactura">
                                        @foreach($datosFiscales as $id => $dire)
                                                <option value="{{ $dire->id  }}" >{{ $dire->Titulo  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                        </div>
                        <div style="padding: 10px" id="newDatos">
                            {!! Form::label('NuevaFacturacion','¿Deseas agregar otros datos Fiscales? ') !!}
                            {!! Form::checkbox('NuevaFacturacion','true') !!}
                        </div>
                        <div id="newDatosFactura">
                            {!! Form::open(['url'=>'guardarDatosFiscales']) !!}
                                <div class="row">
                                    <div class="form-group " style="margin-left: 15px; width: 260px" >
                                        {!! Form::label('Titulo', 'Título') !!}
                                        {!! Form::text('Titulo', '', ['class'=>'form-control']) !!}
                                    </div>
                                    <div class="col-md-3" style="margin-left: 10px">
                                        <div class="form-group">
                                            {!! Form::label('FormaPago', 'Forma de Pago') !!}
                                            {!! Form::select('FormaPago',$forma,'',['class'=>'form-control'] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group" style="margin-left: 15px; width: 300px">
                                        {!! Form::label('RazonSocial', 'Razón Social ') !!}
                                        {!! Form::text('RazonSocial','',['class'=>'form-control', 'required'] ) !!}
                                    </div>
                                    <div class="col-md-2" style="margin-left: 20px; ">
                                        <div class="form-group" style="width: 120px">
                                            {!! Form::label('RFC', 'RFC') !!}
                                            {!! Form::text('RFC','',['class'=>'form-control', 'required'] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3" >
                                        <div class="form-group" style="margin-left: 15px; width: 220px">
                                            {!! Form::label('UsoCFDI', 'CFDI') !!}
                                            {!! Form::select('USOCFDI',$cfdi,'',['class'=>'form-control'] ) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3" >
                                        <div class="form-group" style="margin-left: 80px; width: 205px">
                                            {!! Form::label('MetodoPago', 'Método de Pago') !!}
                                            {!! Form::select('MetodoPago',$metodo,'',['class'=>'form-control'] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="checkout" id="1" value="1">
                                <button class="btn btn-fill-out" style="margin-left: 300px" type="submit">Guardar</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="order_review">
                            <div class="heading_s1">
                                <h4>Tu Pedido</h4>
                            </div>
                            <div class="table-responsive order_table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $total = 0;
                                            $envio = 0;
                                            $arrayTotal=0;
                                        @endphp
                                        @foreach($arrayCart as $carrito)
                                            @php
                                                $total=$carrito->SubTotal;
                                                $arrayTotal=$total+$arrayTotal;
                                            @endphp
                                            {{-- <input type="hidden" name="idPedidoCart" id="idPedidoCart"  value="{{ $carrito->Cantidad }}"> --}}
                                            <input type="hidden" name="idPedido" id="idPedido"  value="{{ $carrito->Pedido }}">
                                            <tr>
                                                <td>{{ $carrito->PartNo }} <span class="product-qty">x {{ $carrito->Cantidad }}</span></td>
                                                <td>${{ $carrito->SubTotal }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        @if($arrayTotal < 499)
                                            @php
                                                $envio = 7 * $tc;
                                            @endphp
                                        @endif
                                        <tr>
                                            <th>SubTotal</th>
                                            <td class="product-subtotal">${{ number_format($arrayTotal,2) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Gastos de Envío</th>
                                            <td>${{ number_format($envio,2) }}</td>
                                        </tr>
                                        @php
                                            $total = $arrayTotal + $envio;
                                        @endphp
                                        <tr>
                                            <th>Total</th>
                                            <td class="product-subtotal">${{ number_format($total,2) }}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="panel-body">
                                    {{-- <span><i class="fas fa-tag"></i>  Si tienes un código de descuento, por favor ingrésalo.</span>
                                    <div class="coupon field_form input-group">
                                        <input type="text" value="" class="form-control" placeholder="Ingresa tu código...">
                                        <button class="btn btn-fill-out btn-sm" type="submit">Aplicar Cupón</button>
                                    </div> --}}
                                </div><br><br>
                            </div>
                            <div class="payment_method">
                                <div class="heading_s1">
                                    <h4>Método de Pago</h4>
                                </div>
                                <div class="payment_option">
                                    <div class="custome-radio">
                                        <input class="form-check-input" required="" type="radio" name="payment_option" id="exampleRadios3" value="6" checked="">
                                        <label class="form-check-label" for="exampleRadios3">Transferencia Bancaria</label>
                                        <p data-method="6" class="payment-text">
                                            Realiza tu pago directamente en nuestra cuenta
                                            bancaria. Por favor, usa el número del pedido como referencia de pago y envía tu comprobante
                                            al correo ventas@tecnoabastos.com. Tu pedido se procesará una vez hayamos corroborado tu pago.6
                                            <br>
                                            <span style="margin-left: 25px">GRUPO BONANCE S.A. DE C.V.</span><br>
                                            <span style="margin-left: 25px">Banamex</span><br>
                                            <span style="margin-left: 25px">Número de Cuenta 8156546443</span><br>
                                            <span style="margin-left: 25px">Cuenta CLABE 002320701283115621</span>
                                        </p>
                                    </div>
                                    <div class="custome-radio">
                                        <input class="form-check-input" type="radio" name="payment_option" id="exampleRadios4" value="1">
                                        <label class="form-check-label" for="exampleRadios4">Depósito Bancario</label>
                                        <p data-method="1" class="payment-text">
                                            Realiza tu pago directamente en nuestra cuenta
                                            bancaria. Por favor, usa el número del pedido como referencia de pago y envía tu comprobante
                                            al correo ventas@tecnoabastos.com. Tu pedido se procesará una vez hayamos corroborado tu pago.6
                                            <br>
                                            <span style="margin-left: 25px">GRUPO BONANCE S.A. DE C.V.</span><br>
                                            <span style="margin-left: 25px">Banamex</span><br>
                                            <span style="margin-left: 25px">Número de Cuenta 8156546443</span><br>
                                            <span style="margin-left: 25px">Cuenta CLABE 002320701283115621</span>
                                        </p>
                                    </div>
                                    <div class="custome-radio">
                                        <input class="form-check-input" type="radio" name="payment_option" id="exampleRadios5" value="Pp">
                                        <label class="form-check-label" for="exampleRadios5">Paypal</label>
                                        <p data-method="Pp" class="payment-text">
                                            {{-- <form action="{{ url('paypal/pay') }}" method="get" target="_top"> --}}
                                                {{-- <input type="hidden" name="cmd" value="_s-xclick"> --}}
                                                {{-- <input type="hidden" name="hosted_button_id" value="PY9BEPCZX9A92"> --}}
                                                {{-- <input type="image" target="_blank" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea.">
                                                <img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1"> --}}
                                            {{-- </form> --}}

                                        </p>
                                    </div>
                                    <div class="custome-radio">
                                        <input class="form-check-input" type="radio" name="payment_option" id="exampleRadios6" value="7">
                                        <label class="form-check-label" for="exampleRadios6">MercadoPago</label>
                                        <p data-method="7" class="payment-text">

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-dark" role="alert">
                            ¿Requieres factura? Por favor registra tus datos de facturación antes de realizar tu pago.
                            Te recordamos que Tecnoabastos no realiza facturas posteriores a tu compra.
                            </div>
                            {{-- <form action="{{ url('') }}" id='pedidoPagar' method="POST"> --}}
                            {!! Form::open(['url'=>'realizarPedido','id'=>'pedidoPagar']) !!}
                                <input type="hidden" name="idPedido" id="idPedido"  value="{{ $pedido->id}}">
                                <input type="hidden" name="idDomicilio" id="idDomicilio" value="">
                                <input type="hidden"  name="idTipoPago" id="idTipoPago" value="" >
                                <input type="hidden" name="folio" id="folio" value="{{ $pedido->Folio }}">
                                <input type="hidden" value="{{ $tc }}" id="tc" name="tipoCambio">
                                <input type="hidden" name="facturacion" id="facturacion" value="">
                                <input type="hidden" name="idFacturacion" id="idFacturacion" value="">
                                <input type="hidden" name="total" id="inputTotal" value="{{ $total }}">
                                <button class="btn btn-fill-out"  style="margin-left: 200px" id="pagar" type="submit">Pagar</button>
                                {!! Form::close() !!}
                                {{-- {!! Form::open(['url'=>'paypal/pay','id'=>'pedidoPagar']) !!} --}}
                                    {{-- <div id="paypal-button" style="margin-left: 200px"></div> --}}
                                    {{-- <input type="hidden" name="idPedido" id="idPedido"  value="{{ $pedido->id}}">
                                    <input type="hidden" name="idDomicilio" id="idDomicilio" value="">
                                    <input type="hidden"  name="idTipoPago" id="idTipoPago" value="" >
                                    <input type="hidden" name="folio" id="folio" value="{{ $pedido->Folio }}">
                                    <input type="hidden" value="{{ $tc }}" id="tc" name="tipoCambio">
                                    <input type="hidden" name="facturacion" id="facturacion" value="">
                                    <input type="hidden" name="idFacturacion" id="idFacturacion" value="">
                                    <input type="hidden" name="total" id="inputTotal" value="{{ $total }}"> --}}
                                    {{-- <a href="{{ url('paypal/pay')}}" class="btn btn-fill-out"  style="margin-left: 200px" id="pagar" type="submit">Paypal</a> --}}
                                 {{-- {!! Form::close() !!} --}}
                                <div class="row">
                                    <button  data-target="#modalMP" data-toggle="modal" type="submit" id="mercadopago-button" class="btn btn-primary" style="margin-left: 50px"> Transferencia</button>
                                    <button  data-target="#modalEfectivo" data-toggle="modal" type="submit" id="pagoEfectivo" class="btn btn-primary" style="margin-left: 200px">Efectivo</button>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="modalMP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Pagar con Tarjeta</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="mercado-pago-pagar" method="post" id="paymentForm">
                                <div class="modal-body" style="margin: 20px">
                                    <div class="container_payment" id='container_payment'>
                                        <div class="form-payment">
                                            <div class="payment-details">
                                                @csrf
                                                <div class="row">
                                                    <label for="email">E-Mail</label>
                                                    <input id="email" name="email" type="text" class="form-control">
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="form-group col-sm-8">
                                                        <label for="cardholderName">Nombre que aparece en la tarjeta</label>
                                                        <input id="cardholderName" data-checkout="cardholderName" type="text" class="form-control">
                                                    </div>
                                                    <div class="form-group col-sm-3" style="margin-left: 15px">
                                                        <label for="">Fecha de Expiración</label>
                                                        <div class="input-group expiration-date">
                                                            <input type="text" class="form-control" placeholder="MM" id="cardExpirationMonth" data-checkout="cardExpirationMonth"
                                                                onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off>
                                                            <input type="text" class="form-control" placeholder="YY" id="cardExpirationYear" data-checkout="cardExpirationYear"
                                                                onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-8">
                                                        <label for="cardNumber">Número de la tarjeta</label>
                                                        <input type="text" class="form-control input-background" id="cardNumber" data-checkout="cardNumber"
                                                            onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off>
                                                    </div>
                                                    <div class="form-group col-sm-3" style="margin-left: 15px">
                                                        <label for="securityCode">CVV</label>
                                                        <input id="securityCode" data-checkout="securityCode" type="text" class="form-control"
                                                            onselectstart="return false" onpaste="return false" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off>
                                                    </div>
                                                    <div id="issuerInput" class="form-group col-sm-12 hidden">
                                                        <label for="issuer">Banco</label>
                                                        <select id="issuer" name="issuer" data-checkout="issuer" class="form-control"></select>
                                                    </div>
                                                    <div class="form-group col-sm-12">
                                                        <label for="installments">Pagos</label>
                                                        <select type="text" id="installments" name="installments" class="form-control"></select>
                                                    </div>
                                                    <div class="form-group col-sm-12">
                                                        <input type="hidden" name="idPedido" id="idPedido"  value="{{ $pedido->id}}">
                                                        <input type="hidden" name="idDomicilio" id="modalDomicilio" value="">
                                                        <input type="hidden"  name="idTipoPago" id="modalTipoPago" value="" >
                                                        <input type="hidden" name="folio" id="folio" value="{{ $pedido->Folio }}">
                                                        <input type="hidden" value="{{ $tc }}" id="tc" name="tipoCambio">
                                                        <input type="hidden" name="facturacion" id="facturacion" value="">
                                                        <input type="hidden" name="idFacturacion" id="idFacturacion" value="">
                                                        <input type="hidden" name="transactionAmount" id="amount" value="{{ $total }}" />
                                                        <input type="hidden" name="paymentMethodId" id="paymentMethodId" />
                                                        <input type="hidden" name="description" id="description" value="TecnoAbastos"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary ">Pagar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modalEfectivo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Pagar con Referencia</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="pagoEfectivo" method="post" id="idEfectivo">
                                @csrf
                                <div class="modal-body" style="margin: 20px">
                                    <div class="row">
                                        <label for="email">E-Mail</label>
                                        <input id="email" name="email" type="text" class="form-control">
                                    </div>
                                    <div class="container_payment">Métodos
                                        {!! Form::select('MetodosPago', [], '', ['id'=>'MetodosPago','class'=>'form-control']) !!}
                                    </div><br>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="idPedido" id="idPedido"  value="{{ $pedido->id}}">
                                    <input type="hidden" name="idDomicilioE" id="modalDomicilioE" value="">
                                    <input type="hidden"  name="idTipoPagoE" id="modalTipoPagoE" value="" >
                                    <input type="hidden"  name="Total" id="modalTotal" value="" >
                                    <input type="hidden" name="folio" id="folio" value="{{ $pedido->Folio }}">
                                    <input type="hidden" value="{{ $tc }}" id="tc" name="tipoCambio">
                                    <input type="hidden" name="facturacion" id="facturacion" value="">
                                    <input type="hidden" name="idFacturacion" id="idFacturacion" value="">
                                    <input type="hidden" name="transactionAmount" id="amount" value="{{ $total }}" />
                                    <input type="hidden" name="paymentMethodId" id="paymentMethodId" value="oxxo"/>
                                    <input type="hidden" name="description" id="description" value="TecnoAbastos"/>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary ">Pagar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal" tabindex="-1" id="myModal" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>No se armó </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary">Save changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SECTION SHOP -->

    </div>
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#formDireccion').hide();
            $('#form-Facturas').hide();
            $('#newDatos').hide();
            $('#newDatosFactura').hide();
            $('#paypal-button').hide();
            $('#mercadopago-button').hide();
            $('#pagoEfectivo').hide();
            $('#msgAlert').fadeIn();
            setTimeout(function() {
                $("#msgAlert").fadeOut();
            },2500);

            $("#Est").prepend("<option value='' selected='selected'>Selecciona</option>");
        })
        $('#pagar').on('click', function(e){
            var u = $('#Dire').val();
            var y = $('#idPedidoCart').val();
            var x= $("input[name='payment_option']:checked").val();
            // alert(x)
            // alert(u)
            $('#idTipoPago').val(x);
            $('#idDomicilio').val(u);
            $('#idPedido').val(y);
        })
        $('#modalMP').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var idDomicilio = $('#Dire').val();
            var idTipoPago = $("input[name='payment_option']:checked").val();

            $('#modalDomicilio').val(idDomicilio);
            $('#modalTipoPago').val(idTipoPago);
            // $.get('{{ url("metodosPago") }}',function(val){
            //     for(var i=0; i<val.length; i++){
            //         if(val[i].id != 'mercadopagocard' && val[i].id != 'clabe' )
            //             $("#MetodosPago").append('<option value="'+val[i].id+'">'+val[i].name+'</option>');
            //     }
            // })
        })
        $('#modalEfectivo').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var idDomicilio = $('#Dire').val();
            var idTipoPago = $("input[name='payment_option']:checked").val();
            var total = $('#inputTotal').val();

            $('#modalDomicilioE').val(idDomicilio);
            $('#modalTipoPagoE').val(idTipoPago);
            $('#modalTotal').val(total);
            $.get('{{ url("metodosPago") }}',function(val){
                for(var i=0; i<val.length; i++){
                    if(val[i].id != 'mercadopagocard' && val[i].id != 'clabe' )
                        $("#MetodosPago").append('<option value="'+val[i].id+'">'+val[i].name+'</option>');
                }
            })
        })
        $('#MetodosPago').on('change',function(){
            var val = $('#MetodosPago').val();
            if(val == 'visa'|| val == 'master' || val == 'debvisa' || val == 'debmaster' || val == 'amex'){
                $('#container_payment').show();

            }else if( val == 'bancomer' || val == 'oxxo' | val == 'serfin' | val == 'banamex'){
                $('#container_payment').hide();
            }
        })
        $(document).on('change','input[type="radio"]' ,function(e) {
            // alert(this.value)
            if(this.value=="Pp") {
                if(this.checked) {
                    //  alert('es paypal')
                    $('#paypal-button').show();
                    $("#pagar").hide();
                    $('#mercadopago-button').hide();
                    $('#pagoEfectivo').hide();
                }
               // alert(this.value)
            }if(this.value=="1") {
                if(this.checked) {
                    $('#checkMotivo').val(this.value);
                    $("#pagar").show();
                    $('#pagoEfectivo').hide();
                    $('#paypal-button').hide();
                    $('#mercadopago-button').hide();
                }
                //alert(this.value)
            }
            if(this.value=="6") {
                if(this.checked) {
                    $('#checkMotivo').val(this.value);
                    $("#pagar").show();
                    $('#pagoEfectivo').hide();
                    $('#paypal-button').hide();
                    $('#mercadopago-button').hide();
                }
               // alert(this.value)
            }
            if(this.value=="7") {
                if(this.checked) {
                    $('#checkMotivo').val(this.value);
                    $('#paypal-button').hide();
                    $("#pagar").hide();
                    $('#mercadopago-button').show();
                    $('#pagoEfectivo').show();
                }
               // alert(this.value)
            }
        });
        $('#Est').on('change',function(){
            var cp= $('#Est').val();
            // cp = parseInt(cp)+1;
            // alert(cp)
            $.get('{{ url("getMunicipio") }}/'+cp,function(val){
                $('#Municipio').empty();
                $("#Municipio").prepend("<option value='' selected='selected'>Selecciona </option>");
                for(var i=0; i<val.length; i++){
                    $("#Municipio").append('<option value="'+val[i].Municipio+'">'+val[i].Municipio+'</option>');
                }
            })
        })
        $('#Municipio').on('change',function(){
            var cp= $('#Est').val();
            // cp = parseInt(cp)+1;
            // alert(cp)
             $.get('{{ url("getCodigoPostal") }}/'+cp,function(val){
                $('#CP').empty();
                $("#CP").prepend("<option value='' selected='selected'>Selecciona</option>");
                for(var i=0; i<val.length; i++){
                    $("#CP").append('<option value="'+val[i].CodigoPostal+'">'+val[i].CodigoPostal+'</option>');
                }
            })
        })
        $('#CP').on('change',function(){
            var cp= $('#CP').val();
            // alert(cp)
             $.get('{{ url("getColonia") }}/'+cp,function(val){
                // $('#CP').empty();
                for(var i=0; i<val.length; i++){
                    $("#Col").append('<option value="'+val[i].Asentamiento+'">'+val[i].Asentamiento+'</option>');
                }
            })
        })
        $('#NuevaDireccion').on('click',function(){
            var algo = $("#NuevaDireccion").is(':checked');

            if(algo == true){
                $('#formDireccion').show();
            }else{
                 $('#formDireccion').hide();
            }
        })
        $('#Factura').on('click',function(){
            var situ  = $("#Factura").is(':checked');

            if(situ == true){
                $('#form-Facturas').show();
                $('#newDatos').show();
            }else{
                 $('#form-Facturas').hide();
                 $('#newDatos').hide();
            }
        })
        $('#NuevaFacturacion').on('click',function(){
            var value = $('#NuevaFacturacion').is(':checked');
            // alert(value)
            if(value == true){
                $('#newDatosFactura').show();
            }else{
                $('#newDatosFactura').hide();
            }
        })
        $('#Factura').on('click',function(){
            if(this.checked){
                $('#facturacion').val('si');
                $('#idFacturacion').val($('#datosFactura').val());
            }
        })
    </script>

{{-- Scripts para Paypal --}}

    {{-- Scripts para Mercado Pago  --}}
    <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
    <script type="text/javascript" src="js/index.js" defer></script>
@endsection
