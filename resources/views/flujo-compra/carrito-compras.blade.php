@extends('layout.mainlayout')

@section('content')
<div class="mt-4 staggered-animation-wrap">
    <!-- START SECTION BREADCRUMB -->
    <div class="breadcrumb_section bg_gray page-title-mini">
        <div class="container"><!-- STRART CONTAINER -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="page-title">
                        <h1>Carrito de Compras</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Shopping Cart</li>
                    </ol>
                </div>
            </div>
        </div><!-- END CONTAINER-->
    </div>
    <!-- END SECTION BREADCRUMB -->

    <!-- START MAIN CONTENT -->
    <div class="main_content">

    <!-- START SECTION SHOP -->
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive shop_cart_table">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="product-thumbnail">&nbsp;</th>
                                    <th class="product-name">Producto</th>
                                    <th class="product-price">Precio</th>
                                    <th class="product-quantity">Cantidad</th>
                                    <th class="product-subtotal">Total</th>
                                    <th class="product-remove">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @php
                                        $envio=0;
                                        $total = 0;
                                        $arrayTotal=0;
                                    @endphp
                                @foreach($arrayCart as $carrito)
                                    @php
                                        $total=$carrito->SubTotal;
                                        $arrayTotal=$total+$arrayTotal;
                                    @endphp
                                    <tr>
                                        <td class="product-thumbnail"><a href="#"><img src="{{ $carrito->ImageURL }}" alt="product1"></a></td>
                                        <td class="product-name" data-title="Product">{{ $carrito->PartNo }}</td>
                                        <td class="product-price" data-title="Price">$ {{ number_format($carrito->Precio,2) }}</td>
                                        <td class="product-quantity" data-title="Quantity">
                                            <div class="quantity">
                                                <a onclick="restar({!!  $carrito->id  !!});" type="button"><i class="ti-minus" style="margin-right: 15px"></i></a>
                                                    <input id="cantidad_{!!  $carrito->id  !!}" class="cantidad" type="number" value="{{ $carrito->Cantidad }}"  style="width: 40px"/>
                                                <a onclick="sumar({!!  $carrito->id  !!});" type="button" id="pruebamesta"><i class="ti-plus" style="margin-left: 15px"></i></a></td>
                                            </div>
                                        </td>
                                        <input type="hidden" id="user" value="{{$carrito->id_usuario}}">
                                        <td class="product-subtotal" id="total_{!! $carrito->id !!}" data-title="Total">$ {{ number_format($carrito->SubTotal,2) }}</td>
                                        <td class="product-remove" data-title="Remove"><a href="{{ url('eliminarProducto/'.$carrito->id) }}"><i class="ti-close"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="medium_divider"></div>
                    <div class="divider center_icon"><i class="ti-shopping-cart-full"></i></div>
                    <div class="medium_divider"></div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-4 col-md-offset-3" style="margin-left: 800px" >
                    <div class="border p-3 p-md-4">
                        <div class="heading_s1 mb-3">
                            <h6>Total del Carrito</h6>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td class="cart_total_label" id="Subtotal">Subtotal</td>
                                        <td class="cart_total_amount">$ <input type="text" style="width: 75px;border: 0" disabled="true" id="totalAmount" value=""></td>
                                    </tr>
                                    @if($arrayTotal < 499)
                                        @php
                                            $envio = 7 * $tc;
                                            // dd($arrayTotal);
                                        @endphp
                                    @endif
                                    <tr>
                                        <td class="cart_total_label">Costo de Envío</td>
                                        <td class="cart_total_amount">$ <input id="envio" disabled="true" style="width: 75px; border: 0"  value="{{  number_format($envio,2) }}"></td>
                                    </tr>
                                    @php
                                        $total = $arrayTotal + $envio;
                                    @endphp
                                    <tr>
                                        <td class="cart_total_label">Total</td>
                                        <td class="cart_total_amount"><strong>$</strong><input id="total" style="border:0;width: 80px; font-weight: 600"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <a href="{{ url('checkout') }}" class="btn btn-fill-out">Proceder con el Pago</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION SHOP -->
</div>
@endsection

@section('script')
    <script>
        // var subtotal=0;
        function sumar(id){
            var cantidadActual = $('#cantidad_'+id).val();
            $('#cantidad_'+id).val(parseInt(cantidadActual)+1);

            $.get('sumarTotal/'+cantidadActual+'/'+id,function(value){
                var nose = JSON.parse(value);
                $('#total_'+id).text(nose);
            })

            var id_usuario = $('#user').val();
             $.get('revisarSubtotal/'+id_usuario,function(data){
                // alert(id_usuario)
                $('#totalAmount').val(data);
                $('#total').val(data);
            });
        }

        function restar(id){
            var cantidadActual = $('#cantidad_'+id).val();
            $('#cantidad_'+id).val(parseInt(cantidadActual)-1);

            $.get('restarTotal/'+cantidadActual+'/'+id,function(value){
                var nose = JSON.parse(value);
               $('#total_'+id).text(nose);
            })

            var id_usuario = $('#user').val();
             $.get('revisarSubtotal/'+id_usuario,function(data){
                $('#totalAmount').val(data);
                $('#total').val(data);
            });
        }

        $(document).ready(function() {
            var id = $('#user').val();

            $.get('revisarSubtotal/'+id,function(data){
                $('#totalAmount').val(data);
                $('#total').val(data);
            });
        })

    </script>
@endsection
