<!DOCTYPE html>
<html lang="en">
    <head>
        @include('partials.head')
    </head>
    <body>
        {{-- @include('partials.nav') --}}
        @include('partials.header')
        @yield('content')
        @include('partials.newsletter')
        @include('partials.footer')
        @include('partials.footer-scripts')
        <script>
            $('#dropdownMenuButton').on('show.bs.dropdown', function () {

            })
        </script>
        @yield('script')

    </body>
</html>
