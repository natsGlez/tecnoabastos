<tr>
<td class="header">
<a href="{{ url('/') }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="{{ asset('assets/images/logo_tecnoabastos.png') }}" class="logo" alt="TecnoAbastos">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
