@extends('layout.mainlayout')

@section('content')
<div class="mt-4 staggered-animation-wrap">
    <!-- START SECTION BREADCRUMB -->
    <div class="breadcrumb_section bg_gray page-title-mini">
        <div class="container"><!-- STRART CONTAINER -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="page-title">
                        <h1>Wishlist</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a >Pages</a></li>
                        <li class="breadcrumb-item active">Wishlist</li>
                    </ol>
                </div>
            </div>
        </div><!-- END CONTAINER-->
    </div>
    <!-- END SECTION BREADCRUMB -->

    <!-- START MAIN CONTENT -->
    <div class="main_content">

    <!-- START SECTION SHOP -->
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive wishlist_table">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="product-thumbnail">&nbsp;</th>
                                    <th class="product-name">Producto</th>
                                    <th class="product-price">Precio</th>
                                    <th class="product-add-to-cart">Agregar</th>
                                    <th class="product-remove">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($arrayFav) > 0 && isset($tc))
                                    @php
                                    $count = 0;
                                    @endphp
                                    @foreach($arrayFav as $fav)
                                        @php
                                            $precio = (($fav->P1*$tc)*1.3)*1.16;
                                        @endphp
                                        <tr>
                                            <td class="product-thumbnail"><a href="#"><img src="{{ $fav->ImageURL }}" alt="product1"></a></td>
                                            <td class="product-name" data-title="Producto">{{ $fav->PartNo }}</td>
                                            <td class="product-price" data-title="Precio">$ {{ number_format($precio,2)}}</td>
                                            <td class="product-add-to-cart"><a href="{{ url('agregarCarrito/'.$fav->id.'/'.$precio.'/1') }}" type="submit" class="btn btn-fill-out"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></td>
                                            <td class="product-remove" data-title="Remove"><a href="{{ url('eliminarFav/'.$fav->id) }}"><i class="ti-close"></i></a></td>
                                        </tr>
                                    @endforeach
                                @endif
                                {{-- <input type="hidden" id="count" value="{{ $count }}"> --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION SHOP -->
</div>
@endsection

