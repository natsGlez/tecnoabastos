<header class="header_wrap" style="margin-top: 250px; position: inherit;">
    {{-- menú principal --}}
    <div class="bottom_header dark_skin main_menu_uppercase" >
    	<div class="custom-container" >
            <div class="row">
            	<div class="col-lg-3 col-md-4 col-sm-6 col-3" style="padding-left: 30px; padding-top: 100px">
                	<div class="categories_wrap">
                        <button type="button" data-toggle="collapse" data-target="#navCatContent" aria-expanded="false" class="categories_btn">
                            <i class="linearicons-menu"></i><span>Menú Principal </span>
                        </button>
                        <div id="navCatContent" class="nav_cat navbar collapse">
                            <ul>
                                {{-- productos --}}
                                 <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos') }}"><span>Productos</span></a></li>

                                </li>
                                {{-- pedidos --}}
                                <li><a class="dropdown-item nav-link nav_item" href="{{ url('pedidos') }}"><span>Pedidos</span></a></li>

                                </li>
                                {{-- direcciones --}}
                                <li><a class="dropdown-item nav-link nav_item" href="{{ url('direcciones') }}"><span>Direcciones</span></a></li>

                                </li>
                                       {{-- facturacion --}}
                                <li><a class="dropdown-item nav-link nav_item" href="{{ url('datosFacturas') }}"><span>Datos de Facturación</span></a></li>

                                </li>
                                {{-- detalles de la cuenta --}}
                                <li><a class="dropdown-item nav-link nav_item" href="{{ url('perfil') }}"><span>Detalles de la cuenta</span></a></li>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>
<!-- END HEADER -->
