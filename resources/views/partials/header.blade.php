<!-- START HEADER -->

@php
    use App\Carrito;
    use GuzzleHttp\Client;
    use App\Favoritos;
    if(!(Auth::guest())){
        $carritos = Carrito::where('id_usuario',Auth::user()->id)->get();
        $favoritos = Favoritos::where('idUser',Auth::user()->id)->get();
        // dd($carritos);
        $arrayCart = array();
            foreach($carritos as $cart){
                // dd($fav->idProducto);
                $client = new Client([
                    'base_uri' => 'http://asserver.ddns.net/americanstock/',
                    // 'base_uri' => 'http://127.0.0.1:8001/',
                    'timeout' => 2.0,
                    ]);

                    $response = $client->request('GET', "api/producto/{$cart->Producto}");
                    $respuesta = json_decode($response->getBody()->getContents());
                    $respuesta->Precio = $cart->Precio;
                    $respuesta->Cantidad = $cart->Cantidad;
                    $respuesta->SubTotal = $cart->SubTotal;
                    $arrayCart[] = $respuesta;
                }
    }
        // return $arrayCart;
@endphp

<header class="header_wrap">
	<div class="top-header light_skin bg_dark d-none d-md-block">
        <div class="custom-container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-8">
                	<div class="header_topbar_info" style="font-weight: 600">
                    	<div class="header_offer" style="margin-left: 50px; height: 80px; padding-top: 23px; color: #E7367B">
                    		<span>Envío</span><span style="color:#b2d585; font-size: 18px"> Gratis</span><span> todo México en compras mayores a $499 pesos</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-4">
                    <div class="row" style="margin-left: 200px">
                        <div class="col-md-2">
                            <a href="https://www.facebook.com/TecnoAbastos" target="_blank"><img style="width: 30px; padding-top: 20px; " src="{{ asset('assets/images/facebook.svg') }}"></a>
                            <a href="https://www.instagram.com/tecnoabastos/" target="_blank"><img style="width: 30px; margin-left: 15px; padding-top: 20px; " src="{{ asset('assets/images/instagram-bosquejado.svg') }}">
                        </div>
                        <div class="col-md-9" >
                            <a href="{{ url('contacto') }}" style="color: #fff; font-weight: 700; font-size: 20px; padding-left: 75px">Contáctanos </a>
                            <a href="https://api.whatsapp.com/send?phone=+5213315759152&text=Hola!%20estoy%20interesado%20en%20sus%20productos." target="_blank" style="color: #fff; font-weight: 700; font-size: 20px;">(33) 1575 9152</a>
                            <a style="color: #fff; font-weight: 700; font-size: 18px; margin-left: 100px;margin-top: -15px">ventas@tecnoabastos.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="middle-header dark_skin">
    	<div class="container">
            <div class="row">
                <div class="nav_block" style="padding-left: 50px">
                    <div >
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('assets/images/pagina.svg') }}" alt="" width="300px" style="padding-right: 30px" >
                        </a>
                    </div>
                    <div class="dropdown" style="width: 50%">
                        <button class="btn btn-fill-out btn-radius staggered-animation text-uppercase animated slideInLeft"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="opacity: 1">
                            <img style="width: 90%" src="{{ asset('assets/images/menutecno.png') }}"/>
                        </button>
                        <div class="dropdown-menu" style="margin-top: 10px; box-shadow:4px 4px 8px #000;" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ url('buscarFamilia/Desktop') }}">Desktop</a>
                            <a class="dropdown-item" href="{{ url('buscarFamilia/Printers') }}">Impresoras</a>
                            <a class="dropdown-item" href="{{ url('buscarFamilia/Laptops') }}">Laptops</a>
                            {{-- <a class="dropdown-item" href="{{ url('buscarFamilia/Plotter') }}">Plotters</a> --}}
                            <a class="dropdown-item" href="{{ url('buscarFamilia/Projectors') }}">Proyectores</a>
                            <a class="dropdown-item" href="{{ url('buscarFamilia/Server') }}">Servidores</a>
                        </div>
                    </div>
                    <a class="navbar-brand" style="" href="/">
                        {{-- <img class="logo_light" src="assets/images/logo_tecnoabastos.png" alt="logo" width="30%"/> --}}
                        <img class="logo_dark" style="margin-left: 20px; margin-top: 25px; margin-bottom: 25px" src="{{ asset('assets/images/logo_tecnoabastos.png') }}" alt="logo" />
                    </a>
                    <div class="product_search_form rounded_input" style="margin-left: 30px">
                        <input class="form-control" placeholder="Buscar Producto.." type="text" name="buscar" id="buscar">
                        <a onclick="buscar()" type="button" class="search_btn2" id="btnBuscar">
                            <i class="fa fa-search" style="padding-left: 13px; padding-top: 13px; color: white"></i>
                        </a>
                    </div>
                    <ul class="navbar-nav attr-nav align-items-center">
                        @guest
                            <div class="dropdown"  style="width: 200px; margin-left: 50px">
                                <button class="btn btn-fill-out btn-radius staggered-animation text-uppercase animated slideInLeft"
                                type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Mi Cuenta
                                </button>
                                <div class="dropdown-menu" style="width: 300px; margin-top: 10px; box-shadow:4px 4px 8px #000;" >
                                    <h4 style="margin-left: 75px">Iniciar Sesión</h4>
                                    <p style="margin-left: 15px">Ingresa tu correo y contraseña</p>
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                        {{-- EMAIL INPUT/VALIDATION --}}
                                        <div class="form-group" style="padding-left: 15px; padding-right: 15px">
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                                placeholder="Ingresa tu email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                        {{-- CONTRASEÑA INPUT/VALIDATION --}}
                                        <div class="form-group" style="padding-left: 15px; padding-right: 15px">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                            placeholder="Ingresa tu contraseña" name="password" required autocomplete="current-password">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-groupmb-0" style="padding-left: 15px; padding-right: 15px">
                                            <button type="submit" class="btn btn-fill-out btn-block" style="background-color: ">
                                                {{ __('Iniciar Sesión') }}
                                            </button>
                                        </div>
                                    </form>
                                    <div class="dropdown-divider"></div>
                                        <div class="form-note text-center">¿No tienes cuenta? <a href="{{ route('register') }}">¡Regístrate!</a></div>
                                        {{-- @if (Route::has('password.request')) --}}
                                            <a class="btn btn" href="{{ route('no') }}">
                                                {{ __('Olvidé mi contraseña') }}
                                            </a>
                                        {{-- @endif --}}
                                </div>
                            </div>
                            {{-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}"><i class="linearicons-user"></i><span style="color: rgb(78, 75, 75)"> {{ __('Iniciar Sesión') }}</span></a>
                            </li> --}}
                            {{-- @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Registrarse') }}</a>
                                </li>
                            @endif --}}
                        @else
                            <li class="nav-item dropdown" >
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="box-shadow:4px 4px 8px #000;">
                                    <a class="dropdown-item" href="{{ url('perfil') }}">Ver Perfil</a>
                                    <a class="dropdown-item" href="{{ url('pedidos') }}">Ver Pedidos</a>
                                    <a class="dropdown-item" href="{{ url('direcciones') }}">Ver Domicilios</a>
                                    <a class="dropdown-item" href="{{ url('datosFacturas') }}">Ver Datos Facturación</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                        @if(!Auth::guest())
                            <li><a href="{{ url('favoritos') }}" class="nav-link"><i class="linearicons-heart"></i><span class="wishlist_count">{{ sizeof($favoritos) }}</span></a></li>
                            <li class="dropdown cart_dropdown"><a class="nav-link cart_trigger" href="#" data-toggle="dropdown"><i class="linearicons-cart"></i><span class="cart_count">{{ sizeof($arrayCart) }}</span></a>
                            <div class="cart_box dropdown-menu dropdown-menu-right">
                                <ul class="cart_list">
                                    @php
                                        $total = 0;
                                        $arrayTotal=0;
                                    @endphp
                                        @foreach($arrayCart as $cart)
                                            @php
                                                $total=$cart->SubTotal;
                                                $arrayTotal=$total+$arrayTotal;
                                                // echo $arrayTotal;
                                            @endphp
                                            <li>
                                                <a href="{{ url('eliminarProducto/'.$cart->id) }}" class="item_remove"><i class="ion-close"></i></a>
                                                <a href="{{ url('verDetalle/'.$cart->id) }}"><img src="{{ $cart->ImageURL}}" alt="cart_thumb1">{{ $cart->PartNo }}</a>
                                                <span class="cart_quantity">{{ $cart->Cantidad }}x <span class="cart_amount"> <span class="price_symbole">$</span></span>{{ $cart->Precio }}</span>
                                            </li>
                                        @endforeach
                                </ul>
                                <div class="cart_footer">
                                    <p class="cart_total"><strong>Subtotal:</strong> <span class="cart_price"> <span class="price_symbole">$</span></span>{{ $arrayTotal }}</p>
                                    <p class="cart_buttons"><a href="{{ url('carrito-de-compras') }}" class="btn btn-fill-line view-cart">Ver Carrito</a><a href="{{ url("checkout") }}" class="btn btn-fill-out checkout">Continuar</a></p>
                                </div>
                            </div>
                        @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

{{-- @include('partials.footer-scripts') --}}

    <script>
        function buscar(){
            var buscar = $('#buscar').val();
            document.getElementById('btnBuscar').href= "/buscarProducto/"+buscar;
        }
    </script>

