<!-- START SECTION SUBSCRIBE NEWSLETTER -->
<div class="section bg_default small_pt small_pb">
	<div class="container">
    	<div class="row align-items-center">
            <div class="col-md-6">
                <div class="newsletter_text text_white">
                    <h3>Suscríbete a nuestro Boletín de Noticias</h3>
                    <p>Recibe nuestras promociones antes que todos</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="newsletter_form">
                        {!! Form::open(['url'=>'suscribirse']) !!}
                            <input type="text" required="" class="form-control rounded-0" name="email" placeholder="Ingresa tu email">
                            <button type="submit" class="btn btn-dark rounded-0" name="submit" value="Submit">Suscríbete</button>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- START SECTION SUBSCRIBE NEWSLETTER -->
