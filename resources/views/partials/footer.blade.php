<!-- START FOOTER -->
<footer class="footer_light">
     {{-- SECCIÓN DE ENVÍOS Y SOPORTE --}}
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="shopping_info">
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <div class="icon" align="center">
                                <img src="{{ asset('assets/images/entrega.svg')  }}" style="width: 80px; margin-top: 50px"alt="logo" >
                            </div>
                            <div class="icon_box icon_box_style2" style="margin-top: -25px">
                                <div class="icon_box_content" align="center" style="padding: 40px">
                                	<h5 style="font-weight: bold">Envío Gratis</h5>
                                    <p>En todas tus compras mayores a $499 pesos con un tiempo
                                        estimado de entrega entre 1 a 3 días hábiles.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="icon" align="center">
                                <img src="{{ asset('assets/images/medalla-03.svg')  }}" style="width: 80px; margin-top: 50px"alt="logo" >
                            </div>
                            <div class="icon_box icon_box_style2" style="margin-top: -25px">
                                <div class="icon_box_content" align="center" style="padding: 40px">
                                	<h5 style="font-weight: bold">Calidad y Garantía</h5>
                                    <p>Máxima calidad en todos nuestros artículos que garantizan
                                        un producto funcional y durable.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="icon" align="center">
                                <img src="{{ asset('assets/images/soporte-02.svg')  }}" style="width: 80px; margin-top: 50px"alt="logo" >
                            </div>
                            <div class="icon_box icon_box_style2" style="margin-top: -25px">
                                <div class="icon_box_content" align="center" style="padding: 40px">
                                    <h5 style="font-weight: bold">Atención y Soporte</h5>
                                    <p>Por parte de nuestros Ejecutivos para resolver tus dudas
                                        y apoyarte en el proceso de compra.</p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="footer_top">
        <div class="container">
            <div class="row">
                {{-- INFORMACIÓN --}}
                <div class="col-md-3" style="margin-left: 50px; margin-top: 17px">
                    <div class="widget"  >
                        <div class="footer_logo">
                            <a href="#"><img src="{{ asset('assets/images/logo_tecnoabastos.png') }}" alt="logo"/></a>
                        </div>
                    </div>
                	<div class="widget">
                        <h6 class="widget_title" style="font-weight: 900; font-size: 17px">Información</h6>
                        <ul class="widget_links" align="left">
                            <li><a href="{{ url('faq') }}">Preguntas Frecuentes</a></li>
                            <li><a href="{{ url('politicasydevoluciones') }}">Garantías y Devoluciones</a></li>
                            <li><a href="{{ url('terminosycondiciones') }}">Términos y Condiciones</a></li>
                            <li><a href="{{ url('avisodeprivacidad') }}">Aviso de Privacidad</a></li>
                        </ul>
                    </div>
                </div>
                 {{-- ACERCA DE NOSOTROS--}}
                <div class="col-md-3 " style="margin-left: 130px; margin-top: 70px"  >
                    <div class="widget">
                        <h6 class="widget_title" style="margin-left: 25px; margin-top: 50px; font-weight: 900; font-size: 17px">Acerca de Nosotros</h6>
                        <ul class="contact_info">
                            <li style="padding-left: 25px">
                                {{-- <i class="ti-location-pin"></i> --}}
                                <p>En TecnoAnastos encontrarás desde Filminas para equipos
                                    de impresión hasta accesorios, baterías, cargadores, teclados
                                y displays para equipos de cómputo. </p>
                                <p>Haz tu compra hoy y comprueba nuestra calidad y servicio.</p>
                            </li>
                            <li style="padding-left: 25px">
                                <a href="https://www.facebook.com/TecnoAbastos" target="_blank"><img style="max-width: 10%" src="{{ asset('assets/images/face.svg') }}">
                                <a href="https://www.instagram.com/tecnoabastos/" target="_blank"><img style="max-width: 10%; margin-left: 15px" src="{{ asset('assets/images/insta.svg') }}">
                                <a href="https://api.whatsapp.com/send?phone=+5213315759152&text=Hola!%20estoy%20interesado%20en%20sus%20productos." target="_blank"><img style="max-width: 10%; margin-left: 15px" src="{{ asset('assets/images/whatsapp.svg') }}">
                                {{-- <br><i class="ti-email"></i> --}}<br><br>
                                <a href="{{ url('contacto') }}" style="color: #687188">Contacto</a><br>
                                <a >ventas@tecnoabastos.com</a>
                            </li>
                            {{-- <li>
                                <i class="ti-mobile"></i>
                                <p>(33) 1575 9152</p>
                            </li> --}}
                        </ul>
                    </div>
                </div>
                {{-- MI CUENTA --}}
                <div class="col-md-3" style="margin-left: 250px; margin-top: 110px; margin-right: -150px" >
                	<div class="widget">
                        <h6 class="widget_title" style="font-weight: 900; font-size: 17px">Mi Cuenta</h6>
                        <ul class="widget_links" >
                            <li><a href="{{ route('register') }}">Crear Cuenta</a></li>
                            <li><a href="{{ route('login') }}">Iniciar Sesión</a></li>
                            <li><a href='{{ url('favoritos') }}'>Lista de Deseos</a></li>
                            <li><a href="{{ url('carrito-de-compras') }}">Carrito de Compra</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- DERECHOS RESERVADOS --}}
    <div class="bottom_footer border-top-tran">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="footer_payment text-center text-lg-center">
                        <li><a href="#"><img style="margin-left: 300px" src="{{ asset('assets/images/logos_pagos.png') }}" alt="visa"></a></li>
                    </ul>
                </div>
            {{-- </div>
            <div class="row"> --}}
                <div class="col-md-12" >
                    <p class="mb-md-0 text-center text-md-center" style="color: rgb(138, 125, 125)">© 2020 Tecnoabastos | Todos los derechos reservados. </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER -->
