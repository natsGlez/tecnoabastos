@extends('layout.mainlayout')

@section('content')
<div class="mt-4 staggered-animation-wrap">
        <!-- START SECTION BREADCRUMB -->
    <div class="breadcrumb_section bg_gray page-title-mini">
        <div class="container"><!-- STRART CONTAINER -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="page-title">
                        <h1>Detalle de Producto</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Product Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- END CONTAINER-->
    </div>
    <!-- END SECTION BREADCRUMB -->

    <!-- START MAIN CONTENT -->
    <div class="main_content">

    <!-- START SECTION SHOP -->
    <div class="section">
        <div class="container">
            <div class="row">
                {{-- IMÁGENES DE LOS PRODUCTOS --}}
                <div class="col-lg-6 col-md-6 mb-4 mb-md-0">
                <div class="product-image">
                    <div class="product_img_box" style="margin-right: 40px">
                        <a href="{{ $respuesta->ImageURL }}" class="content-popup" title="{{ $respuesta->Grupo }}-{{ $respuesta->Marcas }}">
                            <span style="margin-left: 560px" class="linearicons-zoom-in"></span>
                        </a>
                        <img id="product_img" src='{{ $respuesta->ImageURL }}' data-zoom-image="{{ $respuesta->ImageURL }}" alt="product_img1" />
                    </div>
                </div>
            </div>
                {{-- DETALLES DEL PRODUCTO --}}
                <div class="col-lg-6 col-md-6">
                    <div class="pr_detail">
                        <div class="product_description">
                            <h4 class="product_title"><a href="#"></a>{{ $respuesta->Grupo }}-{{ $respuesta->Marcas }}</h4>
                            <div class="product_price">
                                @php
                                    $precioEntreMargen = number_format($respuesta->P1/.65,2);
                                    $precio = ($precioEntreMargen*$tc)*1.16;
                                @endphp
                                <span class="price">${{ number_format($precio,2) }} MXN</span>
                            </div>
                            @foreach($array as $value)
                                @if($value['product_id'] == $respuesta->id)
                                    <div class="rating_wrap">
                                        <div class="rating">
                                            @php
                                                $division = $value['Suma'] / $value['Promedio'];
                                                $redondear = round($division);
                                            @endphp<br>
                                            <div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div>
                                        </div>
                                        <span class="rating_num">{{ $value['Promedio']}}</span>
                                    </div>
                                @endif
                            @endforeach
                            <br><br>
                            <div class="pr_desc">
                                <p>{{ $respuesta->Descripcion }}</p>
                            </div>
                            <div class="product_sort_info">
                                <ul>
                                    <li><i class="linearicons-shield-check"></i>{{ $respuesta->Tipo }}</li>
                                    <li><i class="linearicons-sync"></i>Producto Garantizado <a href="{{ url('politicasydevoluciones') }}" target="_blank"><span style="color: #CDCDCD">Ver Política de Garantías</span></a></li>
                                    <li><i class="linearicons-bag-dollar"></i>Flete Gratis en compras mayores a $499</li>
                                </ul>
                            </div>
                            {{-- <div class="pr_switch_wrap">
                                <span class="switch_lable">Color</span>
                                <div class="product_color_switch">
                                    <span class="active" data-color="#87554B"></span>
                                    <span data-color="#333333"></span>
                                    <span data-color="#DA323F"></span>
                                </div>
                            </div> --}}
                            {{-- <div class="pr_switch_wrap">
                                <span class="switch_lable">Talla</span>
                                <div class="product_size_switch">
                                    <span>xs</span>
                                    <span>s</span>
                                    <span>m</span>
                                    <span>l</span>
                                    <span>xl</span>
                                </div>
                            </div> --}}
                        </div>
                        <hr />
                        <div class="cart_extra">
                            <div class="cart-product-quantity">
                                <div class="quantity">
                                    <input type="button" value="-" class="minus">
                                    <input type="text" name="quantity" value="1" title="Qty" class="qty" size="4">
                                    <input type="button" value="+" class="plus">
                                </div>
                            </div>
                            <div class="cart_btn">
                                <a class="btn btn-fill-out btn-addtocart" type="button" href="{{ url("agregarCarrito/".$respuesta->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al carrito</a>
                                <a href="{{ url('agregarFavoritos/'.$respuesta->id) }}" style="margin-left: 30px"><i class="icon-heart"></i></a>
                            </div>
                        <hr />
                        <ul class="product-meta">
                            @if($respuesta->Marcas !=null)
                                <li>Marca: <a >{{ $respuesta->Marcas }}</a></li>
                            @endif
                            @if($respuesta->Familia !=null)
                                <li>Familia: <a  rel="tag">{{ $respuesta->Familia }}</a></li>
                            @endif
                            @if($respuesta->Grupo !=null)
                                <li>Grupo: <a >{{ $respuesta->Grupo }}</a></li>
                            @endif
                            @if($respuesta->Voltaje !=null)
                                <li>Voltaje: <a>{{ $respuesta->Voltaje }}</a></li>
                            @endif
                            @if($respuesta->Amper !=null)
                                <li>Amperaje: <a rel="tag">{{ $respuesta->Amper }}</a></li>
                            @endif
                            @if($respuesta->Watts !=null)
                            <li>Watts: <a rel="tag">{{ $respuesta->Watts }}</a></li>
                            @endif
                            @if($respuesta->Celdas != null)
                                <li>Celdas: <a rel="tag">{{ $respuesta->Celdas }}</a></li>
                            @endif
                        </ul>
                        <div class="product_share">
                            <span>Compartir:</span>
                            <ul class="social_icons">
                                <li><a href="https://www.facebook.com/TecnoAbastos" target="_blank"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/tecnoabastos/" target="_blank"><i class="ion-social-instagram-outline"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="large_divider clearfix"></div>
                </div>
            </div>
            {{-- DESCRIPCIÓN/ INFO ADICIONAL/ COMENTARIOS--}}
            <div class="row">
                <div class="col-12">
                    <div class="tab-style3">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="Description-tab" data-toggle="tab" href="#Description" role="tab" aria-controls="Description" aria-selected="true">Descripción</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="Reviews-tab" data-toggle="tab" href="#Reviews" role="tab" aria-controls="Reviews" aria-selected="false">Comentarios ({{ sizeof($comentarios) }})</a>
                            </li>
                        </ul>
                        <div class="tab-content shop_info_tab">
                            <div class="tab-pane fade show active" id="Description" role="tabpanel" aria-labelledby="Description-tab">
                                <p>{{ $respuesta->Descripcion }}</p>
                                {{-- <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p> --}}
                            </div>
                            {{-- <div class="tab-pane fade" id="Additional-info" role="tabpanel" aria-labelledby="Additional-info-tab"> --}}
                                {{-- <table class="table table-bordered">
                                    <tr>
                                        <td>Capacity</td>
                                        <td>5 Kg</td>
                                    </tr>
                                    <tr>
                                        <td>Color</td>
                                        <td>Black, Brown, Red,</td>
                                    </tr>
                                    <tr>
                                        <td>Water Resistant</td>
                                        <td>Yes</td>
                                    </tr>
                                    <tr>
                                        <td>Material</td>
                                        <td>Artificial Leather</td>
                                    </tr>
                                </table> --}}
                                {{-- <p>{{ $respuesta->Reemplazo }}</p> --}}
                            {{-- </div> --}}
                            <div class="tab-pane fade" id="Reviews" role="tabpanel" aria-labelledby="Reviews-tab">
                                <div class="comments">
                                    <h5 class="product_tab_title"><span>{{ $respuesta->Grupo }}-{{ $respuesta->Marcas }}</span></h5>
                                    <ul class="list_none comment_list mt-4">
                                        @foreach($comentarios as $coments)
                                            <li>
                                                <div class="comment_img">
                                                    <img src="{{ asset('assets/images/user1.jpg') }}" />
                                                </div>
                                                <div class="comment_block">
                                                    <div class="rating_wrap">
                                                        <div class="rating">
                                                            <div class="product_rate" style="width:{{ (100/5)*$coments->Star_rating }}%"></div>
                                                        </div>
                                                    </div>
                                                    <p class="customer_meta">
                                                        <span class="review_author">{{ $coments->Nombre }}</span>
                                                        <span class="comment-date">{{ $coments->created_at }}</span>
                                                    </p>
                                                    <div class="description">
                                                        <p>{{ $coments->Comentario }}</p>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="review_form field_form">
                                    <h5>Añadir un comentario</h5>
                                    {!! Form::open(['url'=>'crearComentario']) !!}
                                        <div class="form-group col-12">
                                            <input type="hidden" id="Star_rating" name="Star_rating"value="">
                                            <div class="star_rating">
                                                <span data-value="1" ><i class="far fa-star"></i></span>
                                                <span data-value="2" ><i class="far fa-star"></i></span>
                                                <span data-value="3" ><i class="far fa-star"></i></span>
                                                <span data-value="4" ><i class="far fa-star"></i></span>
                                                <span data-value="5" ><i class="far fa-star"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-12">
                                            <textarea required="required" placeholder="Tu comentario *" resize="none" class="form-control" name="Comentario" rows="4"></textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input required="required" placeholder="Ingresa el nombre que aparecerá en el Comentario *" class="form-control" name="Nombre" type="text">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input required="required" placeholder="Ingresa tu email *" class="form-control" name="email" type="email">
                                        </div>
                                        <div class="form-group col-12">
                                            <button type="submit" class="btn btn-fill-out" value="Submit">Añadir Comentario</button>
                                        </div>
                                        <input type="hidden" name="product_id" id="product_id" value="{{ $respuesta->id }}">
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-12">
                    <div class="small_divider"></div>
                    <div class="divider"></div>
                    <div class="medium_divider"></div>
                </div>
            </div> --}}
            {{-- PRODUCTOS RELACIONADOS --}}
            {{-- <div class="row">
                <div class="col-12">
                    <div class="heading_s1">
                        <h3>Productos Relacionados</h3>
                    </div>
                    <div class="releted_product_slider carousel_slider owl-carousel owl-theme" data-margin="20" data-responsive='{"0":{"items": "1"}, "481":{"items": "2"}, "768":{"items": "3"}, "1199":{"items": "4"}}'>
                        {{-- @foreach($relacionados as $rel)
                            <div class="item">
                            <div class="product">
                                <div class="product_img">
                                    <a href="shop-product-detail.html">
                                        <img src="{{ $rel->ImageURL }}" alt="product_img2">
                                    </a>
                                    <div class="product_action_box">
                                        <ul class="list_none pr_action_btn">
                                            <li class="add-to-cart"><a href="#"><i class="icon-basket-loaded"></i> Add To Cart</a></li>
                                            {{-- <li><a href="shop-compare.html"><i class="icon-shuffle"></i></a></li>
                                            <li><a href="{{ url('verDetalle/'.$rel->id) }}" class="popup-ajax"><i class="icon-magnifier-add"></i></a></li>
                                            @if($value != null)
                                                {!! Form::open(['url'=>'agregarFavoritos', 'id'=>'favForm']) !!}
                                                    <li><a onclick="document.getElementById('favForm').submit();" type="submit"><i class="icon-heart"></i></a></li>
                                                    <input type="hidden" name="idUser" value="{{ Auth::user()->id }}">
                                                    <input type="hidden" name="partNo" value="{{ $rel->PartNo }}">
                                                    <input type="hidden" name="idProducto" value="{{ $rel->id }}">
                                                {!! Form::close() !!}
                                            @else
                                                <li><a href="{{ url('favoritos') }}" onclick="document.getElementById('favForm').submit();" type="submit"><i class="icon-heart"></i></a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_info">
                                    <h6 class="product_title"><a href="shop-product-detail.html">{{ $rel->PartNo }}</a></h6>
                                    <div class="product_price">
                                        <span class="price">${{ $rel->P1*$tc }}</span>
                                        {{-- <del>$95.00</del>
                                        <div class="on_sale">
                                            <span>25% Off</span>
                                        </div>
                                    </div>
                                    <div class="rating_wrap">
                                        <div class="rating">
                                            <div class="product_rate" style="width:68%"></div>
                                        </div>
                                        <span class="rating_num">(15)</span>
                                    </div>
                                    <div class="pr_desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit massa enim. Nullam id varius nunc id varius nunc.</p>
                                    </div>
                                    {{-- <div class="pr_switch_wrap">
                                        <div class="product_color_switch">
                                            <span class="active" data-color="#847764"></span>
                                            <span data-color="#0393B5"></span>
                                            <span data-color="#DA323F"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div> --}}
        </div>
    </div>
<!-- END SECTION SHOP -->
</div>
@endsection

@section('script')
    <script>
        // $('document').ready(function(){
           $(document).on("ready", function(){
            $('.star_rating span').on('click', function(){
                    var onStar = parseFloat($(this).data('value'), 10);
                    $('#Star_rating').val(onStar);
                    var stars = $(this).parent().children('.star_rating span');
                    for (var i = 0; i < stars.length; i++) {
                        $(stars[i]).removeClass('selected');
                    }
                    for (i = 0; i < onStar; i++) {
                        $(stars[i]).addClass('selected');
                    }
                    var si = $('#Star_rating').val();
                    // alert(si)
                });
            });
        // })
    </script>
@endsection
