<?php header("Access-Control-Allow-Origin: *"); ?>
@extends('layout.mainlayout')

@section('content')
<div class="mt-4 staggered-animation-wrap" >
    <br><br><br><br><br><br><br><br><br><br><br>
    <!-- START SECTION BREADCRUMB -->
    @if (Request::url() == url('buscarFamilia/Desktop'))
        {!! Form::hidden('family', 'Desktop', ['id'=>'family']) !!}
        <div class="breadcrumb_section page-title-mini" style="background-color: #E7367B">
            <div class="container"><!-- STRART CONTAINER -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="page-title">
                            <h1 id="familia" style="color: WHITE; margin-left: 100px; font-size: 50px">DESKTOPS</h1>
                        </div>
                    </div>
                </div>
            </div><!-- END CONTAINER-->
        </div>
    @elseif(Request::url() == url('buscarFamilia/Laptops'))
        {!! Form::hidden('family', 'Laptops', ['id'=>'family']) !!}
        <div class="breadcrumb_section page-title-mini" style="background-color: #F4B300">
            <div class="container"><!-- STRART CONTAINER -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="page-title">
                            <h1 id="familia" style="color: white; margin-left: 100px; font-size: 50px">LAPTOPS</h1>
                        </div>
                    </div>
                </div>
            </div><!-- END CONTAINER-->
        </div>
    @elseif(Request::url() == url('buscarFamilia/Printers'))
        {!! Form::hidden('family', 'Printers', ['id'=>'family']) !!}
        <div class="breadcrumb_section page-title-mini" style="background-color: #79B729">
            <div class="container"><!-- STRART CONTAINER -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="page-title">
                            <h1 id="familia" style="color: white; margin-left: 100px; font-size: 50px">IMPRESORAS</h1>
                        </div>
                    </div>
                </div>
            </div><!-- END CONTAINER-->
        </div>
    @elseif(Request::url() == url('buscarFamilia/Projectors'))
        {!! Form::hidden('family', 'Projectors', ['id'=>'family']) !!}
        <div class="breadcrumb_section page-title-mini" style="background-color: #43559B">
            <div class="container"><!-- STRART CONTAINER -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="page-title">
                            <h1 id="familia" style="color: white; margin-left: 100px; font-size: 50px">PROYECTORES</h1>
                        </div>
                    </div>
                </div>
            </div><!-- END CONTAINER-->
        </div>
    @elseif(Request::url() == url('buscarFamilia/Server'))
        {!! Form::hidden('family', 'Server', ['id'=>'family']) !!}
        <div class="breadcrumb_section page-title-mini" style="background-color: #1BC1D0">
            <div class="container"><!-- STRART CONTAINER -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="page-title">
                            <h1 id="familia" style="color: white; margin-left: 100px; font-size: 50px">SERVIDORES</h1>
                        </div>
                    </div>
                </div>
            </div><!-- END CONTAINER-->
        </div>
    @elseif(Request::url() == url('buscarFamilia/Plotters'))
        {!! Form::hidden('family', 'Plotters', ['id'=>'family']) !!}
        <div class="breadcrumb_section page-title-mini" style="background-color: #622570">
            <div class="container"><!-- STRART CONTAINER -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="page-title">
                            <h1 id="familia" style="color: white; margin-left: 100px; font-size: 50px">SERVIDORES</h1>
                        </div>
                    </div>
                </div>
            </div><!-- END CONTAINER-->
        </div>
    @endif
    <!-- END SECTION BREADCRUMB -->

    <!-- START MAIN CONTENT -->
    <div class="main_content">

    <!-- START SECTION SHOP -->
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-9" style="padding-left: 50px">
                    <div class="row align-items-center mb-4 pb-1">
                        <div class="col-12">
                            <div class="product_header">
                                <div class="product_header_right">
                                    <div class="products_view">
                                        <a href="javascript:Void(0);" class="shorting_icon grid"><i class="ti-view-grid"></i></a>
                                        <a href="javascript:Void(0);" class="shorting_icon list active"><i class="ti-layout-list-thumb"></i></a>
                                    </div>
                                </div>
                                    @if(session('status'))
                                        <div class="alert alert-success" id="msgAlert" style="width: 400px;" id="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    <div>
                                        Registros encontrados {{ $registros }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="row shop_container list">
                                @if($products->isEmpty() || count($products) == 0)
                                    <div align="center"><br><br><br>
                                        <h1>Ups... por el momento el producto que buscas no se encuentra disponible.</h1>
                                    </div>
                                @else
                                    @foreach($products as $resp)
                                        <div class="col-md-4 col-6">
                                            <div class="product">
                                                <div class="product_img">
                                                    <a href="{{ url('verDetalle/'.$resp->id) }}">
                                                        <img src="{{ $resp->ImageURL }}" alt="product_img1">
                                                    </a>
                                                    @php
                                                        $precioEntreMargen = number_format(floatval($resp->P1)/.65,2);
                                                        $precio = floatval($precioEntreMargen) * floatval($tc) * 1.16;
                                                    @endphp
                                                    <div class="product_action_box">
                                                        <ul class="list_none pr_action_btn">
                                                            <li class="add-to-cart"><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            <li><a href="{{ $resp->ImageURL }}" class="content-popup"><i class="icon-magnifier-add"></i></a></li>
                                                            <li><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="product_info">
                                                    <h6 class="product_title"><a href="{{ url('verDetalle/'.$resp->id) }}" id="aNombre">{{ $resp->Grupo }}-{{ $resp->Marcas }}</a></h6>
                                                    <div class="product_price">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price">{{ $resp->PartNo }}</span></a>
                                                        <div class="on_sale">
                                                            @if($resp->Disponible > 0 )
                                                                <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500"> Stock</span></a>
                                                            @elseif($resp->Disponible == 0)
                                                                <a href="{{ url('verDetalle/'.$resp->id) }}"><span style="color:#43559B; font-weight: 500">Agotado</span></a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    @foreach($array as $value)
                                                        @if($value['product_id'] == $resp->id)
                                                            <div class="rating_wrap">
                                                                <div class="rating">
                                                                    @php
                                                                        $division = $value['Suma'] / $value['Promedio'];
                                                                        $redondear = round($division);
                                                                    @endphp<br>
                                                                    <div class="product_rate" style="width:{{ (100/5)*$redondear }}%"></div>
                                                                </div>
                                                                <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="rating_num">{{ $value['Promedio']}}</span></a>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                    <div class="pr_desc">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><p>{{ $resp->Descripcion }}.</p></a>
                                                    </div>
                                                    @php
                                                        setlocale(LC_MONETARY, 'en_US');
                                                        $precioEntreMargen = number_format(floatval($resp->P1)/.65,2);
                                                        $precio = floatval($precioEntreMargen) * floatval($tc) * 1.16;
                                                        // echo $precioEntreMargen;
                                                        if($resp->Condicion == 1){
                                                            $condicion = 'Nuevo';
                                                        }elseif($resp->Condicion == 2){
                                                            $condicion = 'Remanufacturado';
                                                        }elseif($resp->Condicion == 3){
                                                            $condicion = 'Usado';
                                                        }

                                                    @endphp
                                                    <div class="product_price">
                                                        <span style="color: red">{{ $resp->Tipo }}</span>
                                                        <div class="on_sale">
                                                            <span>{{ $condicion }}</span>
                                                        </div>
                                                    </div>
                                                    <ul class=" pr_action_btn" style="margin-left: 50px; list-style-type: none;">
                                                        <li><a href="{{ $resp->ImageURL }}" class="content-popup"><i class="icon-magnifier-add"></i></a></li>
                                                        <li><a href="{{ url('agregarFavoritos/'.$resp->id) }}"><i class="icon-heart"></i></a></li>
                                                    </ul>
                                                </div>
                                                <div class="product_cart">
                                                    <div class="list_product_action_box">
                                                        <a href="{{ url('verDetalle/'.$resp->id) }}"><span class="price"  style="font-size: 30px">$ {{ number_format($precio,2) }} MXN</span></a>
                                                        @if($resp->Disponible > 0 )
                                                            <ul class="list_none pr_action_btn">
                                                                <li class="add-to-cart"><a href="{{ url("agregarCarrito/".$resp->id.'/'.$precio.'/1') }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                                            </ul>
                                                        @elseif($resp->Disponible == 0)
                                                            <ul class="list_none pr_action_btn">
                                                                <li class="add-to-cart"><a href="modalSolicitar"  data-toggle="modal" data-target="#modalSolicitar" data-whatever="$resp->PartNo" ><i class="icon-cursor"></i> Solicitar Producto</a></li>
                                                            </ul>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        {{-- PAGINACIÓN --}}
                        <div class="row">
                            <div id="paginacion" class="pagination mt-3 justify-content-center pagination_style1"></i></a></li>
                                    {!! $products->links() !!}
                            </div>
                        </div>
                </div>
                {{-- CATEGORÍAS --}}
                <div class="col-lg-3 order-lg-first mt-4 pt-2 mt-lg-0 pt-lg-0" >
                    <div class="sidebar" style="margin-right: 35px">
                        <div class="widget">
                            <div class="sidenav">
                                @if(Request::url() == url('buscarFamilia/Desktop'))
                                    <button class="dropdown-btn ">Desktops
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                    <div class="dropdown-container">
                                        @if(isset($respuesta3[0]->SWITCH))
                                            {!! Form::hidden('grupo', 'SWITCH',['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/desktop/switch') }}">Switch...{{ $respuesta3[0]->SWITCH }}</a>
                                        @endif
                                        @if($respuesta3[0]->MEMORY)
                                            {!! Form::hidden('grupo', 'MEMORY',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/desktop/memory') }}">Memoria...{{ $respuesta3[0]->MEMORY }}</a>
                                        @endif
                                        @if($respuesta3[0]->INTERNALCOMPONENTS)
                                            {!! Form::hidden('grupo', 'INTERNALCOMPONENTS',['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/desktop/internal%20components') }}">Componentes Internos...{{ $respuesta3[0]->INTERNALCOMPONENTS }}</a>
                                        @endif
                                        @if($respuesta3[0]->FAN)
                                            {!! Form::hidden('grupo', 'FAN',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/desktop/fan') }}">FAN...{{ $respuesta3[0]->FAN }}</a>
                                        @endif
                                        @if($respuesta3[0]->MOTHERBOARDS)
                                            {!! Form::hidden('grupo', 'MOTHERBOARDS',['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/desktop/motherboards') }}">Tarjeta Madre...{{ $respuesta3[0]->MOTHERBOARDS }}</a>
                                        @endif
                                        @if($respuesta3[0]->POWERSUPPLY)
                                            {!! Form::hidden('grupo', 'POWERSUPPLY',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/desktop/power%20supply') }}">Power Supply..{{ $respuesta3[0]->POWERSUPPLY }}</a>
                                        @endif
                                        @if($respuesta3[0]->HARDDISK)
                                            {!! Form::hidden('grupo', 'HARDDISK',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/desktop/hard%20disk') }}">Discoduro...{{ $respuesta3[0]->HARDDISK }}</a>
                                        @endif
                                        @if($respuesta3[0]->VIDEOCARDS)
                                            {!! Form::hidden('grupo', 'VIDEOCARDS',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/desktop/video%20cards') }}">Tarjeta de Video...{{ $respuesta3[0]->VIDEOCARDS }}</a>
                                        @endif
                                        @if($respuesta3[0]->DISPLAY)
                                            {!! Form::hidden('grupo', 'DISPLAY',['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/desktop/display') }}">Display...{{ $respuesta3[0]->DISPLAY }}</a>
                                        @endif
                                        @if($respuesta3[0]->CABLES)
                                            {!! Form::hidden('grupo', 'CABLES',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/desktop/cables') }}">Cables...{{ $respuesta3[0]->CABLES }}</a>
                                        @endif
                                        @if($respuesta3[0]->ACCESORIES)
                                            {!! Form::hidden('grupo', 'ACCESORIES',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/desktop/accesories') }}">Accesorios...{{ $respuesta3[0]->ACCESORIES }}</a>
                                        @endif
                                        @if($respuesta3[0]->SOLIDSTATEDISK)
                                            {!! Form::hidden('grupo', 'SOLIDSTATEDISK',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/desktop/solid%20state%20disk') }}">SSD..{{ $respuesta3[0]->SOLIDSTATEDISK }}</a>
                                        @endif
                                        @if($respuesta3[0]->COMPUTER)
                                            {!! Form::hidden('grupo', 'COMPUTER',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/desktop/computer') }}">Computadora...{{ $respuesta3[0]->COMPUTER }}</a>
                                        @endif
                                        @if($respuesta3[0]->KEYBOARDS)
                                            {!! Form::hidden('grupo', 'KEYBOARDS' ,['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/desktop/keyboards') }}">Teclados...{{ $respuesta3[0]->KEYBOARDS }}</a>
                                        @endif
                                        @if($respuesta3[0]->PROCESSORS)
                                            {!! Form::hidden('grupo', 'PROCESSORS',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/desktop/processors') }}">Procesadores...{{ $respuesta3[0]->PROCESSORS }}</a>
                                        @endif
                                    </div>
                                    <br>
                                @endif
                                @if(Request::url() == url('buscarFamilia/Printers'))
                                    <button class="dropdown-btn">Impresoras
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                    <div class="dropdown-container">
                                            <a href="#">Adaptadores Originales </a>
                                            @if( $respuesta3[2]->PICKUP )
                                                {!! Form::hidden('grupo', 'PICKUP',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/pick%20up') }}">Pick Up...{{ $respuesta3[2]->PICKUP }}</a>
                                            @endif
                                            @if($respuesta3[2]->MAINTENANCEKIT)
                                                {!! Form::hidden('grupo', 'MAINTENANCEKIT',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/maintenance%20kit') }}">MAINTENANCE KIT...{{ $respuesta3[2]->MAINTENANCEKIT }}</a>
                                            @endif
                                            @if($respuesta3[2]->PICKUPROLLERS)
                                                {!! Form::hidden('grupo', 'PICKUPROLLERS' ,['id'=>'grupo']) !!}
                                                <a href="{{ url('productosCate/printers/pick%20up%20rollers') }}">Pick Up Rollers...{{ $respuesta3[2]->PICKUPROLLERS }}</a>
                                            @endif
                                            @if($respuesta3[2]->ROLLERSROLLERKIT)
                                                {!! Form::hidden('grupo','ROLLERSROLLERKIT',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/rollers%20/%20/roller%20kit') }}">Rollers/Rollers Kit...{{ $respuesta3[2]->ROLLERSROLLERKIT }}</a>
                                            @endif
                                            @if($respuesta3[2]->INTERNALCOMPONENTS)
                                                {!! Form::hidden('grupo', 'INTERNALCOMPONENTS' ,['id'=>'grupo']) !!}
                                                <a href="{{ url('productosCate/printers/internal%22components') }}">Componentes internos...{{ $respuesta3[2]->INTERNALCOMPONENTS }}</a>
                                            @endif
                                            @if($respuesta3[2]->FUSERS)
                                                {!! Form::hidden('grupo', 'FUSERS' ,['id'=>'grupo']) !!}
                                                <a href="{{ url('productosCate/printers/fusers') }}">Fusers...{{ $respuesta3[2]->FUSERS }}</a>
                                            @endif
                                            @if($respuesta3[2]->TRANSFERROLLER)
                                                {!! Form::hidden('grupo', 'TRANSFERROLLER',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/transfer%roller') }}">Transfer Roller...{{ $respuesta3[2]->TRANSFERROLLER }}</a>
                                            @endif
                                            @if($respuesta3[2]->PADS)
                                                {!! Form::hidden('grupo', 'PADS',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/pads') }}">PADS...{{ $respuesta3[2]->PADS }}</a>
                                            @endif
                                            @if($respuesta3[2]->ROLLERS)
                                                {!! Form::hidden('grupo', 'ROLLERS',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/rollers') }}">Rollers...{{ $respuesta3[2]->ROLLERS }}</a>
                                            @endif
                                            @if($respuesta3[2]->TRANSFERBELTKIT)
                                                {!! Form::hidden('grupo', 'TRANSFERBELTKIT ',['id'=>'grupo']) !!}
                                                <a href="{{ url('productosCate/printers/transfer%20belt%20kit') }}">Transfer Belt Kit...{{ $respuesta3[2]->TRANSFERBELTKIT }}</a>
                                            @endif
                                            @if($respuesta3[2]->TRAY)
                                                {!! Form::hidden('grupo', 'TRAY',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/tray') }}">Tray...{{ $respuesta3[2]->TRAY }}</a>
                                            @endif
                                            @if($respuesta3[2]->ADF)
                                                {!! Form::hidden('grupo', 'ADF' ,['id'=>'grupo']) !!}
                                                <a href="{{ url('productosCate/printers/adf') }}">ADF...{{ $respuesta3[2]->ADF }}</a>
                                            @endif
                                            @if($respuesta3[2]->TRANSFERBELT)
                                                {!! Form::hidden('grupo', 'TRANSFERBELT',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/transfer%20belt') }}">Transfer Belt...{{ $respuesta3[2]->TRANSFERBELT }}</a>
                                            @endif
                                            @if($respuesta3[2]->FUSERPARTS)
                                                {!! Form::hidden('grupo', 'FUSERPARTS',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/fuser%20parts') }}">Fuser Parts...{{ $respuesta3[2]->FUSERPARTS }}</a>
                                            @endif
                                            @if($respuesta3[2]->ACCESORIES)
                                                {!! Form::hidden('grupo', 'ACCESORIES',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/accesories') }}">Accesorios...{{ $respuesta3[2]->ACCESORIES }}</a>
                                            @endif
                                            @if($respuesta3[2]->ASSEMBLY)
                                                {!! Form::hidden('grupo', 'ASSEMBLY',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/assembly') }}">Assembly...{{ $respuesta3[2]->ASSEMBLY }}</a>
                                            @endif
                                            @if($respuesta3[2]->FORMATTER)
                                                {!! Form::hidden('grupo', 'FORMATTER',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/formatter') }}">Formatter...{{ $respuesta3[2]->FORMATTER }}</a>
                                            @endif
                                            @if($respuesta3[2]->TRANSFERKIT)
                                                {!! Form::hidden('grupo', 'TRANSFERKIT',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/transfer%20kit') }}">Transfer Kit...{{ $respuesta3[2]->TRANSFERKIT }}</a>
                                            @endif
                                            @if($respuesta3[2]->POWERSUPPLY)
                                                {!! Form::hidden('grupo', 'POWERSUPPLY' ,['id'=>'grupo']) !!}
                                                <a href="{{ url('productosCate/printers/power%20supply') }}">Power Supply...{{ $respuesta3[2]->POWERSUPPLY }}</a>
                                            @endif
                                            @if($respuesta3[2]->EXTERNALCOMPONENT)
                                                {!! Form::hidden('grupo', 'EXTERNALCOMPONENT' ,['id'=>'grupo']) !!}
                                                <a href="{{ url('productosCate/printers/external%20component') }}">Componentes Externos...{{ $respuesta3[2]->EXTERNALCOMPONENT }}</a>
                                            @endif
                                            @if($respuesta3[2]->DRUM)
                                                {!! Form::hidden('grupo', 'DRUM',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/drum') }}">Drum...{{ $respuesta3[2]->DRUM }}</a>
                                            @endif
                                            @if($respuesta3[2]->TONERS )
                                                {!! Form::hidden('grupo', 'TONERS',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/toners') }}">Toners...{{ $respuesta3[2]->TONERS }}</a>
                                            @endif
                                            @if($respuesta3[2]->FILMS)
                                                {!! Form::hidden('grupo', 'FILMS',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/films') }}">Films...{{ $respuesta3[2]->FILMS }}</a>
                                            @endif
                                            @if($respuesta3[2]->ADFADFKIT)
                                                {!! Form::hidden('grupo', 'ADFADFKIT',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/adf%20/%20adf%20kit') }}">ADF/Kit...{{ $respuesta3[2]->ADFADFKIT }}</a>
                                            @endif
                                            @if($respuesta3[2]->TRANSFERROLLERKIT)
                                                {!! Form::hidden('grupo', 'TRANSFERROLLERKIT',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/transfer%20roller%20kit') }}">Transfer Roller Kit...{{ $respuesta3[2]->TRANSFERROLLERKIT }}</a>
                                            @endif
                                            @if($respuesta3[2]->COVER)
                                                {!! Form::hidden('grupo', 'COVER',['id'=>'grupo'] ) !!}
                                                <a href="{{ url('productosCate/printers/covers') }}">Covers...{{ $respuesta3[2]->COVER }}</a>
                                            @endif
                                    </div>
                                @endif
                                @if(Request::url() == url('buscarFamilia/Laptops'))
                                    <button class="dropdown-btn">Laptops
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                    <div class="dropdown-container">
                                        @if($respuesta3[1]->ACADAPTERSORIGINALES)
                                            {!! Form::hidden('grupo', 'ACADAPTERSORIGINALES',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/ac%20adapters%20originales') }}">Adaptadores Originales...{{ $respuesta3[1]->ACADAPTERSORIGINALES }} </a>
                                        @endif
                                        @if($respuesta3[1]->BATTERIES)
                                            {!! Form::hidden('grupo', 'BATTERIES' ,['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/laptops/batteries') }}">Baterias...{{ $respuesta3[1]->BATTERIES }}</a>
                                        @endif
                                        @if($respuesta3[1]->BATTERIESGENERIC)
                                            {!! Form::hidden('grupo', 'BATTERIESGENERIC',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/batteries%20generic') }}">Baterias Genéricas...{{ $respuesta3[1]->BATTERIESGENERIC }}</a>
                                        @endif
                                        @if($respuesta3[1]->CABLES)
                                            {!! Form::hidden('grupo', 'CABLES',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/cables') }}">Cables...{{ $respuesta3[1]->CABLES }}</a>
                                        @endif
                                        @if($respuesta3[1]->MOTHERBOARDS)
                                            {!! Form::hidden('grupo', 'MOTHERBOARDS',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/motherboards') }}">Motherboards...{{ $respuesta3[1]->MOTHERBOARDS }}</a>
                                        @endif
                                        @if($respuesta3[1]->DISPLAY)
                                            {!! Form::hidden('grupo', 'DISPLAY',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/display') }}">Display...{{ $respuesta3[1]->DISPLAY }}</a>
                                        @endif
                                        @if($respuesta3[1]->FAN)
                                            {!! Form::hidden('grupo', 'FAN',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/fan') }}">FAN...{{ $respuesta3[1]->FAN }}</a>
                                        @endif
                                        @if($respuesta3[1]->KEYBOARDS)
                                            {!! Form::hidden('grupo', 'KEYBOARDS',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/keyboards') }}">Teclados...{{ $respuesta3[1]->KEYBOARDS }}</a>
                                        @endif
                                        @if($respuesta3[1]->COVERS)
                                            {!! Form::hidden('grupo', 'COVERS',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/covers') }}">Covers...{{ $respuesta3[1]->COVERS }}</a>
                                        @endif
                                        @if($respuesta3[1]->HARDDISK)
                                            {!! Form::hidden('grupo', 'HARDDISK' ,['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/laptops/hard%20disk') }}">Disco Duro...{{ $respuesta3[1]->HARDDISK }}</a>
                                        @endif
                                        @if($respuesta3[1]->INTERNALCOMPONENTS)
                                            {!! Form::hidden('grupo', 'INTERNALCOMPONENTS',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/internal%20components') }}">Componentes Internos...{{ $respuesta3[1]->INTERNALCOMPONENTS }}</a>
                                        @endif
                                        @if($respuesta3[1]->DISPLAYTOUCH)
                                            {!! Form::hidden('grupo', 'DISPLAYTOUCH',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/display%20touch') }}">Display Touch...{{ $respuesta3[1]->DISPLAYTOUCH }}</a>
                                        @endif
                                        @if($respuesta3[1]->SOLIDSTATEDISK)
                                            {!! Form::hidden('grupo', 'SOLIDSTATEDISK',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/solid%20state%20disk') }}">SSD...{{ $respuesta3[1]->SOLIDSTATEDISK }}</a>
                                        @endif
                                        @if($respuesta3[1]->TOUCH)
                                            {!! Form::hidden('grupo', 'TOUCH' ,['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/laptops/touch') }}">Touch...{{ $respuesta3[1]->TOUCH }}</a>
                                        @endif
                                        @if($respuesta3[1]->ACCESORIES)
                                            {!! Form::hidden('grupo', 'ACCESORIES' ,['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/laptops/acccesories') }}">Accesorios...{{ $respuesta3[1]->ACCESORIES }}</a>
                                        @endif
                                        @if($respuesta3[1]->MEMORY)
                                            {!! Form::hidden('grupo', 'MEMORY',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/memory') }}">Memoria...{{ $respuesta3[1]->MEMORY }}</a>
                                        @endif
                                        @if($respuesta3[1]->ACADAPTERSGENERIC)
                                            {!! Form::hidden('grupo', 'ACADAPTERSGENERIC',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/ac%20adapters%20generic') }}">Adaptadores Originales...{{ $respuesta3[1]->ACADAPTERSGENERIC }}</a>
                                        @endif
                                        @if($respuesta3[1]->VIDEOCARDS)
                                            {!! Form::hidden('grupo', 'VIDEOCARDS',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/video%20cards') }}">Tarjeta de Video...{{ $respuesta3[1]->VIDEOCARDS }}</a>
                                        @endif
                                        @if($respuesta3[1]->POWERSUPPLY)
                                            {!! Form::hidden('grupo', 'POWERSUPPLY' ,['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/laptops/power%20supply') }}">Powesupply...{{ $respuesta3[1]->POWERSUPPLY }}</a>
                                        @endif
                                        @if($respuesta3[1]->PROCESSORS)
                                            {!! Form::hidden('grupo', 'PROCESSORS',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/laptops/processors') }}">Procesadores...{{ $respuesta3[1]->PROCESSORS }}</a>
                                        @endif
                                    </div>
                                @endif
                                @if(Request::url() == url('buscarFamilia/Server'))
                                    <button class="dropdown-btn">Servidores
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                    <div class="dropdown-container">
                                        @if($respuesta3[3]->HARDDISK)
                                            {!! Form::hidden('grupo', 'HARDDISK',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/server/hard%20disk') }}">Hard Disk...{{ $respuesta3[3]->HARDDISK }} </a>
                                        @endif
                                        @if($respuesta3[3]->POWERSUPPLY)
                                            {!! Form::hidden('grupo', 'POWERSUPPLY' ,['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/server/power%20supply') }}">PowerSupply...{{ $respuesta3[3]->POWERSUPPLY }}</a>
                                        @endif
                                        @if($respuesta3[3]->MEMORY)
                                            {!! Form::hidden('grupo', 'MEMORY',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/server/memory') }}">Memory...{{ $respuesta3[3]->MEMORY }}</a>
                                        @endif
                                        @if($respuesta3[3]->INTERNALCOMPONENTS)
                                            {!! Form::hidden('grupo', 'INTERNALCOMPONENTS' ,['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/server/internal%20components') }}">InternalComponents...{{ $respuesta3[3]->INTERNALCOMPONENTS }}</a>
                                        @endif
                                        @if($respuesta3[3]->FAN)
                                            {!! Form::hidden('grupo','FAN',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/server/fan') }}">Fan...{{ $respuesta3[3]->FAN }}</a>
                                        @endif
                                        @if($respuesta3[3]->MOTHERBOARDS)
                                            {!! Form::hidden('grupo', 'MOTHERBOARDS' ,['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/server/motherboards') }}">Motherboards...{{ $respuesta3[3]->MOTHERBOARDS }}</a>
                                        @endif
                                        @if($respuesta3[3]->ACCESORIES)
                                            {!! Form::hidden('grupo', 'ACCESORIES',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/server/accesories') }}">Accesories...{{ $respuesta3[3]->ACCESORIES }}</a>
                                        @endif
                                        @if($respuesta3[3]->SOLIDSTATEDISK)
                                            {!! Form::hidden('grupo', 'SOLIDSTATEDISK',['id'=>'grupo'] ) !!}
                                            <a href="{{ url('productosCate/server/solid%20state%20disk') }}">SSD...{{ $respuesta3[3]->SOLIDSTATEDISK }}</a>
                                        @endif
                                    </div>
                                @endif
                                @if(Request::url() == url('buscarFamilia/Projectors'))
                                    <button class="dropdown-btn">Proyectores
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                    <div class="dropdown-container">
                                        @if($respuesta3[4]->INTEGRATEDCIRCUITS)
                                            {!! Form::hidden('grupo', 'INTEGRATEDCIRCUITS' ,['id'=>'grupo']) !!}
                                            <a href="{{ url('productosCate/projectors/integrated%20circuits') }}">Circuitos Integrados...{{ $respuesta3[4]->INTEGRATEDCIRCUITS }} </a>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="widget">
                            <h5 class="widget_title">Marcas</h5>
                            @foreach($arrayMarcas as $id => $marca)
                                <ul class="list_brand">
                                    <li>
                                        <div class="custome-checkbox" >
                                            <input class="form-check-input category_checkbox" type="checkbox"  name="checkbox"  id="{{ $marca }}" value="{{ $marca }}">
                                            <label class="form-check-label" for="{{ $marca }}"><span id="spanID">{{ $marca }}</span></label>
                                        </div>
                                    </li>
                                </ul>
                            @endforeach
                        </div>
                        <div class="widget" >
                            <h5 class="widget_title">Condición</h5>
                            <div class="product_size_switch" style="margin-left: 21px">
                                {!! Form::radio('condicion', '1','', ['class'=>'form-check-input']) !!}
                                {!! Form::label('condicion', 'Nuevo', ['class'=>'form-check-label']) !!}
                            </div>
                            <div class="product_size_switch" style="margin-left: 21px">
                                {!! Form::radio('condicion', '2','', ['class'=>'form-check-input']) !!}
                                {!! Form::label('condicion', 'Remanufacturado', ['class'=>'form-check-label']) !!}
                            </div>
                            <div class="product_size_switch" style="margin-left: 21px">
                                {!! Form::radio('condicion', '3','', ['class'=>'form-check-input']) !!}
                                {!! Form::label('condicion', 'Usado', ['class'=>'form-check-label']) !!}
                            </div>
                        </div>
                        <div class="widget" >
                            <h5 class="widget_title">Tipo</h5>
                                <div class="product_size_switch" style="margin-left: 21px">
                                    <div class="form-group">
                                        {!! Form::radio('tipo', 'Original') !!}
                                        {!! Form::label('tipoL', 'Original') !!}
                                    </div>
                                </div>
                                <div class="product_size_switch" style="margin-left: 21px">
                                    <div class="form-group">
                                        {!! Form::radio('tipo', 'Genérico') !!}
                                        {!! Form::label('tipoL', 'Genérico') !!}
                                        {{-- {!! Form::checkbox($name, $value, $checked, [$options]) !!} --}}
                                    </div>
                                </div>
                                <br><br>
                            {!! Form::hidden('Ids','',['id'=>'ids']) !!}
                            {!! Form::hidden('Url', url()->current()) !!}
                            <a onclick="filtrar()" class="btn btn-fill-out" id="filtrar" style="margin-left: 50px">Buscar</a>
                        </div>
                        {{-- <button onclick="checkPalindrome()" >pruebas</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalSolicitar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            {!! Form::open(['url'=>'solicitarProducto']) !!}
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Solicitar Producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    Envía un correo a uno de nuestros asesores para realizar un pedido de este
                    producto.
                    <br><br>
                    @if(count($products) > 0)
                         {!! Form::hidden('PartNo', $resp->PartNo,['id'=>'idproducto']) !!}
                    @endif
                    <div class="form-group">
                        {!! Form::textarea('Mensaje', '¡Hola! Deseo adquirir este producto, por favor contáctame.',
                        ['class'=>'form-control','required', 'style'=>'resize: none']) !!}
                    </div>
                    <div class="form-group col-md-12" style="padding-right: 5px">
                        {!! Form::text('Name', '', ['class'=>'form-control', 'required', 'placeholder'=>'Nombre y Apellido*', 'style'=>'margin-bottom: 15px']) !!}
                        {!! Form::text('Email', '',  ['class'=>'form-control', 'required', 'placeholder'=>'Email*']) !!}
                    </div>
                    <div class="form-group col-md-12" style="padding-left: 5px">
                        {!! Form::text('Tel', '', ['class'=>'form-control','required','placeholder'=>'Teléfono*', 'style'=>'margin-bottom: 15px']) !!}
                        {!! Form::text('Ciudad', '', ['class'=>'form-control','required','placeholder'=>'Ciudad y Estado*']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Enviar Correo</button>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- END SECTION SHOP -->
</div>


@endsection

@section('script')
    <script>
        var dropdown = document.getElementsByClassName("dropdown-btn");
        var i;

        for (i = 0; i < dropdown.length; i++) {
            dropdown[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var dropdownContent = this.nextElementSibling;
                    if (dropdownContent.style.display === "block") {
                        dropdownContent.style.display = "none";
                    } else {
                        dropdownContent.style.display = "block";
                    }
            });
        }

        function filtrar(){
            var pathname = window.location.pathname;
            var split = pathname.split('/');
            var familia = $('#family').val();

            if(familia == undefined){
                var familia = split[2];
            }

            var tipo = $('input:radio[name=tipo]:checked').val();
            var condicion = $('input:radio[name=condicion]:checked').val()

            if(split[1] == 'productosCate'){
                var grupo = split[3];
            }else if(!isNaN(grupo)){
                grupo = undefined;
            }
            // alert(grupo)
            var ids = [];
                $('.category_checkbox').each(function () {
                    if ($(this).is(":checked")) {
                        ids.push($(this).attr('id'));
                    }
                })
                if(ids.length>0){
                    $('#ids').val(ids);
                }
                // alert(familia)
                // alert(grupo)
                // alert(condicion)
                // alert(tipo)
                // alert(ids)

            if(familia != undefined && grupo != undefined && condicion != undefined && tipo != undefined && ids != undefined){
                document.getElementById('filtrar').href= "/filtrar/"+familia+'/'+grupo+'/'+condicion+'/'+tipo+'/'+ids;
            }else if (familia != undefined && grupo == undefined && condicion != undefined && tipo != undefined && ids != undefined){
                document.getElementById('filtrar').href= "/filtrar-grupo/"+familia+'/'+condicion+'/'+tipo+'/'+ids;
            }else if(familia != undefined && grupo == undefined && condicion == undefined && tipo != undefined && ids != undefined){
                document.getElementById('filtrar').href= "/filtrar-grupo-condi/"+familia+'/'+tipo+'/'+ids;
            }else if(familia != undefined && grupo == undefined && condicion == undefined && tipo == undefined && ids != undefined){
                document.getElementById('filtrar').href= "/filtrar-grupo-condi-tipo/"+familia+'/'+ids;
            }else if(familia != undefined && grupo == undefined && condicion != undefined && tipo == undefined && ids != undefined){
                document.getElementById('filtrar').href= "/filtrar-grupo-tipo/"+familia+'/'+condicion+'/'+ids;
            }else if(familia != undefined && grupo != undefined && condicion == undefined && tipo != undefined && ids != undefined){
                document.getElementById('filtrar').href= "/filtrar-condicion/"+familia+'/'+grupo+'/'+tipo+'/'+ids;
            }else if(familia != undefined && grupo != undefined && condicion != undefined && tipo == undefined && ids != undefined){
                document.getElementById('filtrar').href= "/filtrar-tipo/"+familia+'/'+grupo+'/'+condicion+'/'+ids;
            }
        }

        $('#price_filter').each( function() {
            var $filter_selector = $(this);
            var a = $filter_selector.data("min-value");
            var b = $filter_selector.data("max-value");
            var c = $filter_selector.data("price-sign");

            $filter_selector.slider({
                range: true,
                min: $filter_selector.data("min"),
                max: $filter_selector.data("max"),
                values: [ a, b ],
                slide: function( event, ui ) {
                    $( "#flt_price" ).html( c + ui.values[ 0 ] + " - " + c + ui.values[ 1 ] );
                    $( "#price_first" ).val(ui.values[ 0 ]);
                    $( "#price_second" ).val(ui.values[ 1 ]);
                }
            });

            var z = $( "#flt_price" ).html( c + $filter_selector.slider( "values", 0 ) + " - " + c + $filter_selector.slider( "values", 1 ) );
            var y = Object.values(z);

        });

        $(document).ready(function() {

              $('#msgAlert').fadeIn();
                setTimeout(function() {
                    $("#msgAlert").fadeOut();
                },2000);

            $(document).on('click', '#filtrar', function () {
                var ids = [];
                    $('.category_checkbox').each(function () {
                        if ($(this).is(":checked")) {
                            ids.push($(this).attr('id'));
                        }
                    })
                        // alert(ids)
                if(ids.length>0){
                    $('#ids').val(ids);
                    //  $.get('{{ url("filtrar") }}/'+ids, function(returnData){
                    //     // $('#paginacion').val()
                    //     // alert('ahi vas')
                    // });
                }
            });
        });
        function checkPalindrome() {
            var inputArray = [-23, 4, -3, 8, -12];

            var input = inputArray.length;
            var result =0;

                for(i=0; i<input; i++){
                    var promedio = inputArray[i] * inputArray[i+1];
                    var result = promedio;

                    if(result > promedio ){
                        // alert(promedio)
                        result = result;
                    }else{
                        result = promedio;
                    }
                }
                alert(result)
        }
    </script>
@endsection
