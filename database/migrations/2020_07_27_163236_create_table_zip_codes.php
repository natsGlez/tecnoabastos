<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableZipCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zipCodes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('CodigoPostal')->nullable();
            $table->string('Asentamiento')->nullable();
            $table->string('TipoAsentamiento')->nullable();
            $table->string('Municipio')->nullable();
            $table->string('Estado')->nullable();
            $table->string('Ciudad')->nullable();
            $table->integer('d_CP')->nullable();
            $table->integer('idEstado')->nullable();
            $table->integer('c_oficina')->nullable();
            $table->string('c_CP')->nullable();
            $table->integer('idTipoAsentamiento')->nullable();
            $table->integer('idMunicipio')->nullable();
            $table->integer('idAsentamiento')->nullable();
            $table->string('Zona')->nullable();
            $table->integer('ClaveCiudad')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zipCodes');
    }
}
