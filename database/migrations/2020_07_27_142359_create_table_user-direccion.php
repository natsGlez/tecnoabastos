<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUserDireccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userDireccion', function (Blueprint $table) {
            $table->id();
            $table->string('Calle');
            $table->string('Titulo');
            $table->char('NumExt',8)->nullable();
            $table->char('NumInt',8)->nullable();
            $table->char('CP',5);
            $table->string('Colonia');
            $table->string('Estado');
            $table->string('Municipio');
            $table->foreignId('user_id')->nullable()->onDelete('SET NULL')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userDireccion');
    }
}
