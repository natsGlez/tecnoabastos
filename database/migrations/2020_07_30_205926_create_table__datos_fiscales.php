<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDatosFiscales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datosFiscales', function (Blueprint $table) {
            $table->id();
            $table->integer('idUser')->unsigned();
            $table->string('RFC');
            $table->string('Titulo');
            $table->string('RazonSocial');
            $table->string('CFDI');
            $table->string('MetodoPago');
            $table->string('FormaPago');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datosFiscales');
    }
}
