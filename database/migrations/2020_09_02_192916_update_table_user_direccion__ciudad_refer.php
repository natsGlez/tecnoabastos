<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableUserDireccionCiudadRefer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userDireccion', function (Blueprint $table) {
            $table->string('Ciudad')->nullable()->after('Municipio');
            $table->string('Referencia')->after('Titulo');
            $table->string('Telefono')->after('NumInt');
            $table->string('Contacto')->after('Calle');
            $table->bigInteger('direBonance')->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('userDireccion', function (Blueprint $table) {
            $table->dropColumn('Ciudad');
            $table->dropColumn('Referencia');
            $table->dropColumn('Telefono');
            $table->dropColumn('Contacto');
            $table->dropColumn('direBonance');
        });
    }
}
