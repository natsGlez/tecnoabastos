<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarritoComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carritoCompras', function (Blueprint $table) {
            $table->id();
            $table->string('id_usuario');
            $table->string('Producto');
            $table->float('Precio');
            $table->tinyInteger('Cantidad');
            $table->float('SubTotal');
            $table->integer('pedido_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carritoCompras');
    }
}
