<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('install', function() {
    Artisan::call('migrate');
});

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->name('social.auth');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('recuperar-password', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('no');
Route::get('password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('passwordLink','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/','IndexController@index');
Route::get('contacto','IndexController@contacto');
Route::post('enviarMensajeContacto','IndexController@enviarMensajeContacto');
Route::post('solicitarProducto','IndexController@solicitarProducto');
Route::get('acercade','IndexController@acercade');
Route::get('faq','IndexController@faq');
Route::post('registrar','IndexController@registrar');
Route::get('politicasydevoluciones','IndexController@politicas');
Route::get('terminosycondiciones','IndexController@terminos');
Route::get('avisodeprivacidad','IndexController@aviso');
Route::get('comparar-productos','IndexController@compararVista');
Route::get('detalle-de-productos','IndexController@detalleProducto');
Route::get('productos','ProductosController@index');
Route::get('productosCate/{fam}/{cate}','ProductosController@indexCate');
Route::get('buscarMarca', 'ProductosController@buscarMarca');
Route::post('suscribirse','IndexController@newsletterSuscribir');
Route::get('registro','IndexController@registro');
Route::get('verDetalle/{id}','ProductosController@detalle');
Route::get('buscarFamilia/{familia}','ProductosController@buscarFamilia');
Route::post('crearComentario','ComentariosController@create');
Route::get('filtrar/{familia?}/{grupo}/{condicion}/{tipo}/{marcas}','ProductosController@filtrar');
Route::get('filtrar-condicion/{familia?}/{grupo}/{tipo}/{marcas}','ProductosController@filtrarCondicion');
Route::get('filtrar-tipo/{familia?}/{grupo}/{condicion}/{marcas}','ProductosController@filtrarTipo');
Route::get('filtrar-grupo/{familia}/{condicion}/{tipo}/{marcas}','ProductosController@filtrarGrupo');
Route::get('filtrar-grupo-condi/{familia?}/{tipo}/{marcas}','ProductosController@filtrarGrupoCondi');
Route::get('filtrar-grupo-tipo/{familia?}/{condicion}/{marcas}','ProductosController@filtrarGrupoTipo');
Route::get('filtrar-grupo-condi-tipo/{familia?}/{marcas}','ProductosController@filtrarGrupoCondiTipo');
Route::get('filtrar/{familia?}/{grupo}/{condicion}/{tipo}/{marcas}','ProductosController@filtrar');
Route::get('filtrar/{familia?}/{grupo}/{condicion}/{tipo}/{marcas}','ProductosController@filtrar');
Route::get('error_page','IndexController@errorPage');
Route::get('buscarProducto/{id}','ProductosController@buscarProducto');

Route::group(['middleware' => 'auth'], function () {
    Route::get('verCarrito','CarritoController@index');
    Route::get('agregarCarrito/{id}/{precio}/{cantidad}','CarritoController@agregar');
    Route::get('eliminarProducto/{id}','CarritoController@eliminar');
    Route::get('sumarTotal/{cantidad}/{id}','CarritoController@sumarTotal');
    Route::get('restarTotal/{cantidad}/{id}','CarritoController@restarTotal');
    Route::get('/home', 'UserController@inicio')->name('home');
    Route::get('checkout','CarritoController@checkout');
    Route::get('orden-completa/{id}','IndexController@ordenCompleta');
    Route::get('carrito-de-compras','CarritoController@index');
    Route::get('favoritos','FavoritosController@show');
    Route::get('agregarFavoritos/{id}','FavoritosController@agregar');
    Route::get('eliminarFav/{id}','FavoritosController@eliminar');
    Route::get('pedidos','PedidosController@verpedidos');
    Route::get('detallePedido/{id}','PedidosController@detalle');
    Route::post('realizarPedido','PedidosController@realizarPedido');
    Route::get('paypal/pay', 'PaypalController@payWithPaypal');
    Route::get('getImage/{id}','PedidosController@getImage');
    Route::get('paypal/status', 'PaypalController@paypalStatus');
    Route::post('adjuntarPago','PedidosController@adjuntarPago');
    Route::post('mercado-pago-pagar', 'PaymentController@mercadoPagoPay');
    Route::get('metodosPago', 'PaymentController@metodosPago');
    Route::post('pagoEfectivo', 'PaymentController@pagoEfectivo');
    Route::get('inicio', 'UserController@inicio')->name('home');
    Route::post('subirFicha', 'PedidosController@subirFicha');

    Route::get('direcciones','DireccionesController@verDomicilios');
    Route::get('crearDomicilio','DireccionesController@crearDomicilio');
    Route::post('guardarDomicilio','DireccionesController@guardarDomicilio');
    Route::get('eliminarDomicilio/{id}','DireccionesController@eliminarDomicilio');
    Route::get('getMunicipio/{cp}','DireccionesController@getMunicipio');
    Route::get('getCodigoPostal/{cp}','DireccionesController@getCodigoPostal');
    Route::get('getColonia/{cp}','DireccionesController@getColonia');
    Route::get('getDataDomicilio/{id}', 'DireccionesController@edit');
    Route::post('actualizarDomicilio','DireccionesController@actualizarDomicilio');

    Route::get('datosFacturas','DatosFiscalesController@verDatosFiscales');
    Route::get('crearDatosFiscales','DatosFiscalesController@crearDatosFiscales');
    Route::post('guardarDatosFiscales','DatosFiscalesController@guardarDatosFiscales');
    Route::get('eliminarDatosFiscales/{id}','DatosFiscalesController@eliminarDatosFiscales');
    Route::post('actualizarDatosFiscales','DatosFiscalesController@actualizarDatosFiscales');
    Route::get('getData/{id}', 'DatosFiscalesController@edit');

    Route::get('perfil','UserController@perfil');
    Route::post('guardarPerfil','UserController@guardarPerfil');

    Route::get('revisarSubtotal/{id}','CarritoController@revisarSubtotal');
});


