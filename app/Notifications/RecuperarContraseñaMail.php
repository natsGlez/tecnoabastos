<?php

namespace App\Notifications;
// use App\Notifications\MyResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
// use Illuminate\Notifications\Notification;

class RecuperarContraseñaMail extends ResetPassword
{

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        // $url = route('password.reset',$this->token);
        return (new MailMessage)
            ->subject('Recuperar contraseña')
            ->greeting('Hola')
            ->line('Estás recibiendo este mail porque alguien ha solicitado cambiar la contraseña de esta cuenta en TecnoAbastos')
            ->line('Si tú no hiciste esta solicitud simplemente ignora este correo electrónico')
            ->line('Este mensaje expirará en : 60 minutos')
            ->line('Si quieres proceder: ')
            ->action('Haz clic aquí para restablecer tu contraseña', route('password.reset', $this->token));

    }

}
