<?php

namespace App\Http\Controllers;
use App\DatosFiscales;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class DatosFiscalesController extends Controller
{

    public  function verDatosFiscales(){
        $cfdi = DatosFiscales::$CFDI;
        $forma = DatosFiscales::$FormaPago;
        $metodo = DatosFiscales::$MetodoPago;
        $datosFiscales = DatosFiscales::where('idUser',Auth::user()->id)->get();

        return view('dashboard.datosFiscales.facturas', compact('datosFiscales','cfdi','forma','metodo'));
    }

    public function crearDatosFiscales(){
        $cfdi = DatosFiscales::$CFDI;
        $forma = DatosFiscales::$FormaPago;
        $metodo = DatosFiscales::$MetodoPago;

        return view('dashboard.datosFiscales.crearDatosFiscales', compact('cfdi','forma','metodo'));
    }

    public function guardarDatosFiscales(Request $request){
        $datos = new DatosFiscales();

        $validator = Validator::make($request->all(),[
            'RFC' => ['regex: /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/'],
        ]);                   /* /^[A-Z]{4}([0-9]{2})(1[0-2]|0[1-9])([0-3][0-9])([ -]?)([A-Z0-9]{4})$/*/

        if($validator->fails()){
             return Redirect::back()->with('status', 'Es necesario que ingreses un RFC válido!');
        }else{

            $datos->RFC = $request->RFC;
            $datos->RazonSocial = $request->RazonSocial;
            $datos->CFDI = $request->USOCFDI;
            $datos->MetodoPago = $request->MetodoPago;
            $datos->FormaPago = $request->FormaPago;
            $datos->idUser = Auth::user()->id;
            $datos->Titulo = $request->Titulo;
            $datos->save();
            // dd($request->checkout
            if($request->checkout == 1){

                if($datos){
                    return Redirect::back()->with('status', 'Los datos se añadieron correctamente, revisa la lista y selecciónalo!');
                }elseif(Mail::failures()){
                    return Redirect::back()->with('status', 'El producto no pudo ser añadido, intenta más tarde por favor');
                }
                return back();
            }elseif($request->checkout == null)

                $datosFiscales = DatosFiscales::where('idUser',Auth::user()->id)->get();

                return redirect('datosFacturas')->with(['datosFiscales'=>$datosFiscales]);
        }
    }

    public function eliminarDatosFiscales($id){
        $datos = DatosFiscales::find($id);
        // dd($dire);
        $datos->delete();

        $datosFiscales = DatosFiscales::where('idUser',Auth::user()->id)->get();

        return redirect('datosFacturas')->with(['datosFiscales'=>$datosFiscales]);
    }

    public function edit($id){
        $datos = DatosFiscales::find($id);

        return $datos;
    }

    public function actualizarDatosFiscales(Request $request){
        $datos = DatosFiscales::find($request->id);
        //  dd($request->all());
        $datos->RFC = $request->RFC;
        $datos->RazonSocial = $request->RazonSocial;
        $datos->CFDI = $request->USOCFDI;
        $datos->MetodoPago = $request->MetodoPago;
        $datos->FormaPago = $request->FormaPago;
        $datos->idUser = Auth::user()->id;
        $datos->Titulo = $request->Titulo;
        $datos->save();

        $datosFiscales = DatosFiscales::where('idUser',Auth::user()->id)->get();

        return redirect('datosFacturas')->with(['datosFiscales'=>$datosFiscales]);
    }
}
