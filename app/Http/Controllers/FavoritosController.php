<?php

namespace App\Http\Controllers;
use App\Favoritos;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class FavoritosController extends Controller
{

    public function agregar($id){
        $fav = Favoritos::where('idProducto',$id)->first();
        // dd($fav);
        if($fav == null){
            $fav = new Favoritos();
            // dd($id);
            $fav->idUser = Auth::user()->id;
            $fav->idProducto = $id;
            $fav->url= 'algo';
            $value = Auth::user();
            $fav->save();
        }
        else{
            return back();
        }

        if($fav){
            return Redirect::back()->with('status', 'El producto se agregó a favoritos!');
        }elseif(Mail::failures()){
            return Redirect::back()->with('status', 'El producto no pudo ser añadido, intenta más tarde por favor');
        }

        // return back()->with(['value'=>$value]);
    }

    public function eliminar($id){
        $fav = Favoritos::where('idProducto',$id)->first();
        // dd($fav);
        $fav->delete();
        return back();
    }

    public function show(){
        $user =Auth::user()->id;
        $favoritos = Favoritos::where('idUser',$user)->get();

        if(count($favoritos) > 0){
            $arrayFav = array();
            foreach($favoritos as $fav){
                // dd($fav->idProducto);
                $client = new Client([
                    'base_uri' => 'http://asserver.ddns.net/americanstock/',
                    // 'base_uri' => 'http://127.0.0.1:8001/',
                    'timeout' => 2.0,
                    ]);

                    $response = $client->request('GET', "api/producto/{$fav->idProducto}");
                    $respuesta = json_decode($response->getBody()->getContents());
                    $arrayFav[] = $respuesta;

                    $response2 = $client->request('GET', 'api/tipoDeCambio');
                    $tc = json_decode($response2->getBody()->getContents());
                }
            // dd($arrayFav);
            return view('favoritos.wishlist',compact('arrayFav','tc'));
        }
            return view('favoritos.wishlist');
    }
}
