<?php

namespace App\Http\Controllers;
use App\Comentarios;
use App\User;
use Illuminate\Http\Request;

class ComentariosController extends Controller
{
    public function create(Request $request){
        $coments = new Comentarios();
        $user = User::where('email',$request->email)->first();
       
        if($user != null){
            $coments->create($request->all());
            $coments->save();
        }
        else{
            echo 'Es necesario que te registres para poder agregar comentarios';
        }
        return back();
    }
}
