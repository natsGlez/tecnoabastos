<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Carrito;
use App\UserDireccion;
use App\DatosFiscales;
use App\ZipCodes;
use App\Pedidos;
use App\Comentarios;
use App\User;
use App\ZipCodesEstados;
use App\ZipCodesMunicipios;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class CarritoController extends Controller
{

    public function agregar($id,$precio,$cantidad){
        $producto = Carrito::where('Producto',$id)
                    ->where('id_usuario',Auth::user()->id)->first();

        if($producto == null){
            $producto = new Carrito();

            // $porcentaje = $precio*1.3;
            $producto->id_usuario = Auth::user()->id;
            $producto->Cantidad = $cantidad;
            $producto->Producto = $id;
            $producto->Precio = $precio;
            $producto->SubTotal = $producto->Precio*$producto->Cantidad;
            $producto->save();
        }else{
            $total = $producto->Precio * ($cantidad + 1);

            $producto->Subtotal = $total;
            $producto->Cantidad = $producto->Cantidad+1;
            $producto->save();
        }

        if($producto){
            return Redirect::back()->with('status', 'El producto se agregó al Carrito!');
        }else{
            return Redirect::back()->with('status', 'El producto no se tiene en existencia, contacta a un representante para ayudarte ');
        }

        return back();
    }

    public function index(){
        $user =Auth::user()->id;
        $carritos = Carrito::where('id_usuario',$user)->get();

        $arrayCart = array();
        $client = new Client([
                'base_uri' => 'http://asserver.ddns.net/americanstock/api/',
                // 'base_uri' => 'http://127.0.0.1:8001/api/',
                'timeout' => 2.0,
            ]);

        foreach($carritos as $cart){

                $response = $client->request('GET', "producto/{$cart->Producto}");
                $respuesta = json_decode($response->getBody()->getContents());
                $respuesta->Precio = $cart->Precio;
                $respuesta->Cantidad = $cart->Cantidad;
                $respuesta->SubTotal = $cart->SubTotal;
                $respuesta->id_usuario = $user;
                $arrayCart[] = $respuesta;

        }
            $response2 = $client->request('GET', 'tipoDeCambio');
            $tc = json_decode($response2->getBody()->getContents());
        // dd($arrayCart);
        return view('flujo-compra.carrito-compras',compact('arrayCart','tc'));
    }

    public function eliminar($id){
        $productoCart = Carrito::where('Producto',$id);
        // dd($productoCart);
        $productoCart->delete();
        
        $carrito = Carrito::where('id_usuario',Auth::user()->id)->first();
        // dd($carrito);
        if($carrito == null){
            $pedido = Pedidos::where('id_pedidoBonance',null);

            $pedido->delete();
        }

        $client = new Client([
            'base_uri' => 'http://asserver.ddns.net/americanstock/api/',
            // 'base_uri' => 'http://127.0.0.1:8001/api/',
            'timeout' => 2.0,
        ]);

        $response = $client->request('GET', 'destacados');
        $respuesta = json_decode($response->getBody()->getContents());
        $hp_chunk = array_chunk($respuesta[0],3,true);
        $lenovo_chunk = array_chunk($respuesta[1],3,true);
        $dell_chunk = array_chunk($respuesta[2],3,true);

        $response2 = $client->request('GET', 'tipoDeCambio');
        $tc = json_decode($response2->getBody()->getContents());
        $suma=0;
        $array = Array();
        $calificaciones = Comentarios::select('product_id')->distinct()->get()->toArray();

        // dd($array);
        foreach($calificaciones as $cali){
            $estrellas = Comentarios::where('product_id',$cali)->get();
                foreach($estrellas as $estrella){
                    $suma = $suma+$estrella->Star_rating;
                }
                $cali['Suma'] = $suma;
                $cali['Promedio'] = count($estrellas);
                $array[]= $cali;
                $suma=0;
        }

        return view('pages.welcome',compact('hp_chunk','lenovo_chunk', 'dell_chunk', 'tc','array'));
    }

    public function sumarTotal($cantidad,$id){
        $productoCart = Carrito::where('Producto',$id)->first();
        $total = $productoCart->Precio * ($cantidad + 1);
        //  dd($total);
        $productoCart->Subtotal = $total;
        $productoCart->Cantidad = $cantidad+1;
        $productoCart->save();

        return $total;
    }

    public function restarTotal($cantidad,$id){
         $productoCart = Carrito::where('Producto',$id)->first();
         $total = $productoCart->Precio * ($cantidad-1);
        //  dd($total);
        $productoCart->Subtotal = $total;
        $productoCart->Cantidad = $cantidad-1;
        $productoCart->save();

         return $total;
    }

    public function checkout(){
        $code = ZipCodesEstados::all();
        $carritos = Carrito::where('id_usuario',Auth::user()->id)->get();
        $user = User::find(Auth::user()->id);
        $id = $user->idBonance;

        $client = new Client([
            'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
            'timeout' => 2.0,
        ]);

        $response = $client->request('GET', 'usuario/domicilio/'.$id.'/list');
        $direcciones  = json_decode($response->getBody());
        $datosFiscales = DatosFiscales::where('idUser',Auth::user()->id)->get();
        $cfdi = DatosFiscales::$CFDI;
        $forma = DatosFiscales::$FormaPago;
        $metodo = DatosFiscales::$MetodoPago;
        $pedido = Pedidos::where('id_pedidoBonance',null)->first();
        // dd($pedido);
        $total = 0;

            if($pedido == null){
                $pedido = new Pedidos();
                $pedido->estatus_id = 1;
                $pedido->Folio = $this->generarCodigo(12);
                $pedido->user_id = Auth::user()->id;
                $pedido->save();
            }


            $arrayCart = array();
            // dd($direcciones);
        foreach($carritos as $cart){
            $client = new Client([
                'base_uri' => 'http://asserver.ddns.net/americanstock/api/',
                // 'base_uri' => 'http://127.0.0.1:8001/',
                'timeout' => 2.0,
                ]);

                $response = $client->request('GET', "producto/{$cart->Producto}");
                $respuesta = json_decode($response->getBody()->getContents());

                $respuesta->Precio = $cart->Precio;
                $respuesta->Cantidad = $cart->Cantidad;
                $respuesta->SubTotal = $cart->SubTotal;
                $respuesta->Pedido = $cart->pedido_id;
                $arrayCart[] = $respuesta;
                $total=$total+$cart->SubTotal;
                // dd($cart);
                $cart->pedido_id = $pedido->id;
                $cart->save();
        }

        // dd($pedido);

        $pedido->Total = $total;
        $pedido->save();
        $response2 = $client->request('GET', 'tipoDeCambio');
        $tc = json_decode($response2->getBody()->getContents());

        return view('flujo-compra.checkout',compact('arrayCart','pedido','tc','direcciones','datosFiscales','code','cfdi','forma','metodo'));
    }

    public function revisarSubtotal($id){
        $productoCart = Carrito::where('id_usuario',$id)->get();
        $total=0;
         foreach($productoCart as $product){
            $sub=$product->SubTotal;
            $total=$total+$sub;
         }
        //  dd($total);
         return $total;
    }

    function generarCodigo($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }
}
