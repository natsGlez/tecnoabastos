<?php

namespace App\Http\Controllers;
use App\UserDireccion;
use App\User;
use App\ZipCodes;
use App\ZipCodesEstados;
use App\ZipCodesMunicipios;
use GuzzleHttp\Client;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class DireccionesController extends Controller
{
    public  function verDomicilios(){
        $user = User::find(Auth::user()->id);
        $id = $user->idBonance;

        $client = new Client([
            'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
            'timeout' => 2.0,
        ]);

        $response = $client->request('GET', 'usuario/domicilio/'.$id.'/list');
        $domicilios  = json_decode($response->getBody());

            // dd($domicilios);
    //    = UserDireccion::where('user_id',Auth::user()->id)->get();
        $code = ZipCodesEstados::all();
        return view('dashboard.direcciones.direcciones', compact('domicilios','code'));
    }

    public function crearDomicilio(){
        // $direE = UserDireccion::find($id);
         $code = ZipCodesEstados::all();
        //  dd($code);
        return view('dashboard.direcciones.crearDireccion',compact('code'));
    }

    public function getMunicipio($cp){
        $code = ZipCodes::select('Municipio')->where('Estado',$cp)->distinct()
                ->get()->toArray();
        // dd($code);
        return $code;
    }

    public function getCodigoPostal($cp){
        $code = ZipCodes::select('CodigoPostal')->where('Estado',$cp)->distinct()
                ->get()->toArray();
        // dd($code);
        return $code;
    }

    public function getColonia($cp){
        $code = ZipCodes::select('Asentamiento')->where('CodigoPostal',$cp)
                ->get()->toArray();
        // dd($code);
        return $code;
    }

    public function guardarDomicilio(Request $request){
        $user = User::find(Auth::user()->id);
        $id = $user->idBonance;
        // dd($request->all());

        $client = new Client([
            'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
            'timeout' => 2.0,
        ]);

        $response = $client->request('POST', 'usuario/domicilio/'.$id,
            ['form_params'=> [
                'Calle' => $request->Calle,
                'NumeroExterior' => $request->NumE,
                'NumeroInterior' => $request->NumI,
                'CP' => $request->CP,
                'Telefono' => $request->Telefono,
                'Estado' => $request->Estado,
                'Municipio' => $request->Municipio,
                'Ciudad' => $request->Ciudad,
                'Referencia' => $request->Referencias,
                'Colonia' => $request->Colonia,
                'Contacto' => $request->Contacto,
                'Nombre' => $request->Titulo,
            ]]
        );

        $respuesta = json_decode($response->getBody());

            if($respuesta->status == 'ok'){
                // $domicilio = new UserDireccion();

                // $domicilio->Calle = $request->Calle;
                // $domicilio->NumExt = $request->NumE;
                // $domicilio->NumInt = $request->NumI;
                // $domicilio->CP = $request->CP;
                // $domicilio->Contacto = $request->Contacto;
                // $domicilio->Telefono = $request->Telefono;
                // $domicilio->Estado = $request->Estado;
                // $domicilio->Municipio = $request->Municipio;
                // $domicilio->Ciudad = $request->Ciudad;
                // $domicilio->Referencia = $request->Referencias;
                // $domicilio->Colonia = $request->Colonia;
                // $domicilio->user_id = Auth::user()->id;
                // $domicilio->Titulo = $request->Titulo;
                // $domicilio->direBonance = $respuesta->id;
                // $domicilio->save();
                if($request->checkout == 1){
                // if($domicilio){
                    return Redirect::back()->with('status', 'Los datos se añadieron correctamente, revisa la lista y selecciónalo!');
                // }
                    // return back();
                }elseif($request->checkout == null)
                $domicilios = UserDireccion::where('user_id',Auth::user()->id)->get();

                return redirect('direcciones')->with(['domicilios'=>$domicilios]);
            }else{
                // elseif(Mail::failures()){
                    return Redirect::back()->with('status', 'El producto no pudo ser añadido, intenta más tarde por favor');
                // }
                return view('pages.404');
            }



    }

    public function eliminarDomicilio($id){
        // $dire = UserDireccion::find($id);
        // dd($id);
        $client = new Client([
            'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
            'timeout' => 2.0,
        ]);

        $response = $client->request('delete', 'usuario/domicilio/'.$id);
        $respuesta = json_decode($response->getBody());

        if($respuesta->status == 'ok'){
            $domicilios = UserDireccion::where('user_id',Auth::user()->id)->get();

            return redirect('direcciones')->with(['domicilios'=>$domicilios]);
        }else{
            return 'hay un error, no se borró nada';
        }


    }

    public function edit($id){
        // dd($id);
         $client = new Client([
            'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
            'timeout' => 2.0,
        ]);

        $response = $client->request('GET', 'usuario/domicilio/'.$id);
        $domicilio = json_decode($response->getBody());

        // $domicilio = UserDireccion::find($id);
        // dd($domicilio->Calle);

        return response()->jSon($domicilio,200);
    }

    public function actualizarDomicilio(Request $request){
        // $domicilio = UserDireccion::find($request->id);

         $client = new Client([
            'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
            'timeout' => 2.0,
        ]);

        $response = $client->request('PUT', 'usuario/domicilio/'.$request->id,
            ['form_params'=> [
                'Calle' => $request->Calle,
                'NumeroExterior' => $request->NumE,
                'NumeroInterior' => $request->NumI,
                'CP' => $request->CP,
                'Telefono' => $request->Telefono,
                'Estado' => $request->Estado,
                'Municipio' => $request->Municipio,
                'Ciudad' => $request->Ciudad,
                'Referencia' => $request->Referencias,
                'Colonia' => $request->Colonia,
                'Contacto' => $request->Contacto,
                'Nombre' => $request->Titulo,
            ]]
        );

        $domicilios = json_decode($response->getBody());
        // dd($respuesta->status());
        //     if($respuesta->status == 'ok'){
        //         $domicilio->Calle = $request->Calle;
        //         $domicilio->NumExt = $request->NumE;
        //         $domicilio->NumInt = $request->NumI;
        //         $domicilio->CP = $request->CP;
        //         $domicilio->Contacto = $request->Contacto;
        //         $domicilio->Telefono = $request->Telefono;
        //         $domicilio->Estado = $request->Estado;
        //         $domicilio->Municipio = $request->Municipio;
        //         $domicilio->Ciudad = $request->Ciudad;
        //         $domicilio->Referencia = $request->Referencias;
        //         $domicilio->Colonia = $request->Colonia;
        //         $domicilio->user_id = Auth::user()->id;
        //         $domicilio->Titulo = $request->Titulo;
        //         $domicilio->save();
        //     }

        // $domicilios = UserDireccion::where('user_id',Auth::user()->id)->get();

         return redirect('direcciones')->with(['domicilios'=>$domicilios]);
    }
}
