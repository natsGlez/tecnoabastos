<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use App\ImageModel;
use App\Comentarios;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Middleware;
use Hashids;

class ProductosController extends Controller{

    private $client;
    private $tc;
    private $arrayMarcas;
    private $arrayTipos;

    public function __construct(){
        $this->client = new Client([
            'base_uri' => 'http://asserver.ddns.net/americanstock/api/',
            // 'base_uri' => 'http://127.0.0.1:8001/api/',
            'timeout' => 10.0,
        ]);

        $response2 =  $this->client->request('GET', 'tipoDeCambio');
        $this->tc = json_decode($response2->getBody()->getContents());

        $response = $this->client->request('GET', 'productos');
        $respuesta = json_decode($response->getBody()->getContents());

        $marcas = $tipo = array();
        foreach($respuesta as $respuest){
            $marcas[] = $respuest->Marcas;
        }
        $arrayMarca = array_unique($marcas);
        $this->arrayMarcas = array_diff($arrayMarca, array(null));
        // dd($arrayMarca);
        foreach($respuesta as $respuest){
            $tipo[] = $respuest->Tipo;
        }
        $arrayTipo= array_unique($tipo);
        $this->arrayTipos = array_diff($arrayTipo, array(null));
    }
    public function parametros($responseS){
        $response = $responseS;
        $respuesta = json_decode($response->getBody()->getContents());
        //  dd($respuesta);
        $registros = sizeof($respuesta); //devuelve la cantidad de registros encontrados para la vista
        $page = LengthAwarePaginator::resolveCurrentPage();
        $perPage=15;
        $current_page_orders = array_slice($respuesta, ($page - 1) * $perPage, $perPage);

        $products = new LengthAwarePaginator($current_page_orders, count($respuesta), $perPage, $page, [
            'path' => url()->current(),
        ]);

        $value = Auth::user();

        $suma=0;
        $array = Array();
        $calificaciones = Comentarios::select('product_id')->distinct()->get()->toArray();

        // $familia = $respuesta[0]->Familia;
        // dd($familia);
        foreach($calificaciones as $cali){
            $estrellas = Comentarios::where('product_id',$cali)->get();
                foreach($estrellas as $estrella){
                    $suma = $suma+$estrella->Star_rating;
                }
                $cali['Suma'] = $suma;
                $cali['Promedio'] = count($estrellas);
                $array[]= $cali;
                $suma=0;
        }

        $response3 = $this->client->request('GET', 'registrosCatFamilia');
        $respuesta3 = json_decode($response3->getBody()->getContents());
        // dd($products);
        // dd($respuesta3);
        return view('productos.productos')
            ->with(['tc'=>$this->tc,
                'arrayMarcas'=>$this->arrayMarcas,
                'arrayTipos'=>$this->arrayTipos,
                'products'=>$products,
                'array'=>$array,
                // 'familia'=>$familia,
                'respuesta3'=>$respuesta3,
                'registros'=>$registros,
                'value'=>$value]);
    }
    public function index(){

        $respuesta = $this->parametros($this->client->request('GET', 'productos'));

        return $respuesta;
    }
    public function indexCate($fam,$cate){

        $respuesta = $this->parametros($this->client->request('GET', 'productos/'.$fam.'/'.$cate));

        return $respuesta;
    }
    public function detalle($id){

        $response = $this->client->request('GET', "producto/{$id}");
        $respuesta = json_decode($response->getBody()->getContents());
        // dd($respuesta);

        $comentarios = Comentarios::where('product_id',$id)->get();
        // dd($comentarios);
        $suma=0;
        $array = Array();
        $calificaciones = Comentarios::select('product_id')->distinct()->get()->toArray();

        // dd($array);
        foreach($calificaciones as $cali){
            $estrellas = Comentarios::where('product_id',$cali)->get();
                foreach($estrellas as $estrella){
                    $suma = $suma+$estrella->Star_rating;
                }
                $cali['Suma'] = $suma;
                $cali['Promedio'] = count($estrellas);
                $array[]= $cali;
                $suma=0;
        }
        $value = Auth::user();
        return view('productos.detalle-de-producto',compact('respuesta','array','tc','value','comentarios'))->with(['tc'=>$this->tc]);
    }
    public function buscarFamilia($familia){

        $respuesta = $this->parametros($this->client->request('GET', 'productoFamilia/'.$familia));

        return $respuesta;
    }
    public function filtrar($familia,$grupo,$condicion,$tipo,$marcas){

        $respuesta = $this->parametros($this->client->request('GET',
            'filtros/'.$familia.'/'.$grupo.'/'.$condicion.'/'.$tipo.'/'.$marcas));

        return $respuesta;
    }
    public function filtrarGrupo($familia,$condicion,$tipo,$marcas){

        $respuesta = $this->parametros($this->client->request('GET',
            'filtrar-Grupo/'.$familia.'/'.$condicion.'/'.$tipo.'/'.$marcas));

        return $respuesta;
    }
    public function filtrarCondicion($familia,$grupo,$tipo,$marcas){

        $respuesta = $this->parametros($this->client->request('GET',
            'filtrar-Condicion/'.$familia.'/'.$grupo.'/'.$tipo.'/'.$marcas));

        return $respuesta;
    }
    public function filtrarTipo($familia,$grupo,$condicion,$marcas){

        $respuesta = $this->parametros($this->client->request('GET',
            'filtrar-Tipo/'.$familia.'/'.$grupo.'/'.$condicion.'/'.$marcas));

        return $respuesta;
    }
    public function filtrarGrupoCondi($familia,$tipo,$marcas){
        // dd($familia,$tipo,$marcas);
        $respuesta = $this->parametros($this->client->request('GET',
            'filtrar-Grupo-Condi/'.$familia.'/'.$tipo.'/'.$marcas));

        return $respuesta;
    }
    public function filtrarGrupoTipo($familia,$condicion,$marcas){

        $respuesta = $this->parametros($this->client->request('GET',
            'filtrar-Grupo-Tipo/'.$familia.'/'.$condicion.'/'.$marcas));

        return $respuesta;
    }
    public function filtrarGrupoCondiTipo($familia,$marcas){
        // dd($familia, $marcas);
        $respuesta = $this->parametros($this->client->request('GET',
            'filtrar-Grupo-Condi-Tipo/'.$familia.'/'.$marcas));

        return $respuesta;
    }
    public function buscarProducto($buscar){

        $respuesta = $this->parametros($this->client->request('GET', 'buscarProducto/'.$buscar));

        return $respuesta;
    }
}
