<?php

namespace App\Http\Controllers;
use App\User;
use Validator;
use App\Pedidos;
use App\Carrito;
use Carbon\Carbon;
use App\DatosFiscales;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class PedidosController extends Controller
{

    public function verpedidos(){
        $pedidos = Pedidos::where('user_id',Auth::user()->id)->get();

        return view('dashboard.pedidos.pedidos', compact('pedidos'));
    }

    public function detalle($id){
        $pedido = Pedidos::where('id_pedidoBonance',$id)->first();
        // dd($pedido->user->email);
        $client = new Client([
            'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
            'timeout' => 10.0,
        ]);

        $response = $client->request('GET', 'pedido/'.$id);

        $respuesta  = json_decode($response->getBody());
        // dd($respuesta);

        foreach($respuesta->Partidas as $id){

            $client2 = new Client([
                'base_uri' => 'http://asserver.ddns.net/americanstock/api/',
                // 'base_uri' => 'http://127.0.0.1:8001/api/',
                'timeout' => 10.0,
            ]);

            $response2 = $client2->request('GET', "producto/{$id->idProducto}");
            $respuesta2 = json_decode($response2->getBody()->getContents());
            $id->ImagenUrl = $respuesta2->ImageURL;
        }

        $respuesta->Total = $pedido->Total;

        return view('dashboard.pedidos.detalle_pedido', compact('respuesta','pedido'));
    }

    public function realizarPedido(Request $request){
        // dd($request->all());
        $validator = Validator::make($request->all(),['idDomicilio' => 'required',
            'idTipoPago' => 'required']);

        if($validator->fails()){
            if($request->idTipoPago == 'Pp' ){
                return 'error';
            }
                return Redirect::back()->with('status', 'Es necesario que ingreses un domicilio!');
        }else{
            $cfdi = DatosFiscales::find($request->idFacturacion);
            $pedido = Pedidos::find($request->idPedido);

            if($request->facturacion == null){
                $usoCFDI = "P01";
            }elseif($request->facturacion == 'si'){
                $usoCFDI = $cfdi->CFDI;
            }

            // if($request->idTipoPago == 'Pp' ){
            //     $tipoPago = 7;
            //     $estatus = 2;
            // }else{
                $tipoPago = $request->idTipoPago;
                $estatus = 1;
            // }

            $user = User::find(Auth::user()->id);
            $id = $user->idBonance;

            $productos = Carrito::where('pedido_id',$request->idPedido)->get();

            // dd($productos);
            $data['form_params'] = [
                'idUsuario' => $id,
                'Folio' => $request->folio,
                'Fecha' => Carbon::now()->format('Y-m-d'),
                'TipoDePago' => $request->idTipoPago,
                'TipoDeCambio' => $request->tipoCambio,
                'CuentaDePago' => '',
                'idDomicilio' => $request->idDomicilio,
                'UsoCFDI' => $usoCFDI,
                'Estatus' => $estatus,
                'Partidas' => [],
            ];

            foreach($productos as $product){
                // dd($product);
                $row=[];
                $row['idProducto'] = $product->Producto;
                $row['Cantidad'] = $product->Cantidad;
                $row['PrecioUnitario'] = $product->Precio;
                $row['CodigoCliente'] = $product->id_usuario;
                $data['form_params']['Partidas'][] = $row;
            }
        //    dd($data);
            $client = new Client([
                'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
                'timeout' => 20.0,
            ]);

            $response = $client->request('POST', 'pedido', $data);

            $respuesta  = json_decode($response->getBody());


            if($respuesta->status == 'ok'){
                $pedido->id_pedidoBonance = $respuesta->id;
                $pedido->Total = $request->total;
                $pedido->save();

                foreach($productos as $prod){
                    $prod->delete();
                }

                if($request->idTipoPago == 'Pp' ){
                    return 'ok';
                }else{
                    return view('flujo-compra.orderCompleta', compact('pedido'));
                }
            }elseif($respuesta->status == 'error'){
                return view('pages.404');
            }
        }
    }

    public function subirFicha(Request $request){
        // dd($request->all());
         if($request->hasFile('fichaPago')){
            $extension = $request->fichaPago->getClientOriginalExtension();
            $mimeType = $request->fichaPago->getMimeType();
            $archivo = file_get_contents($request->fichaPago);
            $base64 = base64_encode($archivo);
            // dd($base64);

            $data['form_params'] = [
                'id' => $request->idPedido,
                'Archivo' => $base64,
                'ArchivoMime' => $mimeType,
                'ArchivoExt' => $extension,
            ];

            $client = new Client([
                'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
                'timeout' => 30.0,
            ]);

            // dd($data);
            $response = $client->request('POST','pedido/storeDocumento',$data);
            // dd($response);
            $respuesta  = json_decode($response->getBody());
            // dd($respuesta);
            // dd($respuesta);
            if($respuesta->status == 'ok'){
                return back()->with('status', 'Tu ficha se subió exitosamente, espera el cambio de estatus de tu pedido');
            }else{
                return back()->with('status','Hubo un problema para recibir tu archivo, intenta más tarde por favor');
            }
        }
    }
}
