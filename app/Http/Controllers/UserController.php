<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use App\User;

class UserController extends Controller
{


    public function inicio(){
        return view('dashboard.index-dashboard');
    }

    public function perfil(){
        $user = User::find(Auth::user()->id);

        return view('dashboard.perfil',compact('user'));
    }

    public function guardarPerfil(Request $request){
        $user = User::find(Auth::user()->id);

        $id = $user->idBonance;

        $client = new Client([
            'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
            'timeout' => 2.0,
        ]);

        $response = $client->request('POST', 'usuario/'.$id,
            ['form_params'=> [
                'Nombre' => $request->Nombre,
                'Email' => $request->Email,
                'Pass' => $request->password,
            ]]
        );

        $respuesta = json_decode($response->getBody());

        if($respuesta->status == 'ok'){
            $user->name =$request->Nombre;
            $user->email = $request->Email;
            $user->password = Hash::make($request->password);
            $user->save();
        }else{
            return 'no se pudo man ';
        }


        return view('dashboard.perfil',compact('user'));
    }

}
