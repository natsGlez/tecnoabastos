<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use App\Mail\MensajeRecibido;
use App\Mail\SolicitudProducto;
use App\Mail\SuscripcionTecnoabastos;
use App\Usuarios;
use App\Pedidos;
use App\Newsletter;
use App\Comentarios;

class IndexController extends Controller
{
    public function index(){

        $client = new Client([
            'base_uri' => 'http://asserver.ddns.net/americanstock/api/',
            // 'base_uri' => 'http://127.0.0.1:8001/api/',
            'timeout' => 2.0,
        ]);

        $response = $client->request('GET', 'destacados');
        $respuesta = json_decode($response->getBody()->getContents());
        $hp_chunk = array_chunk($respuesta[0],3,true);
        $lenovo_chunk = array_chunk($respuesta[1],3,true);
        $dell_chunk = array_chunk($respuesta[2],3,true);

        $response2 = $client->request('GET', 'tipoDeCambio');
        $tc = json_decode($response2->getBody()->getContents());
        $suma=0;
        $array = Array();
        $calificaciones = Comentarios::select('product_id')->distinct()->get()->toArray();

        // dd($array);
        foreach($calificaciones as $cali){
            $estrellas = Comentarios::where('product_id',$cali)->get();
                foreach($estrellas as $estrella){
                    $suma = $suma+$estrella->Star_rating;
                }
                $cali['Suma'] = $suma;
                $cali['Promedio'] = count($estrellas);
                $array[]= $cali;
                $suma=0;
        }

        // dd($response->getStatusCode());
        if($response->getStatusCode() >= 400){
            return view('pages.404');
        }else{
            return view('pages.welcome',compact('hp_chunk','lenovo_chunk', 'dell_chunk', 'tc','array'));
        }
    }

    public function contacto(){
        $success=0;

        return view('pages.contacto', compact('success'));
    }

    public function enviarMensajeContacto(Request $request){

         $msg = $request->validate([
            'Name' => 'required',
            'Email' => 'required | email',
            'Tel' => 'required',
            'Ciudad' => 'required',
            'Mensaje' => 'required'
         ]);
             Mail::to('contacto@tecnoabastos.com')->send(new MensajeRecibido($msg));


        if(!Mail::failures()){
            return Redirect::back()->with('status', 'El mensaje ha sido enviado!');
        }elseif(Mail::failures()){
            return Redirect::back()->with('status', 'El mensajo no pudo ser enviado, intenta más tarde por favor');
        }
    }

     public function solicitarProducto(Request $request){
        // dd($request->all());
         $msg = $request->validate([
            'Name' => 'required',
            'Email' => 'required | email',
            'Tel' => 'required',
            'Ciudad' => 'required',
            'Mensaje' => 'required',
            'PartNo' => 'required'
         ]);

        Mail::to('contacto@tecnoabastos.com')->send(new SolicitudProducto($msg));

        if(!Mail::failures()){
            return Redirect::back()->with('status', 'El mensaje ha sido enviado!');
        }elseif(Mail::failures()){
            return Redirect::back()->with('status', 'El mensajo no pudo ser enviado, intenta más tarde por favor');
        }
    }

    public function acercade(){
        return view('footer.acercade');
    }

    public function loginView(){
        return view('pages.login');
    }

    public function faq(){
        return view('footer.faq');
    }

    public function registro(){
        return view('pages.registro');
    }

    public function registrar(Request $request){
        $user = new Usuarios();

            $user->name = $request->Name;
            $user->email = $request->Email;
            $user->password = Hash::make($request->Password);

        // dd($request);
        return back();
    }

    public function politicas(){
        return view('footer.politicas');
    }

    public function aviso(){
        return view('footer.avisoprivacidad');
    }

    public function terminos(){
        return view('footer.terminos');
    }

    public function compararVista(){
        return view('pages.comparar');
    }

    public function ordenCompleta($id){
        $pedido = Pedidos::find($id);
        return view('flujo-compra.orderCompleta', compact('pedido'));
    }

    public function detalleProducto(){
        return view('productos.detalle-de-producto');
    }

    public function newsletterSuscribir(Request $request){
        $suscribir = new Newsletter();

        $suscribir->email = $request->email;

        $suscribir->save();

        // dd($suscribir->email);
        Mail::to($suscribir->email)->send(new SuscripcionTecnoabastos($suscribir->email));

        if(!Mail::failures()){
            return Redirect::back()->with('status', 'El mensaje ha sido enviado!');
        }elseif(Mail::failures()){
            return Redirect::back()->with('status', 'El mensajo no pudo ser enviado, intenta más tarde por favor');
        }
    }

    public function errorPage(){
        return view('pages.404');
    }
}
