<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;
use Illuminate\Support\Facades\Config;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Exception\PayPalConnectionException;
use App\Pedidos;
use App\User;
use Validator;
use App\Carrito;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\DatosFiscales;
use GuzzleHttp\Client;

class PaypalController extends Controller
{
    private $apiContext;

    public function __construct()
    {
        $payPalConfig  = Config::get('paypal');

        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                $payPalConfig['client_id'],
                $payPalConfig['client_secret']
            )
        );
        // dd($this->apiContext);
    }

    public function payWithPaypal(Request $request)
    {
        // dd($request->all());
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        // dd($request->all());
        $amount = new Amount();
        $amount->setTotal('1.00');
        $amount->setCurrency('MXN');

        $transaction = new Transaction();
        $transaction->setAmount($amount);

        $callBackUrl = url("paypal/status");

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($callBackUrl)
            ->setCancelUrl($callBackUrl);

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);

        // dd($transaction);
        // dd($payment);
        try {
            $payment->create($this->apiContext);
            $nose = redirect()->away($payment->getApprovalLink());
            // dd($nose);
            return $nose;
        } catch (PayPalConnectionException $ex) {
            $status = $ex->getData();
            // return redirect('error_page')->with(['status' => $status]);
        }
        // $aproved = 
    }

    public function paypalStatus(Request $request)
    {
        // dd($request->all());
        $paymentId = $request->input('paymentId');
        $payerId = $request->PayerID;
        $token = $request->token;

        if (!$paymentId || !$payerId || !$token) {
            $status = 'Hubo un problema con la transacción, inténtalo más tarde por favor';
            return redirect('error_page')->with(['status' => $status]);
        }

        $payment = Payment::get($paymentId, $this->apiContext);
        // dd($payment);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        $result = $payment->execute($execution, $this->apiContext);

        if ($result->getState() === 'approved') {
            return redirect('orden-completa')->with(['status' => 'approved']);
        } else {
            $status = 'Hubo un problema con la transacción, inténtalo más tarde por favor ';
            return redirect('error_page')->with(['status' => $status]);
        }
    }
}
