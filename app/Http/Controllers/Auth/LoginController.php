<?php

namespace App\Http\Controllers\Auth;
use Auth;
use App\User;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider){
        // dd($provider);
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider){
        $user = User::all();

        $social_suser = Socialite::driver($provider)->stateless()->user();

        $userExists = User::where('email', $social_suser->email)->first();
        // dd($social_suser);
        if($userExists != null){
           $user =  Auth::login($userExists);
            // dd($user);
        }else{
                $client = new Client([
                    'base_uri' => 'http://asserver.ddns.net/grupobonance/api/',
                    'timeout' => 2.0,
                ]);

                $response = $client->request('POST', 'usuario',
                    ['form_params'=> [
                        'Nombre' => $social_suser->name,
                        'Email' => $social_suser->email,
                        'Pass' => $social_suser->token,
                    ]]
                );

            $respuesta = json_decode($response->getBody());
            // dd($respuesta->id);
            if($respuesta->status == 'ok'){
                $user = new User();

                $user->name = $social_suser->name;
                $user->email = $social_suser->email;
                $user->password = Hash::make($social_suser->token);
                $user->idBonance = $respuesta->id;
                $user->RedesSociales = 1;
                $user->save();

                Auth::login($user);
            }else{
                return 'no se armó';
            }
        }return redirect()->to('/home');
    }
}

