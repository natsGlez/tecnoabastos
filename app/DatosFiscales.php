<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosFiscales extends Model
{
    protected $table = 'datosFiscales';

    public static $CFDI = [
        'G01'=> 'G01	Adquisición de mercancias',
        'G02' => 'G02	Devoluciones, descuentos o bonificaciones',
        'G03' => 'G03	Gastos en general',
        'I01' => 'I01	Construcciones',
        'I02' => 'I02	Mobilario y equipo de oficina por inversiones',
        'I03' => 'I03	Equipo de transporte',
        'I04'=> 'I04	Equipo de computo y accesorios',
        'I05' => 'I05	Dados, troqueles, moldes, matrices y herramental',
        'I06' => 'I06	Comunicaciones telefónicas',
        'I07' => 'I07	Comunicaciones satelitales',
        'I08' => 'I08	Otra maquinaria y equipo',
        'D01' => 'D01	Honorarios médicos, dentales y gastos hospitalarios',
        'D02' => 'D02	Gastos médicos por incapacidad o discapacidad',
        'D03' => 'D03	Gastos funerales',
        'D04' => 'D04	Donativos',
        'D05' => 'D05	Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación)',
        'D06'=> 'D06	Aportaciones voluntarias al SAR',
        'D07' => 'D07	Primas por seguros de gastos médicos',
        'D08' => 'D08	Gastos de transportación escolar obligatoria',
        'D09' => 'D09	Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones',
        'D10' => 'D10	Pagos por servicios educativos (colegiaturas)',
        'P01' => 'P01	Por definir'
    ];

    public static $MetodoPago = [
        'PUE (Pago en una exhibición'=> 'PUE (Pago en una exhibición',
        'PPD (Pago en parcialidad o diferido' => 'PPD (Pago en parcialidad o diferido'
    ];

    public static $FormaPago = [
        'Efectivo' => 'Efectivo',
        'Cheque nominativo' => 'Cheque nominativo',
        'Transferencia electrónica de fondos' => 'Transferencia electrónica de fondos',
        'Tarjeta de crédito' => 'Tarjeta de crédito',
        'Monedero electrónico' => 'Monedero electrónico',
        'Dinero electrónico' => 'Dinero electrónico',
        'Vales de despensa' => 'Vales de despensa',
        'Dación en pago' =>' Dación en pago',
        'Pago por subrogación' => 'Pago por subrogación',
        'Pago por consignación' => 'Pago por consignación',
        'Condonación' => 'Condonación',
        'Compensación' =>  'Compensación',
        'Novación' => 'Novación',
        'Confusión' => 'Confusión',
        'Remisión de deuda' => 'Remisión de deuda',
        'Prescripción o caducidad' => 'Prescripción o caducidad',
        'A satisfacción del acreedor' => 'A satisfacción del acreedor',
        'Tarjeta de débito' => 'Tarjeta de débito',
        'Tarjeta de servicios' => 'Tarjeta de servicios',
        'Aplicación de anticipos' => 'Aplicación de anticipos',
        'Por definir' => 'Por definir'
    ];
}
