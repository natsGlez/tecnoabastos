<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageModel extends Model
{
    protected $table = 'imagenes';

    public $fillable = [
        'fotografia',
        'Fotografia_Path',
        'Mime',
        'Extension',
        'Archivo',
    ];


    public function getDataFile(){

        if(!empty($this->Fotografia_Path)){

            $file = $this->Fotografia_Path;

            if (Storage::exists($file)){
                return Storage::get($file);
            } else {
                return file_get_contents(asset('img/nodisponible.jpg'));
            }
        } else {
            return $this->Fotografia;
        }
    }

    public function setDataFile($blob){

        $file = 'Imagenes/Imagenes_'.$this->id.'.dat';

        if (Storage::exists($file)){
            Storage::delete($file);
        }

        Storage::put($file,$blob);

        $this->Fotografia_Path = $file;

        $this->save();

        $path_real = Storage::disk('local')->path($file);

        @chmod ( $path_real , 777 );

    }
}
