<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    protected $table = "pedidos";

    public function estatus()
    {
        return $this->belongsTo(Estatus::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
