<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrito extends Model
{
    protected $table = 'carritoCompras';

    public function pedido()
    {
        return $this->belongsTo(Pedidos::class, 'pedido_id');
    }

}
