<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentarios extends Model
{
    protected $table = "comentarios";

    protected $fillable = [
        'product_id',
        'Comentario',
        'email',
        'Nombre',
        'Star_rating',
    ];
}
